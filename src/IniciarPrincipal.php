<?php
ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] . '/herramientas/sesiones');
session_start();
if(!isset($_SESSION["SsFichaEstudiante"])){
    header('Location: index.php');
    exit;
}

include_once('PrepararMenu.php');
$MenuSistema = new MenuPrincipal();
$MenuSistema -> ObtenerMenu();
?>