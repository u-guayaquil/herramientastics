<?php
require_once "../utils/utils.php";
require_once "../utils/conexionPdo.php";
class ControladorConsulta{
    public static function ctrConsultarRegistroDocente($Cedula){
        $DataCollection = ModeloConsulta::mdlDatosRegistroAulaVirtualByDocente($Cedula);
        if($DataCollection['status']==1){
            $DatosDocente = ControladorConsulta::PreparaDatos($DataCollection['message']);
            $respuesta = VistaConsulta::viewPintarTablaReporte($DatosDocente);
            return array("status"=> 1, "message"=>$respuesta);
        }
        else{
            return $DataCollection;
        }
    }

    public static function PreparaDatos($DataCollection){
        $Contador=1;
        $Datos = [];
        $ArrayDatos = [];
        $ArrayTemporal = [];
        $Grupo = '';
        foreach($DataCollection as $Count => $Row){
            if($Grupo != $Row['COD_GRUPO']){
                $Grupo = $Row['COD_GRUPO'];
                $ArrayDatos['USUARIO']  = $Row['USUARIO'];
                $ArrayDatos['DOCENTE']  = $Row['DOCENTE'];
                $ArrayDatos['GRUPO']    = $Row['GRUPO'];
                $ArrayDatos['MATERIA']  = $Row['MATERIA'];
                $ArrayDatos['LUNES']  = $Row['LUNES'];
                $ArrayDatos['MARTES']  = $Row['MARTES'];
                $ArrayDatos['MIERCOLES']  = $Row['MIERCOLES'];
                $ArrayDatos['JUEVES']  = $Row['JUEVES'];
                $ArrayDatos['VIERNES']  = $Row['VIERNES'];
                $ArrayDatos['SABADO']  = $Row['SABADO'];
                foreach($DataCollection as $Reg){
                    if($Grupo == $Reg['COD_GRUPO']){
                        $Reg['DIASEMANA'] = Utils::DevolverDiaSemana(date('Y-m-d',strtotime($Reg['FECHA'])));
                        $ArrayTemporal[]  = $Reg;
                        $Contador++;
                    }
                }
                $ArrayDatos['DATOS'] = $ArrayTemporal;
                $Datos[] = $ArrayDatos;
                $Contador = 1;
            }
            $ArrayTemporal = [];
            $ArrayDatos = [];
        }
        return $Datos;
    }
}
class ModeloConsulta{
    static public function mdlDatosRegistroAulaVirtualByDocente($Cedula)
    {   $DataCollection = [];
        $QUERY = "SELECT ADL.USUARIO, CV.MATERIA, CV.COD_GRUPO, 
                            CV.GRUPO, ADL.FECHA, CV.DOCENTE,
                            CV.LUNES, CV.MARTES, CV.MIERCOLES, CV.JUEVES, CV.VIERNES, CV.SABADO
                    FROM   BdClases_online.dbo.TB_ASISTENCIA_DOCENTE_EN_LINEA ADL
                        INNER JOIN BdAcademico.dbo.CAMPUSVIRTUAL CV ON (((ADL.USUARIO=CV.IDENTIFICACION COLLATE Modern_Spanish_CI_AS) 
                        AND (ADL.PLECTIVO=CV.COD_PLECTIVO COLLATE Modern_Spanish_CI_AS)) 
                        AND (ADL.MATERIA=CV.COD_MATERIA COLLATE Modern_Spanish_CI_AS)) 
                        AND (ADL.GRUPO=CV.COD_GRUPO COLLATE Modern_Spanish_CI_AS)
                    WHERE  ADL.USUARIO = :CI
                      AND CV.CALENDARIO IN (1,2) AND ADL.FECHA >= '16-05-2021'
                    ORDER BY ADL.USUARIO, CV.GRUPO, CV.MATERIA";
        $stmt = Conexion::conectar1()->prepare($QUERY);
        $stmt -> bindParam(":CI", $Cedula, PDO::PARAM_STR);
        $stmt -> execute();
        $DataCollection = $stmt ->fetchAll(PDO::FETCH_ASSOC);
        if($DataCollection)
        {    return array('status'=>1,'message'=>$DataCollection);   }
        else
        {   return array('status'=>0,'message'=>"NO HAY INFORMACIÓN DEL USUARIO SOLICITADO"); }
    }
}
class VistaConsulta{
    static public function viewPintarTablaReporte($DataCollection)
    {   $HTML = '
            <div class="card card-borrar"> 
                <h5 class="card-header text-center">Resultados de búsqueda</h5>
                <div class="card-body form-horizontal">';
                foreach($DataCollection as $Row){
                  $HTML.= '<div class="form-group row center-items">
                              <label class="col-sm-12 col-xs-12 col-form-label font-weight-bold"><b>DOCENTE</b> '.$Row['USUARIO'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$Row['DOCENTE'].'</label>
                              <label class="col-sm-12 col-xs-12 col-form-label font-weight-bold"><b>GRUPO</b> '.$Row['GRUPO'].'</label>
                              <label class="col-sm-12 col-xs-12 col-form-label font-weight-bold"><b>MATERIA</b> '.$Row['MATERIA'].'</label>
                          </div>
                          <div class="form-group row col-sm-12 col-md-12">
                            <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                              <thead>
                                <tr>
                                  <th class="text-center">LUNES</br>'.$Row['LUNES'].'</th>
                                  <th class="text-center">MARTES</br>'.$Row['MARTES'].'</th>
                                  <th class="text-center">MIERCOLES</br>'.$Row['MIERCOLES'].'</th>
                                  <th class="text-center">JUEVES</br>'.$Row['JUEVES'].'</th>
                                  <th class="text-center">VIERNES</br>'.$Row['VIERNES'].'</th>
                                  <th class="text-center">SABADO</br>'.$Row['SABADO'].'</th>
                                </tr> 
                              </thead> 
                              <tbody>';
                                foreach($Row['DATOS'] as $Reg){
                                    $HTML.= '<tr>';
                                    if($Reg['DIASEMANA']==1){
                                      $HTML.= '<td>'.$Reg['FECHA'].'</td>';
                                      $HTML.='<td></td><td></td><td></td><td></td><td></td>';
                                    }
                                    if($Reg['DIASEMANA']==2){
                                      $HTML.= '<td></td><td>'.$Reg['FECHA'].'</td>';
                                      $HTML.='<td></td><td></td><td></td><td></td>';
                                    }
                                    if($Reg['DIASEMANA']==3){
                                      $HTML.= '<td></td><td></td><td>'.$Reg['FECHA'].'</td>';
                                      $HTML.='<td></td><td></td><td></td>';
                                    }
                                    if($Reg['DIASEMANA']==4){
                                      $HTML.='<td></td><td></td><td></td>';
                                      $HTML.= '<td>'.$Reg['FECHA'].'</td><td></td><td></td>';
                                    }
                                    if($Reg['DIASEMANA']==5){
                                      $HTML.='<td></td><td></td><td></td><td></td>';
                                      $HTML.= '<td>'.$Reg['FECHA'].'</td><td></td>';
                                    }
                                    if($Reg['DIASEMANA']==6){
                                      $HTML.='<td></td><td></td><td></td><td></td><td></td>';
                                      $HTML.= '<td>'.$Reg['FECHA'].'</td>';
                                    }
                                    $HTML.= '</tr>';
                                }
                          $HTML.= '</tbody>';
                      $HTML.= '</table>
                        </div>';
                }  
                        $HTML.= '
                  </div>
                </div>
              ';
        return $HTML;
    }
}
?>