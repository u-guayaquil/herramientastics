<?php
class MenuPrincipal{
    public function ObtenerMenu(){
        $datosSesion = new InicioSesionResponse();
        $datosSesion = unserialize($_SESSION["SsFichaEstudiante"]);

        $url = "https://servicioenlinea.ug.edu.ec/AdminAPI/api/Usuario/ModuloUsuario/";
        $datos = array( 'usuarioId'     => $datosSesion->UsuarioSIUG,
                        'sistemaId'     => $datosSesion->Sistema,
                        'perfilId'      => $datosSesion->PerfilId);
        $data_json = json_encode($datos);
        $opciones = array('http' => array(
                                        'method'  => 'POST',
                                        'header'  => 'Content-type: application/json',
                                        'content' => $data_json));
        $contexto  = stream_context_create($opciones);
        $resAPI = file_get_contents($url, false, $contexto);
        $verificarAPI = json_decode($resAPI);
        echo json_encode($verificarAPI);
    }
    public function ObtenerOpciones($moduloId){
        $datosSesion = new InicioSesionResponse();
        $datosSesion = unserialize($_SESSION["SsFichaEstudiante"]);
        $contador = 1;

        $url = "https://servicioenlinea.ug.edu.ec/AdminAPI/api/Usuario/MenuUsuario/";
        $datos = array( 'usuarioId'   => $datosSesion->UsuarioSIUG,
                        'sistemaId'   => $datosSesion->Sistema,
                        'perfilId'    => $datosSesion->PerfilId,
                        'moduloId'    => $moduloId                   
                    );
        $data_json = json_encode($datos);
        $opciones = array('http' => array(
                                        'method'  => 'POST',
                                        'header'  => 'Content-type: application/json',
                                        'content' => $data_json));
        $contexto  = stream_context_create($opciones);
        $resAPI = file_get_contents($url, false, $contexto);
        $validarAPI = json_decode($resAPI);
        echo json_encode($validarAPI);
    }
}
class InicioSesionResponse{
    public $Id;
    public $Token;
    public $Respuesta;
    public $Leyenda;
    public $UsuarioSIUG;
    public $Correo;
    public $SistemaId;
    public $Perfil;
    public $PerfilId;
}
?>