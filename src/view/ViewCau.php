<!DOCTYPE html> 
<meta charset="UTF-8">
<html> 	
<head>
<!-- Metas -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HERRAMIENTAS - UG</title>
<link  rel="icon"   href="img/favicon.png" type="image/png" />
<!-- Bootstrap core CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>   
<!-- CSS Personalizable -->
<link href="css/style.css" rel="stylesheet">	
  <!-- FontAwesome -->
  <link href="plugins/fontawesome/css/all.css" rel="stylesheet">	
</head>
<body>
<div class="col-md-12 center-items row hijo">
    <div class="col-md-12 center-items row" style="margin-top:25px;">

            <div class="col-md-12 center-items">
                <a href="./" class="center" style="text-decoration:none">
                    <img src="img/LogosUG.png" class="card-img-top" alt="Logo UG" style="width: 200px;">
                    <h2 class="center">HERRAMIENTAS TICS - UG</h2>
                </a> 
            </div>

    </div>
    <div class="col-md-12 center row hijo">
        <div class="col-md-4  center-items">
            <a href="apps/estudiantes/" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/d-excel.png" class="card-img-top" alt="descarga de excel" style="width: 210px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Listado de Estudiantes</h5>
                        <p class="card-text" style="color: #000;">Listado de estudiantes en formato excel. Moodle (Campus Virtual - UG)</p>
                        <a href="apps/estudiantes/" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 center-items">
            <a href="./horarios/" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/horarios.png" class="card-img-top" alt="horarios" style="width: 210px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Consultar Horarios</h5>
                        <p class="card-text" style="color: #000;">Consulta de horarios de clase de Docentes o Aulas Virtuales (Cedia Zoom - UG)</p>
                        <a href="./horarios/" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 center-items">
            <a href="./zonadescargas" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/Descarga.png" class="card-img-top" alt="Zona de descarga" style="width: 210px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Zona de Descarga</h5>
                        <p class="card-text" style="color: #000;">Archivos varios - Universidad de Guayaquil</p>
                        <a href="./zonadescargas" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
</body>
</html>