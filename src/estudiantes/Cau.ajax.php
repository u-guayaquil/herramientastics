<?php
require_once "VistaCau.php";
require_once "ModelCau.php";
class AjaxRevision{
    public $CI;
    public function ajaxPintarXCarrera(){
        $respuesta = VistaCau::PintarXCarrera();
        echo json_encode($respuesta);
    }
    public function ajaxPintarXMateria(){
        $respuesta = VistaCau::PintarXMateria();
        echo json_encode($respuesta);
    }
    public function ajaxPintarXEstudiante(){
        $respuesta = VistaCau::PintarXEstudiante();
        echo json_encode($respuesta);
    }
    public $CodFacultad;
    public function ajaxBuscarCarrerasByFacultad(){
        $respuesta = ModelsCau::ObtenerCarrerasByFacultad($this->CodFacultad);
        echo json_encode($respuesta);
    }
    public $CodCarrera;
    public function ajaxBuscarParalelosByCarrera(){
        $respuesta = ModelsCau::ObtenerParalelosByCarrera($this->CodCarrera);
        echo json_encode($respuesta);
    }
    public $CodParalelo;
    public function ajaxBuscarMateriasByParalelo(){
        $respuesta = ModelsCau::ObtenerMateriasByParalelo($this->CodParalelo);
        echo json_encode($respuesta);
    }
}
/*====================================
        PINTAR X CARRERA
 ====================================*/
 if (isset($_POST["Carrera"])){
    $Servidores = new AjaxRevision();
    $Servidores -> ajaxPintarXCarrera();
}
/*====================================
        PINTAR X MATERIAS
 ====================================*/
 if (isset($_POST["Materia"])){
    $Servidores = new AjaxRevision();
    $Servidores -> ajaxPintarXMateria();
}
/*====================================
        PINTAR X ESTUDIANTE
 ====================================*/
 if (isset($_POST["Estudiante"])){
    $Servidores = new AjaxRevision();
    $Servidores -> ajaxPintarXEstudiante();
}
/*====================================
        BUSCAR CARRERA BY FACULTAD
 ====================================*/
 if (isset($_POST["CodFacultad"])){
    $Servidores = new AjaxRevision();
    $Servidores -> CodFacultad = $_POST["CodFacultad"];
    $Servidores -> ajaxBuscarCarrerasByFacultad();
}
/*====================================
        BUSCAR PARALELO BY CARRERA
 ====================================*/
 if (isset($_POST["CodCarrera"])){
    $Servidores = new AjaxRevision();
    $Servidores -> CodCarrera = $_POST["CodCarrera"];
    $Servidores -> ajaxBuscarParalelosByCarrera();
}
/*====================================
        BUSCAR MATERIAS BY PARALELO
 ====================================*/
 if (isset($_POST["CodParalelo"])){
    $Servidores = new AjaxRevision();
    $Servidores -> CodParalelo = $_POST["CodParalelo"];
    $Servidores -> ajaxBuscarMateriasByParalelo();
}
/*====================================
        EXPORTAR
 ====================================*/
 if (isset($_POST["Accion"])){
    ControllerCau::ExportarExcel();
}