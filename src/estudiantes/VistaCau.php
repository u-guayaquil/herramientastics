<?php
class VistaCau{
        public static function PintarXCarrera(){
            $HTML = '<div class="form-group row center">
                            <div class="col-md-3"></div> 
                            <div class="col-md-6 center">
                                <label><b>FACULTAD: </b></label>
                                <select  name="Facultad" id="Facultad" class="form-control mb-3" onchange="return ChangeSelectFacultad();" required>
                                    <option value="" selected> SELECCIONE FACULTAD</option>
                                    <option value="01">JURISPRUDENCIA CIENCIAS SOCIALES Y POLITICAS</option>
                                    <option value="21">FACULTAD DE CIENCIAS PARA EL DESARROLLO</option>
                                    <option value="26">ADMINISTRACIÓN CENTRAL</option>
                                    <option value="13">INGENIERIA QUIMICA</option>
                                    <option value="10">ODONTOLOGIA</option>
                                    <option value="08">CIENCIAS ECONOMICAS</option>
                                    <option value="07">CIENCIAS QUIMICAS</option>
                                    <option value="03">CIENCIAS MATEMATICAS Y FISICAS</option>
                                    <option value="12">ARQUITECTURA Y URBANISMO</option>
                                    <option value="20">CIENCIAS AGRARIAS</option>
                                    <option value="02">CIENCIAS MEDICAS</option>
                                    <option value="04">FILOSOFIA, LETRAS Y CIENCIAS DE LA EDUCACION</option>
                                    <option value="14">COMUNICACION SOCIAL</option>
                                    <option value="09">CIENCIAS ADMINISTRATIVAS</option>
                                    <option value="15">INGENIERIA INDUSTRIAL</option>
                                    <option value="17">MEDICINA VETERINARIA Y ZOOTECNIA</option>
                                    <option value="16">CIENCIAS PSICOLOGICAS</option>
                                    <option value="19">EDUCACION FISICA DEPORTE Y RECREACION</option>
                                    <option value="11">CIENCIAS NATURALES</option>
                                </select>      
                            </div>
                        </div>
                        <div class="form-group row center">
                            <div class="col-md-3"></div> 
                            <div class="col-md-6 center">
                                <label><b>CARRERA: </b></label>
                                <select  name="Carrera" id="Carrera" class="form-control mb-3" onchange="return ChangeSelectCarrera();" required>
                                    <option value="" selected>SELECCIONE CARRERA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">				
                            <div class="col-md-4"></div>
                            <div class="col-md-4 center">
                                <input type="submit" name="insert" class="btn btn-success" value="EXPORTAR EXCEL" onclick="return ExportarExcel();"><br/>
                            </div>
                        </div>
                    ';
            return $HTML;
        }

        public static function PintarXMateria(){
            $HTML = '<div class="form-group row center">
                            <div class="col-md-3"></div> 
                            <div class="col-md-6 center">
                                <label><b>FACULTAD: </b></label>
                                <select  name="Facultad" id="Facultad" class="form-control mb-3" onchange="return ChangeSelectFacultad();" required>
                                    <option value="" selected>SELECCIONE FACULTAD</option>
                                    <option value="01">JURISPRUDENCIA CIENCIAS SOCIALES Y POLITICAS</option>
                                    <option value="21">FACULTAD DE CIENCIAS PARA EL DESARROLLO</option>
                                    <option value="26">ADMINISTRACIÓN CENTRAL</option>
                                    <option value="13">INGENIERIA QUIMICA</option>
                                    <option value="10">ODONTOLOGIA</option>
                                    <option value="08">CIENCIAS ECONOMICAS</option>
                                    <option value="07">CIENCIAS QUIMICAS</option>
                                    <option value="03">CIENCIAS MATEMATICAS Y FISICAS</option>
                                    <option value="12">ARQUITECTURA Y URBANISMO</option>
                                    <option value="20">CIENCIAS AGRARIAS</option>
                                    <option value="02">CIENCIAS MEDICAS</option>
                                    <option value="04">FILOSOFIA, LETRAS Y CIENCIAS DE LA EDUCACION</option>
                                    <option value="14">COMUNICACION SOCIAL</option>
                                    <option value="09">CIENCIAS ADMINISTRATIVAS</option>
                                    <option value="15">INGENIERIA INDUSTRIAL</option>
                                    <option value="17">MEDICINA VETERINARIA Y ZOOTECNIA</option>
                                    <option value="16">CIENCIAS PSICOLOGICAS</option>
                                    <option value="19">EDUCACION FISICA DEPORTE Y RECREACION</option>
                                    <option value="11">CIENCIAS NATURALES</option>
                                </select>      
                            </div>
                        </div>
                        <div class="form-group row center">
                            <div class="col-md-3"></div> 
                            <div class="col-md-6 center">
                                <label><b>CARRERA: </b></label>
                                <select  name="Carrera" id="Carrera" class="form-control mb-3" onchange="return ChangeSelectCarrera();" required>
                                    <option value="" selected>SELECCIONE CARRERA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row center">
                            <div class="col-md-3"></div> 
                            <div class="col-md-6 center">
                                <label><b>PARALELO: </b></label>
                                <select  name="Paralelo" id="Paralelo" class="form-control mb-3" onchange="return ChangeSelectParalelo();" required>
                                    <option value="" selected>SELECCIONE PARALELO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row center">
                            <div class="col-md-3"></div> 
                            <div class="col-md-6 center">
                                <label><b>MATERIA: </b></label>
                                <select  name="Materia" id="Materia" class="form-control mb-3" onchange="return ChangeSelectMateria();" required>
                                    <option value="" selected>SELECCIONE MATERIA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">				
                            <div class="col-md-4"></div>
                            <div class="col-md-4 center">
                                <input type="submit" name="insert" class="btn btn-success" value="EXPORTAR EXCEL" onclick="return ExportarExcel();"><br/>
                            </div>
                        </div>
                    ';
            return $HTML;
        }

        public static function PintarXEstudiante(){
            $HTML = '<div class="form-group row center">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <label><b>CÉDULA DEL ESTUDIANTE</b></label>
                                <input type="text" name="CodEstudiante" id="CodEstudiante" class="form-control form-input mb-3" style="padding:20px;" autocomplete="off" required><br />
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        <div class="form-group row">				
                            <div class="col-md-4"></div>
                            <div class="col-md-4 center">
                                <input type="submit" name="insert" class="btn btn-success" value="EXPORTAR EXCEL" onclick="return ExportarExcel();"><br/>
                            </div>
                        </div>
                    ';
            return $HTML;
        }
}
?>