<?php
    require_once "ModelCau.php";
    require_once "../utils/utils.php";
    class ControllerCau{
        public static function ExportarExcel(){
            if(isset($_POST['Tipo']) && $_POST['Tipo'] == '1'){
                if(isset($_POST['Carrera']) && $_POST['Carrera'] != '')
                {   $Datos = array('CodFacultad'=>$_POST['Facultad'],
                                    'CodCarrera'=>$_POST['Carrera']             
                                );
                    $ModeloCau = new ModelsCau();
                    $DataCollection = $ModeloCau->ObtenerEstudiantesByCarrera($Datos);
                        if($DataCollection){
                            Utils::GenerarExcel($DataCollection,$_POST['Carrera']);
                        }
                        else{
                            echo '  <script>
                                        alert("¡No hay estudiantes registrados en la carrera seleccionada!");
                                        window.location.href="https://herramientastics.ug.edu.ec/principal.php";
                                    </script>';
                           // Utils::AlertaBasica('No hay estudiantes registrados en la carrera seleccionada','info','CARRERA SIN ESTUDIANTES');
                        }  
                }
            }
            if(isset($_POST['Tipo']) && $_POST['Tipo'] == '2'){
                if(isset($_POST['Materia']) && $_POST['Materia'] != '')
                {   $Datos = array("CodFacultad"=>$_POST['Facultad'],
                                    "CodCarrera"=>$_POST['Carrera'],
                                    "CodParalelo"=>$_POST['Paralelo'],
                                    "CodMateria"=>$_POST['Materia']               
                                );
                    $ModeloCau = new ModelsCau();
                    $DataCollection = $ModeloCau->ObtenerEstudiantesByMaterias($Datos);

                    if($DataCollection){
                        Utils::GenerarExcel($DataCollection,$_POST['Materia']);
                    }
                    else{
                        //Utils::AlertaBasica('No hay estudiantes registrados en la materia seleccionada 2','info','MATERIA SIN ESTUDIANTES');
                        echo '  <script>
                                    alert("¡No hay estudiantes registrados en la materia seleccionada!");
                                    window.location.href="https://herramientastics.ug.edu.ec/principal.php";
                                </script>';

                    }                   
                }
            }
            if(isset($_POST['Tipo']) && $_POST['Tipo'] == '3'){
                if(isset($_POST['CodEstudiante']) && $_POST['CodEstudiante'] != '')
                {   $ModeloCau = new ModelsCau();
                    $DataCollection = $ModeloCau->ObtenerMateriaEstudiante($_POST['CodEstudiante']);

                    if($DataCollection){
                        Utils::GenerarExcel($DataCollection,$_POST['CodEstudiante']);
                    }
                    else{
                        echo '  <script>
                                    alert("¡No hay materias encontradas para el estudiante que ingreso!");
                                    window.location.href="https://herramientastics.ug.edu.ec/principal.php";
                                </script>';
                        //Utils::AlertaBasica('No hay materias encontradas para el estudiante que ingreso','info','ESTUDIANTE SIN MATERIAS');
                    }  
                }
            }
        }
    }
?>