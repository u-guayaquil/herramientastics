<?php
    class ModelsCau{
        public static function ObtenerMateriaEstudiante($COD_ESTUDIANTE){
            include "../utils/conexion.php";
            $DataCollection = [];
            $QUERY = "SELECT EM.COD_ESTUDIANTE AS username, D.APELLIDO AS lastname, D.NOMBRE AS firstname,
                     LTRIM(RTRIM(EM.COD_PLECTIVO))+ '_' + LTRIM(RTRIM(EM.COD_GRUPO))+ '_' + LTRIM(RTRIM(EM.COD_MATERIA)) AS course1,
                     IIF(D.EMAIL = '...' OR D.EMAIL IS NULL, IIF(D.PERSONAL_EMAIL = '', D.COD_ESTUDIANTE+'@ug.edu.ec' , D.PERSONAL_EMAIL ) , D.EMAIL) EMAIL
                        FROM TB_ESTUDIANTE_MATERIA EM, TB_ESTUDIANTE_DPERSONAL D
                        WHERE EM.COD_ESTUDIANTE = '$COD_ESTUDIANTE'
                        AND EM.COD_ESTUDIANTE = D.COD_ESTUDIANTE
                        AND EM.ESTADO = ' ' 
                        AND EM.COD_PLECTIVO IN (SELECT COD_PLECTIVO 
                                                    FROM TB_PLECTIVO 
                                                    WHERE DESCRIPCION IN ('2021 - 2022','2021 - 2022 CII','2021 - 2022 CMP2','2021 - 2022 ING2')
                                                )";
            $ejecutar = sqlsrv_query($con, $QUERY);
            while ($row = sqlsrv_fetch_array($ejecutar)){
                $DataCollection[] = $row;
            }
            sqlsrv_close($con);
            return $DataCollection;
        }

        public static function ObtenerEstudiantesByCarrera($DATOS){
            include "../utils/conexion.php";
            $CODFACULTAD = $DATOS['CodFacultad'];
            $CODCARRERA= $DATOS['CodCarrera'];
            $DataCollection = [];
            $QUERY = "SELECT EM.COD_ESTUDIANTE AS username, D.APELLIDO AS lastname, D.NOMBRE AS firstname, 
                            LTRIM(RTRIM(EM.COD_PLECTIVO))+ '_' + LTRIM(RTRIM(EM.COD_GRUPO))+ '_' + LTRIM(RTRIM(EM.COD_MATERIA)) AS course1,
                            IIF(D.EMAIL = '...' OR D.EMAIL IS NULL, IIF(D.PERSONAL_EMAIL = '', D.COD_ESTUDIANTE+'@ug.edu.ec' , D.PERSONAL_EMAIL ) , D.EMAIL) EMAIL
                        FROM TB_ESTUDIANTE_MATERIA EM INNER JOIN TB_ESTUDIANTE_DPERSONAL D ON EM.COD_ESTUDIANTE = D.COD_ESTUDIANTE
                            INNER JOIN TB_CARRERA CA ON SUBSTRING(COD_ESTU_CARRE,LEN(EM.COD_ESTUDIANTE)+1,LEN(COD_ESTU_CARRE)) = COD_CARRERA
                            INNER JOIN TB_FACULTAD FA ON CA.COD_FACULTAD = FA.COD_FACULTAD
                        WHERE EM.ESTADO = ' ' 
                        AND FA.COD_FACULTAD = '$CODFACULTAD'
                        AND CA.COD_CARRERA = '$CODCARRERA'
                        AND EM.COD_PLECTIVO IN (SELECT COD_PLECTIVO FROM TB_PLECTIVO WHERE DESCRIPCION IN ('2021 - 2022','2021 - 2022 CII','2021 - 2022 CMP2','2021 - 2022 ING2') )
                        ORDER BY D.APELLIDO";
                        //print_r($QUERY);
            $ejecutar = sqlsrv_query($con, $QUERY);    
            while ($row = sqlsrv_fetch_array($ejecutar)){
                $DataCollection[] = $row;
            }
            sqlsrv_close($con);
            return $DataCollection;
        }

        public static function ObtenerEstudiantesByMaterias($DATOS){
            include "../utils/conexion.php";
            $CODFACULTAD = $DATOS['CodFacultad'];
            $CODCARRERA= $DATOS['CodCarrera'];
            $CODPARALELO = $DATOS['CodParalelo'];
            $CODMATERIA= $DATOS['CodMateria'];
            $DataCollection = [];
            $QUERY = "SELECT EM.COD_ESTUDIANTE AS username, D.APELLIDO AS lastname, D.NOMBRE AS firstname, 
                            LTRIM(RTRIM(EM.COD_PLECTIVO))+ '_' + LTRIM(RTRIM(EM.COD_GRUPO))+ '_' + LTRIM(RTRIM(EM.COD_MATERIA)) AS course1,
                            IIF(D.EMAIL = '...' OR D.EMAIL IS NULL, IIF(D.PERSONAL_EMAIL = '', D.COD_ESTUDIANTE+'@ug.edu.ec' , D.PERSONAL_EMAIL ) , D.EMAIL) EMAIL
                            
                        FROM TB_ESTUDIANTE_MATERIA EM INNER JOIN TB_ESTUDIANTE_DPERSONAL D ON EM.COD_ESTUDIANTE = D.COD_ESTUDIANTE
                            INNER JOIN TB_CARRERA CA ON SUBSTRING(COD_ESTU_CARRE,LEN(EM.COD_ESTUDIANTE)+1,LEN(COD_ESTU_CARRE)) = COD_CARRERA
                            INNER JOIN TB_FACULTAD FA ON CA.COD_FACULTAD = FA.COD_FACULTAD
                        WHERE EM.ESTADO = ' ' 
                        AND FA.COD_FACULTAD = '$CODFACULTAD'
                        AND CA.COD_CARRERA = '$CODCARRERA'
                        AND EM.COD_MATERIA = CASE WHEN '$CODMATERIA' = '0' THEN EM.COD_MATERIA ELSE '$CODMATERIA' END
                        AND EM.COD_GRUPO = CASE WHEN '$CODPARALELO  ' = '0' THEN EM.COD_GRUPO ELSE '$CODPARALELO' END
                        AND EM.COD_PLECTIVO IN (SELECT COD_PLECTIVO FROM TB_PLECTIVO WHERE DESCRIPCION IN ('2021 - 2022','2021 - 2022 CII','2021 - 2022 CMP2','2021 - 2022 ING2') )
                        ORDER BY D.APELLIDO";
                       //print_r($QUERY);
            $ejecutar = sqlsrv_query($con, $QUERY);
            while ($row = sqlsrv_fetch_array($ejecutar)){
                $DataCollection[] = $row;
            }
            sqlsrv_close($con);
            return $DataCollection;
        }

        public static function ObtenerCarrerasByFacultad($CodFacultad){
            include "../utils/conexion.php";
            $DataCollection = [];
            $QUERY = "SELECT DISTINCT  LTRIM(RTRIM(LEFT(COD_PLECTIVO,LEN(COD_PLECTIVO)-4))) CARRERA,B.NOMBRE 
                        FROM TB_ESTUDIANTE_MATERIA A,TB_CARRERA B 
                        WHERE B.COD_CARRERA=LEFT(COD_PLECTIVO,LEN(COD_PLECTIVO)-4) 
                        --AND A.ESTADO = ' ' 
                        AND LEFT(A.COD_PLECTIVO,2)='$CodFacultad'";
            $ejecutar = sqlsrv_query($con, $QUERY);
            while ($row = sqlsrv_fetch_array($ejecutar)){
                $DataCollection[] = $row;
            }
            sqlsrv_close($con);
            return $DataCollection;
        }

        public static function ObtenerParalelosbyCarrera($CodCarrera){
            include "../utils/conexion.php";
            $DataCollection = [];
            $QUERY = "SELECT DISTINCT  LTRIM(RTRIM(A.COD_GRUPO)) AS PARALELO,B.DESCRIPCION AS NOMBRE
                        FROM TB_ESTUDIANTE_MATERIA A,TB_GRUPO B 
                        WHERE A.COD_GRUPO=B.COD_GRUPO AND B.COD_CARRERA=LEFT(COD_PLECTIVO,LEN(COD_PLECTIVO)-4) 
                        AND A.ESTADO = ' ' AND LEFT(COD_PLECTIVO,LEN(COD_PLECTIVO)-4)='$CodCarrera'";
            $ejecutar = sqlsrv_query($con, $QUERY);
            while ($row = sqlsrv_fetch_array($ejecutar)){
                $DataCollection[] = $row;
            }
            sqlsrv_close($con);
            return $DataCollection;
        }

        public static function ObtenerMateriasByParalelo($CodParalelo){
            include "../utils/conexion.php";
            $DataCollection = [];
            $QUERY = "SELECT DISTINCT  LTRIM(RTRIM(B.COD_MATERIA)) AS MATERIA,B.NOMBRE 
                        FROM TB_ESTUDIANTE_MATERIA A,TB_MATERIA B 
                        WHERE A.COD_MATERIA=B.COD_MATERIA AND A.ESTADO = ' ' 
                        AND A.COD_GRUPO='$CodParalelo'";
            $ejecutar = sqlsrv_query($con, $QUERY);
            while ($row = sqlsrv_fetch_array($ejecutar)){
                $DataCollection[] = $row;
            }
            sqlsrv_close($con);
            return $DataCollection;
        }
    }
?>