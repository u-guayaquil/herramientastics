<?php
require "utils/conexionPdo.php";
ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] . '/herramientas/sesiones');
session_start();
class InicioSesion{
    public $Usuario;
    public $Clave;
    public $IpPublica;

    public function ValidaCredenciales(){
        $respuesta = new InicioSesionResponse();

        $url = "https://servicioenlinea.ug.edu.ec/Microsoft365API/api/SesionAzure/LoginHerramientaTic";
        $datos = array( 'ip'        => $this->IpPublica,
                        'usuario'   => $this->Usuario,
                        'clave'     => $this->Clave);
        $data_json = json_encode($datos);
        $opciones = array('http' => array(
                                        'method'  => 'POST',
                                        'header'  => 'Content-type: application/json',
                                        'content' => $data_json));
        $contexto  = stream_context_create($opciones);
        $resAPI = file_get_contents($url, false, $contexto);
        $verificarAPI = json_decode($resAPI);
        $respuesta->Id = $verificarAPI->id;
        $respuesta->Token = $verificarAPI->token;
        $respuesta->Respuesta = $verificarAPI->respuesta;
        $respuesta->Leyenda = $verificarAPI->leyenda;
        $respuesta->UsuarioSIUG = $verificarAPI->usuario;
        $respuesta->Correo = $this->Usuario;
        $respuesta->IpPublica = $this->IpPublica;
        $respuesta->IpPrivada = $this->ObtenerIpRemota();

        if($verificarAPI->respuesta == "OK"){
            $respuesta = $this->ValidaAccesoSistema($respuesta);
        }
        if($respuesta->Respuesta == "OK"){
            $PrepareSQL = array('USUARIO'=>str_replace(' ','',$respuesta->UsuarioSIUG),
								'APLICACION'=>'13',
                                'CORREO'=>$respuesta->Correo);
            $response = $this->ValidaTokenAcuerdo($PrepareSQL);
            if($response["message"][0]["ExisteAcuerdo"]==0){
                $GeneraToken = $this->GeneraTokenAcuerdo($PrepareSQL);
                $URL = "https://servicioenlinea.ug.edu.ec/SIUGV2/Acuerdo?token=".$GeneraToken["message"][0]["TOKEN"];
                $respuesta -> Respuesta = 'TOKEN';
                $respuesta -> URL = $URL;
            }
        }
        echo json_encode($respuesta);
    }
    private function ValidaAccesoSistema($respuesta){
        $url = "https://servicioenlinea.ug.edu.ec/AdminAPI/api/Usuario/InicioSesion/";
        $datos = array( 'usuarioId'   => $respuesta->UsuarioSIUG,
                        'sistemaId'   => 'HERRAMIENTASTICS');
        $data_json = json_encode($datos);
        $opciones = array('http' => array(
                                        'method'  => 'POST',
                                        'header'  => 'Content-type: application/json',
                                        'content' => $data_json));
        $contexto  = stream_context_create($opciones);
        $resAPI = file_get_contents($url, false, $contexto);
        $validarAPI = json_decode($resAPI);

        if($validarAPI->mensaje == "OK"){
            $respuesta->Sistema = $validarAPI->objetos[0]->sistemaId;
            $respuesta->Perfil = $validarAPI->objetos[0]->perfil;
            $respuesta->PerfilId = $validarAPI->objetos[0]->perfilId;            
        }
        else{
            $respuesta->Respuesta =  "ERROR ACCESO";
            $respuesta->Leyenda = $validarAPI->mensaje;
        }

        if($respuesta->Respuesta == "OK"){
            $_SESSION["SsFichaEstudiante"] = serialize($respuesta);
        }

        return $respuesta;
    }
    private function ObtenerIpRemota(){       
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
        else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		    return $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            return $_SERVER['REMOTE_ADDR'];
    }
    private function ValidaTokenAcuerdo($PrepareSQL){
        $DataCollection = [];
        $DATA = '{"USUARIO":"'.$PrepareSQL['USUARIO'].'","SISTEMA":"'.$PrepareSQL['APLICACION'].'"}';
		$PROCESO = 'VALIDA_EXISTENCIA_ACUERDO';
		$SQL = "EXEC BD_SIUG.dbo.SIUG_CONSULTAS_VARIAS '$PROCESO', '$DATA'";
        $stmt = Conexion::conectar1()->prepare($SQL);
        $stmt -> execute();
        $DataCollection = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if($DataCollection)
        {    return array('status'=>1,'message'=>$DataCollection);   }
        else
        {   return array('status'=>0,'message'=>"NO HAY INFORMACIÓN DEL USUARIO SOLICITADO"); }
    }
    private function GeneraTokenAcuerdo($PrepareSQL){
        $DataCollection = [];
        $DATA = '{"USUARIO":"'.$PrepareSQL['USUARIO'].'","CORREO":"'.$PrepareSQL['CORREO'].'","APLICACION":"'.$PrepareSQL['APLICACION'].'"}';
        $PROCESO = 'GENERA_TOKEN_ACUERDO';
        $SQL = "EXEC BD_SIUG.dbo.SIUG_CONSULTAS_VARIAS '$PROCESO', '$DATA'";
        $stmt = Conexion::conectar1()->prepare($SQL);
        $stmt -> execute();
        $DataCollection = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if($DataCollection)
        {    return array('status'=>1,'message'=>$DataCollection);   }
        else
        {   return array('status'=>0,'message'=>"NO HAY INFORMACIÓN DEL USUARIO SOLICITADO"); }


    }
}
class InicioSesionResponse{
    public $Id;
    public $Token;
    public $Respuesta;
    public $Leyenda;
    public $UsuarioSIUG;
    public $Correo;
    public $IpPublica;
    public $IpPrivada;
    public $SistemaId;
    public $Perfil;
    public $PerfilId;
}

if (isset($_POST["transaccion"])){
    $LoginFicha = new InicioSesion();
    $LoginFicha -> Usuario = $_POST["usuario"];
    $LoginFicha -> Clave = $_POST["clave"];
    $LoginFicha -> IpPublica = $_POST["ip"];
    $LoginFicha -> ValidaCredenciales();
}
?>