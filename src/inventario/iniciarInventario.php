<?php
    class InventarioTIC{
        public $Id;
        public $TipoAccion;
        public $TipoId;        
        public $FacultadId;
        public $Edificio;
        public $Departamento;
        public $MarcaId;
        public $Modelo;
        public $Serie;
        public $CodigoInventario;
        public $Responsable;        
        public $AdministradorEdificio;        
        public $EstadoId;
        public $EstadoRegistro;
        public $IpPublica;
        public $IpPrivada;                
        public $Transaccion;
        public $datosSesion;

        public function __construct() {
            $this->datosSesion = new InicioSesionResponse();
            $this->datosSesion = unserialize($_SESSION["SsFichaEstudiante"]);
        }
        public function ObtenerCatalogo(){
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $TipoAccion = $this->TipoAccion;
            $PerfilId = $this->datosSesion->PerfilId;
            $Transaccion = $this->Transaccion;

            $url = "http://10.87.151.38/InventarioTicAPI/api/Consulta/ObtenerCatalogo/";
            $datos = array( 'UsuarioId'     => $UsuarioID,
                            'TipoAccion'    => $TipoAccion,
                            'PerfilId'      => $PerfilId,
                            'Transaccion'   => $Transaccion
                            );
            
            $data_json = json_encode($datos);
            $opciones = array('http' => array(
                                            'method'  => 'POST',
                                            'header'  => 'Content-type: application/json',
                                            'content' => $data_json));
            $contexto  = stream_context_create($opciones);
            $resAPI = file_get_contents($url, false, $contexto);
            $verificarAPI = json_decode($resAPI);
            echo json_encode($verificarAPI);
        }
        public function ObtenerArticulos(){
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $TipoAccion = $this->TipoAccion;
            $PerfilId = $this->datosSesion->PerfilId;
            $FacultadId = $this->FacultadId;
            $EstadoId = $this->EstadoId;
            $Transaccion = $this->Transaccion;

            $url = "http://10.87.151.38/InventarioTicAPI/api/Consulta/ObtenerRegistros/";
            $datos = array( 'UsuarioId'     => $UsuarioID,
                            'TipoAccion'    => $TipoAccion,
                            'PerfilId'      => $PerfilId,
                            'FacultadId'    => $FacultadId,
                            'EstadoId'      => $EstadoId,
                            'Transaccion'   => $Transaccion
                            );
            $data_json = json_encode($datos);
            $opciones = array('http' => array(
                                            'method'  => 'POST',
                                            'header'  => 'Content-type: application/json',
                                            'content' => $data_json));
            $contexto  = stream_context_create($opciones);
            $resAPI = file_get_contents($url, false, $contexto);
            $verificarAPI = json_decode($resAPI);
            echo json_encode($verificarAPI);
        }
        public function ObtenerArticulo(){
            $Id = $this->Id;
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $TipoAccion = $this->TipoAccion;
            $Transaccion = $this->Transaccion;

            $url = "http://10.87.151.38/InventarioTicAPI/api/Consulta/ObtenerRegistro/";
            $datos = array( 'Id'            => $Id,
                            'UsuarioId'     => $UsuarioID,
                            'TipoAccion'    => $TipoAccion,
                            'Transaccion'   => $Transaccion
                            );
            $data_json = json_encode($datos);
            $opciones = array('http' => array(
                                            'method'  => 'POST',
                                            'header'  => 'Content-type: application/json',
                                            'content' => $data_json));
            $contexto  = stream_context_create($opciones);
            $resAPI = file_get_contents($url, false, $contexto);
            $verificarAPI = json_decode($resAPI);
            echo json_encode($verificarAPI);
        }
        public function RegistrarArticulo(){
            $Id = $this->Id;
            $TipoId = $this->TipoId;
            $FacultadId = $this->FacultadId;
            $Edificio = $this->Edificio;
            $Departamento = $this->Departamento;
            $MarcaId = $this->MarcaId;
            $Modelo = $this->Modelo;
            $Serie = $this->Serie;
            $CodigoInventario = $this->CodigoInventario;
            $Responsable = $this->Responsable;
            $AdministradorEdificio = $this->AdministradorEdificio;
            $EstadoId = $this->EstadoId;
            $EstadoRegistro = "1";
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $IpPublica = $this->datosSesion->IpPublica;
            $IpPrivada = $this->datosSesion->IpPrivada;
            $Transaccion = $this->Transaccion;

            try{
                $url = "http://10.87.151.38/InventarioTicAPI/api/Transaccion/SetArticulo/";
                $datos = array( 'Id'                        => $Id,
                                'TipoId'                    => $TipoId,
                                'FacultadId'                => $FacultadId,
                                'Edificio'                  => $Edificio,
                                'Departamento'              => $Departamento,
                                'MarcaId'                   => $MarcaId,
                                'Modelo'                    => $Modelo,
                                'Serie'                     => $Serie,
                                'CodigoInventario'          => $CodigoInventario,
                                'Responsable'               => $Responsable,
                                'AdministradorEdificio'     => $AdministradorEdificio,
                                'EstadoId'                  => $EstadoId,
                                'EstadoRegistro'            => $EstadoRegistro,
                                'UsuarioId'                 => $UsuarioID,
                                'IpPublica'                 => $IpPublica,
                                'IpPrivada'                 => $IpPrivada,
                                'Transaccion'               => $Transaccion
                                );
                
                $data_json = json_encode($datos);
                $opciones = array('http' => array(
                                                'method'  => 'POST',
                                                'header'  => 'Content-type: application/json',
                                                'content' => $data_json));
                $contexto  = stream_context_create($opciones);
                $resAPI = file_get_contents($url, false, $contexto);
                $verificarAPI = json_decode($resAPI);
                echo json_encode($verificarAPI);
            } catch (Exception $e) {
                $objError = new ErrorResponse();
                $objError->Id = 0;
                $objError->Mensaje = "ERROR";
                $objError->Respuesta = $e;

                echo json_encode($objError);
            }
            
        }        
    }
    class InicioSesionResponse{
        public $Id;
        public $Token;
        public $Respuesta;
        public $Leyenda;
        public $UsuarioSIUG;
        public $Correo;
        public $IpPublica;
        public $IpPrivada;
        public $SistemaId;
        public $Perfil;
        public $PerfilId;
    }
    class ErrorResponse{
        public $Mensaje;
        public $Id;
        public $Respuesta;
    }
?>