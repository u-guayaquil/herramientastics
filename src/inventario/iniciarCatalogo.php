<?php
    class CatalogoTIC{
        public $Id;
        public $TipoAccion;
        public $TipoId;        
        public $Nombre;
        public $BusquedaRapida;
        public $Valor;
        public $Valor2;
        public $EstadoId;
        public $EstadoRegistro;
        public $IpPublica;
        public $IpPrivada;                
        public $Transaccion;
        public $datosSesion;

        public function __construct() {
            $this->datosSesion = new InicioSesionResponse();
            $this->datosSesion = unserialize($_SESSION["SsFichaEstudiante"]);
        }
        public function ObtenerCatalogo(){
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $TipoAccion = $this->TipoAccion;
            $PerfilId = $this->datosSesion->PerfilId;
            $Transaccion = $this->Transaccion;

            $url = "http://10.87.151.38/InventarioTicAPI/api/Consulta/ObtenerCatalogo/";
            $datos = array( 'UsuarioId'     => $UsuarioID,
                            'TipoAccion'    => $TipoAccion,
                            'PerfilId'      => $PerfilId,
                            'Transaccion'   => $Transaccion
                            );
            
            $data_json = json_encode($datos);
            $opciones = array('http' => array(
                                            'method'  => 'POST',
                                            'header'  => 'Content-type: application/json',
                                            'content' => $data_json));
            $contexto  = stream_context_create($opciones);
            $resAPI = file_get_contents($url, false, $contexto);
            $verificarAPI = json_decode($resAPI);
            echo json_encode($verificarAPI);
        }
        public function ObtenerTiposCatalogo(){
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $TipoAccion = $this->TipoAccion;
            $PerfilId = $this->datosSesion->PerfilId;
            $TipoId = $this->TipoId;
            $EstadoId = $this->EstadoId;
            $Transaccion = $this->Transaccion;

            $url = "http://10.87.151.38/InventarioTicAPI/api/Consulta/ObtenerTiposCatalogo/";
            //$url = "http://localhost:41394/api/Consulta/ObtenerTiposCatalogo/";
            $datos = array( 'UsuarioId'     => $UsuarioID,
                            'TipoAccion'    => $TipoAccion,
                            'PerfilId'      => $PerfilId,
                            'TipoId'        => $TipoId,
                            'EstadoId'      => $EstadoId,
                            'Transaccion'   => $Transaccion
                            );
            $data_json = json_encode($datos);
            $opciones = array('http' => array(
                                            'method'  => 'POST',
                                            'header'  => 'Content-type: application/json',
                                            'content' => $data_json));
            $contexto  = stream_context_create($opciones);
            $resAPI = file_get_contents($url, false, $contexto);
            $verificarAPI = json_decode($resAPI);
            echo json_encode($verificarAPI);
        }
        public function ObtenerCatalogosMaestro(){
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $PerfilId = $this->datosSesion->PerfilId;            
            $TipoAccion = $this->TipoAccion;
            $TipoId = $this->TipoId;
            $EstadoId = $this->EstadoId;
            $Transaccion = $this->Transaccion;

            $url = "http://10.87.151.38/InventarioTicAPI/api/Consulta/ObtenerCatalogosMaestro/";
            //$url = "http://localhost:41394/api/Consulta/ObtenerCatalogosMaestro/";
            $datos = array( 'UsuarioId'     => $UsuarioID,
                            'TipoAccion'    => $TipoAccion,
                            'PerfilId'      => $PerfilId,
                            'TipoId'        => $TipoId,
                            'EstadoId'      => $EstadoId,
                            'Transaccion'   => $Transaccion
                            );
            $data_json = json_encode($datos);
            $opciones = array('http' => array(
                                            'method'  => 'POST',
                                            'header'  => 'Content-type: application/json',
                                            'content' => $data_json));
            $contexto  = stream_context_create($opciones);
            $resAPI = file_get_contents($url, false, $contexto);
            $verificarAPI = json_decode($resAPI);
            echo json_encode($verificarAPI);
        }
        public function RegistrarTipoCatalogo(){
            $Id = $this->Id;
            $Nombre = $this->Nombre;
            $BusquedaRapida = $this->BusquedaRapida;
            $EstadoId = $this->EstadoId;
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $IpPublica = $this->datosSesion->IpPublica;
            $IpPrivada = $this->datosSesion->IpPrivada;
            $Transaccion = $this->Transaccion;

            try{
                $url = "http://10.87.151.38/InventarioTicAPI/api/Transaccion/SetCatalogo/";
                $datos = array( 'Id'                        => $Id,
                                'Nombre'                    => $Nombre,
                                'BusquedaRapida'            => $BusquedaRapida,
                                'EstadoId'                  => $EstadoId,
                                'UsuarioId'                 => $UsuarioID,
                                'IpPublica'                 => $IpPublica,
                                'IpPrivada'                 => $IpPrivada,
                                'Transaccion'               => $Transaccion
                                );
                
                $data_json = json_encode($datos);
                $opciones = array('http' => array(
                                                'method'  => 'POST',
                                                'header'  => 'Content-type: application/json',
                                                'content' => $data_json));
                $contexto  = stream_context_create($opciones);
                $resAPI = file_get_contents($url, false, $contexto);
                $verificarAPI = json_decode($resAPI);
                echo json_encode($verificarAPI);
            } catch (Exception $e) {
                $objError = new ErrorResponse();
                $objError->Id = 0;
                $objError->Mensaje = "ERROR";
                $objError->Respuesta = $e;

                echo json_encode($objError);
            }
            
        }
        public function RegistrarCatalogo(){
            $Id = $this->Id;
            $TipoId = $this->TipoId;
            $Orden = $this->Orden;
            $Valor = $this->Valor;
            $Valor2 = $this->Valor2;
            $EstadoId = $this->EstadoId;
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $IpPublica = $this->datosSesion->IpPublica;
            $IpPrivada = $this->datosSesion->IpPrivada;
            $Transaccion = $this->Transaccion;

            try{
                $url = "http://10.87.151.38/InventarioTicAPI/api/Transaccion/SetCatalogo/";
                //$url = "http://localhost:41394/api/Transaccion/SetCatalogo/";
                $datos = array( 'Id'                        => $Id,
                                'TipoId'                    => $TipoId,
                                'Valor'                     => $Valor,
                                'Valor2'                    => $Valor2,
                                'EstadoId'                  => $EstadoId,
                                'UsuarioId'                 => $UsuarioID,
                                'IpPublica'                 => $IpPublica,
                                'IpPrivada'                 => $IpPrivada,
                                'Transaccion'               => $Transaccion
                                );
                
                $data_json = json_encode($datos);
                $opciones = array('http' => array(
                                                'method'  => 'POST',
                                                'header'  => 'Content-type: application/json',
                                                'content' => $data_json));
                $contexto  = stream_context_create($opciones);
                $resAPI = file_get_contents($url, false, $contexto);
                $verificarAPI = json_decode($resAPI);
                echo json_encode($verificarAPI);
            } catch (Exception $e) {
                $objError = new ErrorResponse();
                $objError->Id = 0;
                $objError->Mensaje = "ERROR";
                $objError->Respuesta = $e;

                echo json_encode($objError);
            }            
        }
        public function CambiarEstadoCatalogo(){
            $Id = $this->Id;
            $EstadoId = $this->EstadoId;
            $UsuarioID = $this->datosSesion->UsuarioSIUG;
            $IpPublica = $this->datosSesion->IpPublica;
            $IpPrivada = $this->datosSesion->IpPrivada;
            $Transaccion = $this->Transaccion;

            try{
                //$url = "http://10.87.151.38/InventarioTicAPI/api/Transaccion/SetCatalogo/";
                $url = "http://localhost:41394/api/Transaccion/SetCatalogo/";
                $datos = array( 'Id'                        => $Id,
                                'EstadoId'                  => $EstadoId,
                                'UsuarioId'                 => $UsuarioID,
                                'IpPublica'                 => $IpPublica,
                                'IpPrivada'                 => $IpPrivada,
                                'Transaccion'               => $Transaccion
                                );
                
                $data_json = json_encode($datos);
                $opciones = array('http' => array(
                                                'method'  => 'POST',
                                                'header'  => 'Content-type: application/json',
                                                'content' => $data_json));
                $contexto  = stream_context_create($opciones);
                $resAPI = file_get_contents($url, false, $contexto);
                $verificarAPI = json_decode($resAPI);
                echo json_encode($verificarAPI);
            } catch (Exception $e) {
                $objError = new ErrorResponse();
                $objError->Id = 0;
                $objError->Mensaje = "ERROR";
                $objError->Respuesta = $e;

                echo json_encode($objError);
            }            
        }
    }
    class InicioSesionResponse{
        public $Id;
        public $Token;
        public $Respuesta;
        public $Leyenda;
        public $UsuarioSIUG;
        public $Correo;
        public $IpPublica;
        public $IpPrivada;
        public $SistemaId;
        public $Perfil;
        public $PerfilId;
    }
    class ErrorResponse{
        public $Mensaje;
        public $Id;
        public $Respuesta;
    }
?>