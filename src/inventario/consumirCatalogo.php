<?php
    ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] . '/herramientas/sesiones');
    session_start();
    require_once "iniciarCatalogo.php";

    if (isset($_POST["transaccion"])){
        $obj = new CatalogoTIC();

        switch($_POST["transaccion"]){
            case "TIPO_ARTICULO":
                $obj->TipoAccion = $_POST["tipoAccion"];
                $obj->Transaccion = $_POST["transaccion"];
                return $obj->ObtenerCatalogo();
                break;
            case "CONSULTA_GENERAL":
                $obj->TipoAccion = $_GET["tipoAccion"];
                $obj->TipoId = $_GET["tipoId"];
                $obj->EstadoId = $_GET["estadoId"];
                $obj->Transaccion = $_GET["transaccion"];
                return $obj->ObtenerArticulos();
                break;
            case "CONSULTA_TIPO_CATALOGO":
                $obj->Id = $_POST["id"];
                $obj->TipoAccion = $_POST["tipoAccion"];
                $obj->TipoId = $_GET["tipoId"];
                $obj->EstadoId = $_GET["estadoId"];
                $obj->Transaccion = $_POST["transaccion"];
                return $obj->ObtenerArticulo();
                break;
            case "ESTADO CATALOGO":
                $obj->Id = $_POST["id"];
                $obj->EstadoId = $_POST["estadoId"];
                $obj->Transaccion = $_POST["transaccion"];
                return $obj->CambiarEstadoCatalogo();
                break;
            default:
                $obj->Id = $_POST["id"];
                $obj->TipoId = $_POST["tipoId"];
                $obj->Orden = "1";
                $obj->Valor = $_POST["valor"];
                $obj->Valor2 = $_POST["valor2"];
                $obj->EstadoId = "1";
                $obj->Transaccion = $_POST["transaccion"];
                return $obj->RegistrarCatalogo();
                break;                
        }
    }
    else{
        if(isset($_GET["transaccion"])){
            $obj = new CatalogoTIC();

            switch($_GET["transaccion"]){
                case "CONSULTA_GENERAL":
                    $obj->TipoAccion = $_GET["tipoAccion"];
                    $obj->TipoId = $_GET["tipoId"];
                    $obj->EstadoId = $_GET["estadoId"];
                    $obj->Transaccion = $_GET["transaccion"];
                    return $obj->ObtenerCatalogosMaestro();
                    break;
                case "CONSULTA_TIPO_CATALOGO":
                    $obj->TipoAccion = $_GET["tipoAccion"];
                    $obj->TipoId = $_GET["tipoId"];                    
                    $obj->EstadoId = $_GET["estadoId"];
                    $obj->Transaccion = $_GET["transaccion"];
                    return $obj->ObtenerTiposCatalogo();
                    break;
            }
        }
    }
    
?>