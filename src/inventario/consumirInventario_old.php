<?php
    ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] . '/herramientas/sesiones');
    session_start();
    require_once "iniciarInventario.php";

    if (isset($_POST["transaccion"])){
        $obj = new InventarioTIC();

        switch($_POST["transaccion"]){
            case "FACULTAD":
            case "MARCA":
            case "ESTADO":
            case "TIPO_ARTICULO":
            case "AMBIENTE_EQUIPO":
            case "FACULTAD ESPECIFICO":
                $obj->TipoAccion = $_POST["tipoAccion"];
                $obj->Transaccion = $_POST["transaccion"];
                return $obj->ObtenerCatalogo();
                break;
            case "CONSULTA_GENERAL":
                $obj->TipoAccion = $_GET["tipoAccion"];
                $obj->FacultadId = $_GET["facultadId"];
                $obj->EstadoId = $_GET["estadoId"];
                $obj->Transaccion = $_GET["transaccion"];
                return $obj->ObtenerArticulos();
                break;
            case "CONSULTA_ESPECIFICA":
                $obj->Id = $_POST["id"];
                $obj->TipoAccion = $_POST["tipoAccion"];
                $obj->Transaccion = $_POST["transaccion"];
                return $obj->ObtenerArticulo();
                break;
            default:
                $obj->Id = $_POST["id"];
                $obj->TipoId = $_POST["tipoId"];
                $obj->FacultadId = $_POST["facultadId"];
                $obj->Edificio = $_POST["edificio"];
                $obj->Departamento = $_POST["departamento"];
                $obj->MarcaId = $_POST["marcaId"];
                $obj->Modelo = $_POST["modelo"];
                $obj->Serie = $_POST["serie"];
                $obj->CodigoInventario = $_POST["codigoInventario"];
                $obj->Responsable = $_POST["responsable"];
                $obj->AdministradorEdificio = $_POST["administradorEdificio"];
                $obj->EstadoId = $_POST["estadoId"];
                $obj->EstadoRegistro = $_POST["estadoRegistro"];
                $obj->Transaccion = $_POST["transaccion"];
                return $obj->RegistrarArticulo();
                break;                
        }
    }
    else{
        if(isset($_GET["transaccion"])){
            $obj = new InventarioTIC();

            switch($_GET["transaccion"]){
                case "FACULTAD":
                case "MARCA":
                case "ESTADO":
                case "TIPO_ARTICULO":
                case "AMBIENTE_EQUIPO":
                    $obj->TipoAccion = $_GET["tipoAccion"];
                    $obj->Transaccion = $_GET["transaccion"];
                    return $obj->ObtenerCatalogo();
                    break;
                case "FACULTAD ESPECIFICO":
                        $obj->TipoAccion = $_GET["tipoAccion"];
                        $obj->Transaccion = $_GET["transaccion"];
                        return $obj->ObtenerCatalogo();
                        break;
                case "CONSULTA_GENERAL":
                    $obj->TipoAccion = $_GET["tipoAccion"];
                    $obj->FacultadId = $_GET["facultadId"];
                    $obj->EstadoId = $_GET["estadoId"];
                    $obj->Transaccion = $_GET["transaccion"];
                    return $obj->ObtenerArticulos();
                    break;          
            }
        }
    }
    
?>