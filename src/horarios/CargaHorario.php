<?php
require_once "ConsultaHorario.php";
class AjaxConsulta{
    public $DocenteAula;
    public $Clave;
    public function ajaxBuscarHorario(){
        $Datos = ["DocenteAula"=>$this->DocenteAula,"Clave"=>$this->Clave];
        $Response = ControladorConsulta::ctrBuscarHorario($Datos);
        echo json_encode($Response);
    }
}
/*====================================
        BUSCAR USUARIO
 ====================================*/
 if (isset($_POST["DocenteAula"])){
    $Servidores = new AjaxConsulta();
    $Servidores -> DocenteAula = $_POST["DocenteAula"];
    $Servidores -> Clave = $_POST["Clave"];
    $Servidores -> ajaxBuscarHorario();
}

?>