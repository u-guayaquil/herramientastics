function ChangeSelect(){
    $("#Tipo option[value='']").prop("disabled",true);
    var SelectTipo = document.getElementById('Tipo');
    var Valor = SelectTipo.options[SelectTipo.selectedIndex].value;
    if(Valor == 1){ TraerEstudiantesByCarrera(); }
    else if(Valor == 2){ TraerEstudiantesByMateria(); }
    else if(Valor == 3){ TraerEstudiantesByCedula(); }
}

function TraerEstudiantesByCarrera(){
    var datos = new FormData();
    datos.append("Carrera", "Carrera");
    $.ajax({
        url:"src/estudiantes/Cau.ajax.php",
        method:"POST",
        data:datos,
        cache:false,
        contentType:false,
        processData:false,
        dataType: "json",
        beforeSend:function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success:function(respuesta){
            $.unblockUI();
            //$(".card-body").prepend(respuesta);
            $(".card-body").html(respuesta).fadeIn("fast");
            $(document).ready(function(){
                $(".overlay").remove();
            });
        }
    });
}

function TraerEstudiantesByMateria(){
    var datos = new FormData();
    datos.append("Materia", "Materia");
    $.ajax({
        url:"src/estudiantes/Cau.ajax.php",
        method:"POST",
        data:datos,
        cache:false,
        contentType:false,
        processData:false,
        dataType: "json",
        beforeSend:function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success:function(respuesta){
            $.unblockUI();
            $(".card-body").html(respuesta).fadeIn("fast");
            $(document).ready(function(){
                $(".overlay").remove();
            });
        }
    });
}

function TraerEstudiantesByCedula(){
    var datos = new FormData();
    datos.append("Estudiante", "Estudiante");
    $.ajax({
        url:"src/estudiantes/Cau.ajax.php",
        method:"POST",
        data:datos,
        cache:false,
        contentType:false,
        processData:false,
        dataType: "json",
        beforeSend:function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success:function(respuesta){
            $.unblockUI();
            $(".card-body").html(respuesta).fadeIn("fast");
            $(document).ready(function(){
                $(".overlay").remove();
            });
        }
    });
}

function ChangeSelectFacultad(){
    $("#Facultad option[value='']").prop("disabled",true);
    var SelectFacultad = document.getElementById('Facultad');
    var CodFacultad = SelectFacultad.options[SelectFacultad.selectedIndex].value;

    var SelectCarrera = document.getElementById("Carrera");
    var length = SelectCarrera.options.length;
    for (i = length-1; i >= 0; i--) {
        SelectCarrera.options[i] = null;
    }

    var datos = new FormData();
    datos.append("CodFacultad", CodFacultad);
    $.ajax({
        url:"src/estudiantes/Cau.ajax.php",
        method:"POST",
        data:datos,
        cache:false,
        contentType:false,
        processData:false,
        dataType: "json",
        beforeSend:function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success:function(respuesta){
            $.unblockUI();
            option = document.createElement("option");
            option.value = '';
            option.text = 'SELECCIONE CARRERA';
            SelectCarrera.appendChild(option);
            respuesta.forEach( element =>  {
                option = document.createElement("option");
                option.value = element['CARRERA'];
                option.text = element['NOMBRE'];
                SelectCarrera.appendChild(option);
            });
        }
    });
}

function ChangeSelectCarrera(){
    $("#Carrera option[value='']").prop("disabled",true);
    var SelectCarrera = document.getElementById('Carrera');
    var CodCarrera = SelectCarrera.options[SelectCarrera.selectedIndex].value;

    var SelectParalelo = document.getElementById("Paralelo");
    var length = SelectParalelo.options.length;
    for (i = length-1; i >= 0; i--) {
        SelectParalelo.options[i] = null;
    }

    var datos = new FormData();
    datos.append("CodCarrera", CodCarrera);
    $.ajax({
        url:"src/estudiantes/Cau.ajax.php",
        method:"POST",
        data:datos,
        cache:false,
        contentType:false,
        processData:false,
        dataType: "json",
        beforeSend:function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success:function(respuesta){
            $.unblockUI();
            option = document.createElement("option");
            option.value = '';
            option.text = 'SELECCIONE PARALELO';
            SelectParalelo.appendChild(option);
            respuesta.forEach( element =>  {
                option = document.createElement("option");
                option.value = element['PARALELO'];
                option.text = element['NOMBRE'];
                SelectParalelo.appendChild(option);
            });
        }
    });
}

function ChangeSelectParalelo(){
    $("#Paralelo option[value='']").prop("disabled",true);
    var SelectParalelo = document.getElementById('Paralelo');
    var CodParalelo = SelectParalelo.options[SelectParalelo.selectedIndex].value;

    var SelectMateria = document.getElementById("Materia");
    var length = SelectMateria.options.length;
    for (i = length-1; i >= 0; i--) {
        SelectMateria.options[i] = null;
    }

    var datos = new FormData();
    datos.append("CodParalelo", CodParalelo);
    $.ajax({
        url:"src/estudiantes/Cau.ajax.php",
        method:"POST",
        data:datos,
        cache:false,
        contentType:false,
        processData:false,
        dataType: "json",
        beforeSend:function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success:function(respuesta){
            $.unblockUI();
            option = document.createElement("option");
            option.value = '';
            option.text = 'SELECCIONE MATERIA';
            SelectMateria.appendChild(option);
            respuesta.forEach( element =>  {
                option = document.createElement("option");
                option.value = element['MATERIA'];
                option.text = element['NOMBRE'];
                SelectMateria.appendChild(option);
            });
        }
    });
}

function ChangeSelectMateria(){
    $("#Materia option[value='']").prop("disabled",true);
}

function ExportarExcel(){
    $("#frmEstudiante").submit();
}