function DatosTablaReporteClasesVirtuales(IdTabla){
    $(IdTabla).DataTable({
        autoWidth: false,
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true,
        "iDisplayLength": -1,
        //"aaSorting": [[ 3, "asc" ]],
        pageLength : 20,
        bPaginate: false,
        ordering: false,
        bFilter: false, bInfo: false,
        lengthMenu: [[20, 40, 80, -1], [20, 40, 80, 'Todos']]
    });
};

function ConsultarClasesVirtuales(){
    $(".card-borrar").remove();
    var datos = new FormData();
    var CI = document.getElementById('CI').value;

    if(CI){

        datos.append("Cedula", CI);

        $.ajax({
            url:"src/reporteclasesvirtuales/LlamadaAjax.php",
            method:"POST",
            data:datos,
            cache:false,
            contentType:false,
            processData:false,
            dataType: "json",
            beforeSend:function () {
                $.blockUI({
                    message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                });
            },
            success:function(respuesta){
                if(respuesta['status'] == 1){
                    $("#divGrid").prepend(respuesta['message']);
                    DatosTablaReporteClasesVirtuales("#tablas");
                    $.unblockUI();
                }
                else{
                    $.unblockUI();
                    document.getElementById('CI').value = '';
                    Swal.fire({
                        icon: "error",
                        title: "¡NO HAY DATOS!",
                        text: "¡"+respuesta['message']+"!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    });
                }
            }
        });
    }
    else{
        document.getElementById('CI').value = '';
        Swal.fire({
            icon: "error",
            title: "¡Cédula no ingresada!",
            text: "¡Ingrese una cédula por favor!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
}

