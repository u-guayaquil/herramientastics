function LlenarCombos(tipoAccion,transaccion) {
    $.ajax({
        type: "POST",
        url: "src/inventario/consumirInventario.php",
        dataType: "json",
        data: {
            tipoAccion: tipoAccion,
            transaccion: transaccion
        },
        beforeSend: function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success: function (result) {
            $.unblockUI();   
            switch (result.mensaje) {
                case "OK":
                    var objeto;

                    switch(transaccion){
                        case "FACULTAD":
                            if(tipoAccion == 1){
                                objeto = $("#cmbFacultad");
                            }
                            else{
                                objeto = $("#cmbFacultadMant");
                            }
                            break;
                        case "MARCA":
                            objeto = $("#cmbMarca");
                            break;
                        case "ESTADO":
                            if(tipoAccion == 1){
                                objeto = $("#cmbEstado");
                            }
                            else{
                                objeto = $("#cmbEstadoMant");
                            }
                            break;
                        case "TIPO_ARTICULO":
                            objeto = $("#cmbTipo");
                            break;
                        case "AMBIENTE_EQUIPO":
                            objeto = $("#cmbAmbiente");
                            break;
                    }    

                    objeto.empty();
                    $.each(result.objetos, function () {
                        objeto.append(new Option(this.descripcion, this.id));
                    });
                    break;
                default:
                    Swal.fire({
                        html: result.mensaje,
                        icon: "error",
                        allowOutsideClick: false,
                        buttonsStyling: false,
                        confirmButtonText: "Ok, entendido!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    });
                    break;
            }
        },
        error: function (request, status, error) {
            $.unblockUI();

            Swal.fire({
                text: error,
                icon: "error",
                allowOutsideClick: false,
                buttonsStyling: false,
                confirmButtonText: "Ok, entendido!",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            });
        }
    });
}
function ConsultaFacultad(tipoAccion){
    $.ajax({
        type: "POST",
        url: "src/inventario/consumirInventario.php",
        dataType: "json",
        data: {
            tipoAccion:  tipoAccion,
            transaccion: "FACULTAD ESPECIFICO"
        },
        beforeSend: function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success: function (result) {
            $.unblockUI();   
            switch (result.mensaje) {
                case "OK":
                    if(tipoAccion == "1"){
                        if(result.objetos.length > 1){
                            $("#txtFacultadId").val("");
                            $("#txtFacultad").val("");
                            $("#btnBuscarFacultadI").show();
                            $("#btnBorrarFacultadI").show();
                            //$("#btnNuevoInventario").hide();
                        }
                        else{
                            $("#txtFacultadId").val(result.objetos[0].id);
                            $("#txtFacultad").val(result.objetos[0].descripcion);
                            $("#btnBuscarFacultadI").hide();
                            $("#btnBorrarFacultadI").hide();
                            //$("#btnNuevoInventario").show();
                        }
                    }
                    else{
                        if(result.objetos.length <= 1){
                            $("#txtFacultadMantId").val(result.objetos[0].id);
                            $("#txtFacultadMant").val(result.objetos[0].descripcion);
                            $("#btnBuscarFacultadM").hide();
                            $("#btnBorrarFacultadM").hide();
                        }
                        else{
                            $("#txtFacultadMantId").val("");
                            $("#txtFacultadMant").val("");
                            $("#btnBuscarFacultadM").show();
                            $("#btnBorrarFacultadM").show();
                        }                        
                    }
                    break;
                default:
                    Swal.fire({
                        html: result.respuesta,
                        icon: "error",
                        allowOutsideClick: false,
                        buttonsStyling: false,
                        confirmButtonText: "Ok, entendido!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    });
                    break;
            }
        },
        error: function (request, status, error) {
            $.unblockUI();

            Swal.fire({
                text: error,
                icon: "error",
                allowOutsideClick: false,
                buttonsStyling: false,
                confirmButtonText: "Ok, entendido!",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            });
        }
    });
    return false;
}
function ConsultarRegistro(id,transaccion){
    $.ajax({
        type: "POST",
        url: "src/inventario/consumirInventario.php",
        dataType: "json",
        data: {
            id:  id,
            tipoAccion:  "1",
            transaccion: transaccion
        },
        beforeSend: function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success: function (result) {
            $.unblockUI();   
            switch (result.mensaje) {
                case "OK":                    
                    $("#btnNuevoInventario").hide();
                    $("#btnBuscarInventario").hide();
                    $("#btnExportarExcelInventario").hide();
                    $("#hTitulo").html("Edición de Registro");
                    $("#txtCodigo").val(id);
                    $("#txtFacultadMantId").val(result.objetos[0].facultadId);
                    $("#txtFacultadMant").val(result.objetos[0].facultad);
                    $("#txtDepartamento").val(result.objetos[0].departamento);
                    $("#txtEdificio").val(result.objetos[0].edificio);
                    $("#txtTipoId").val(result.objetos[0].tipoId);
                    $("#txtTipo").val(result.objetos[0].tipo);
                    $("#txtMarcaId").val(result.objetos[0].marcaId);
                    $("#txtMarca").val(result.objetos[0].marca);                    
                    $("#txtModelo").val(result.objetos[0].modelo);
                    $("#txtSerie").val(result.objetos[0].serie);
                    $("#txtCodigoInventario").val(result.objetos[0].codigoInventario);
                    $("#txtEstadoId").val(result.objetos[0].estadoId);
                    $("#txtEstado").val(result.objetos[0].estado);

                    if (result.objetos[0].administradorEdificio == "1") {
                        $("#txtUsuarioInventario").val("");
                        $("#txtUsuarioInventario").attr('readonly','readonly');
                        $("#chkAdministradorEdificio").prop("checked", true);
                    }
                    else{
                        $("#txtUsuarioInventario").removeAttr('readonly');
                        $("#txtUsuarioInventario").val(result.objetos[0].responsable);
                    }

                    $("#btnRegresarInventario").show();
                    $("#btnGrabarInventario").show();
                    $("#divConsultas").hide();           

                    $("#divTransaccion").show();
                    break;
                default:
                    Swal.fire({
                        html: result.respuesta,
                        icon: "error",
                        allowOutsideClick: false,
                        buttonsStyling: false,
                        confirmButtonText: "Ok, entendido!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    });
                    break;
            }
        },
        error: function (request, status, error) {
            $.unblockUI();

            Swal.fire({
                text: error,
                icon: "error",
                allowOutsideClick: false,
                buttonsStyling: false,
                confirmButtonText: "Ok, entendido!",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            });
        }
    });
    return false;
}
function GrabarArticulo(transaccion){
    $.ajax({
        type: "POST",
        url: "src/inventario/consumirInventario.php",
        dataType: "json",
        data: {
            id:  function () { return $("#txtCodigo").val(); },
            tipoId:  function () { return $("#txtTipoId").val(); },
            facultadId:  function () { return $("#txtFacultadMantId").val(); },
            edificio:  function () { return $("#txtEdificio").val(); },
            departamento:  function () { return $("#txtDepartamento").val(); },
            marcaId:  function () { return $("#txtMarcaId").val(); },
            modelo:  function () { return $("#txtModelo").val(); },
            serie:  function () { return $("#txtSerie").val(); },
            codigoInventario:  function () { return $("#txtCodigoInventario").val(); },
            responsable:  function () { return $("#txtUsuarioInventario").val(); },
            administradorEdificio : function () {
                if ($('#chkAdministradorEdificio').is(":checked") == true) {
                    return "1";
                }
                else{
                    return "0";
                }
            },
            estadoId:  function () { return $("#txtEstadoId").val(); },
            estadoRegistro: "1",
            transaccion: transaccion
        },
        beforeSend: function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success: function (result) {
            $.unblockUI();   
            switch (result.mensaje) {
                case "OK":
                    Swal.fire({
                        html: result.respuesta,
                        icon: "success",
                        allowOutsideClick: false,
                        buttonsStyling: false,
                        confirmButtonText: "Ok, entendido!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    }).then(function (result) {
                        $("#btnRegresarInventario").hide();
                        $("#btnGrabarInventario").hide();
                        $("#divTransaccion").hide();
                        
                        LlenarCombos(1,"FACULTAD");
                        LlenarCombos(1,"ESTADO");  

                        $("#btnNuevoInventario").show();
                        $("#btnBuscarInventario").show();
                        $("#btnExportarExcelInventario").show();
                        $("#divConsultas").show();
                    });
                    break;
                default:
                    Swal.fire({
                        html: result.respuesta,
                        icon: "error",
                        allowOutsideClick: false,
                        buttonsStyling: false,
                        confirmButtonText: "Ok, entendido!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    });
                    break;
            }
        },
        error: function (request, status, error) {
            $.unblockUI();

            Swal.fire({
                text: error,
                icon: "error",
                allowOutsideClick: false,
                buttonsStyling: false,
                confirmButtonText: "Ok, entendido!",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            });
        }
    });
    return false;
}