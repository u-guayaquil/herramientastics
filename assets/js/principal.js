function SetApp(documento){
    var iframe = "0";
    var urlFinal = documento.split('|');

    if(urlFinal.length > 1){
        iframe = urlFinal[1];
    }

    if(iframe == "1"){
        var datosIframe = "<iframe frameborder='0' style='width: 100%; height:98%;' scrolling='yes' src='" + urlFinal[0] + "' allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>";
        console.log(datosIframe);
        $('#kt_sidebar').hide();
        $('#kt_sidebar').css('display', 'none');
        $('#kt_wrapper').html(datosIframe).fadeIn("fast");
    }
    else{
        $.ajax({
            type: 'GET',
            url: urlFinal[0],
            success: function (dataRecieved) {			
                $('#kt_sidebar').hide();
                $('#kt_sidebar').css('display', 'none');
                $('#kt_wrapper').html(dataRecieved).fadeIn("fast");						
            },
            error: function () {
            }
        });
    }    
}

function CerrarSesion(){
    $.ajax({
        url:"src/CerrarSesion.php",
        method:"GET",
        cache:false,
        contentType:false,
        processData:false,
        dataType: "json",
        beforeSend:function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Cerrando sesión, un momento por favor...</span></div>'
            });
        },
        success:function(respuesta){
            $.unblockUI();
            location.href = "index.php";
        }
    });
}
function Menu(Tipo){
    $.ajax({
        type: "POST",
        url: "src/IniciarPrincipal.php",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Configurando opciones en el sistema, un momento por favor...</span></div>'
            });
        },
        success: function (result) {
            $.unblockUI();
            if(result.mensaje == "OK"){
                if(Tipo==1){
                    Opciones(result.objetos[0].moduloId);
                }
                else{
                    AccesosDirectos(result.objetos[0].moduloId);
                }
            }            
        },
        error: function (request, status, error) {
            $.unblockUI();

            Swal.fire({
                text: error,
                icon: "error",
                allowOutsideClick: false,
                buttonsStyling: false,
                confirmButtonText: "Ok, entendido!",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            });
        }
    });
}
function Opciones(ModuloId){
    $.ajax({
        type: "POST",
        url: "src/IniciarOpciones.php",
        dataType: "json",
        data: {
            moduloId: ModuloId
        },
        beforeSend: function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Configurando opciones en el sistema, un momento por favor...</span></div>'
            });
        },
        success: function (result) {
            $.unblockUI();

            var urlFinal = "";

            if(result.mensaje == "OK"){
                var resultadoHTML = "";

                var ListadoEstudiantes = '<polygon points="0 0 24 0 24 24 0 24"/>';
                ListadoEstudiantes += '<path d="M5.74714567,13.0425758 C4.09410362,11.9740356 3,10.1147886 3,8 C3,4.6862915 5.6862915,2 9,2 C11.7957591,2 14.1449096,3.91215918 14.8109738,6.5 L17.25,6.5 C19.3210678,6.5 21,8.17893219 21,10.25 C21,12.3210678 19.3210678,14 17.25,14 L8.25,14 C7.28817895,14 6.41093178,13.6378962 5.74714567,13.0425758 Z" fill="#000000" opacity="0.3"/>';
                ListadoEstudiantes += '<path d="M11.1288761,15.7336977 L11.1288761,17.6901712 L9.12120481,17.6901712 C8.84506244,17.6901712 8.62120481,17.9140288 8.62120481,18.1901712 L8.62120481,19.2134699 C8.62120481,19.4896123 8.84506244,19.7134699 9.12120481,19.7134699 L11.1288761,19.7134699 L11.1288761,21.6699434 C11.1288761,21.9460858 11.3527337,22.1699434 11.6288761,22.1699434 C11.7471877,22.1699434 11.8616664,22.1279896 11.951961,22.0515402 L15.4576222,19.0834174 C15.6683723,18.9049825 15.6945689,18.5894857 15.5161341,18.3787356 C15.4982803,18.3576485 15.4787093,18.3380775 15.4576222,18.3202237 L11.951961,15.3521009 C11.7412109,15.173666 11.4257142,15.1998627 11.2472793,15.4106128 C11.1708299,15.5009075 11.1288761,15.6153861 11.1288761,15.7336977 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.959697, 18.661508) rotate(-270.000000) translate(-11.959697, -18.661508) "/>';

                var ConsultarHorarios = '<rect x="0" y="0" width="24" height="24"/>';
                ConsultarHorarios += '<path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z" fill="#000000"/>';
                ConsultarHorarios += '<path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"/>';

                var ZonaDescarga = '<rect x="0" y="0" width="24" height="24"/>';
                ZonaDescarga += '<path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>';
                ZonaDescarga += '<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>';
                ZonaDescarga += '<path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>';

                var FichaEstudiante = '<rect x="0" y="0" width="24" height="24"/>';
                FichaEstudiante += '<path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"/>';

                var ReporteClasesVirtuales = '<rect x="0" y="0" width="24" height="24"/>';
                ReporteClasesVirtuales += '<path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z" fill="#000000" opacity="0.3"/>';
                ReporteClasesVirtuales += '<path d="M13,5.06189375 C12.6724058,5.02104333 12.3386603,5 12,5 C11.6613397,5 11.3275942,5.02104333 11,5.06189375 L11,4 L10,4 C9.44771525,4 9,3.55228475 9,3 C9,2.44771525 9.44771525,2 10,2 L14,2 C14.5522847,2 15,2.44771525 15,3 C15,3.55228475 14.5522847,4 14,4 L13,4 L13,5.06189375 Z" fill="#000000"/>';
                ReporteClasesVirtuales += '<path d="M16.7099142,6.53272645 L17.5355339,5.70710678 C17.9260582,5.31658249 18.5592232,5.31658249 18.9497475,5.70710678 C19.3402718,6.09763107 19.3402718,6.73079605 18.9497475,7.12132034 L18.1671361,7.90393167 C17.7407802,7.38854954 17.251061,6.92750259 16.7099142,6.53272645 Z" fill="#000000"/>';
                ReporteClasesVirtuales += '<path d="M11.9630156,7.5 L12.0369844,7.5 C12.2982526,7.5 12.5154733,7.70115317 12.5355117,7.96165175 L12.9585886,13.4616518 C12.9797677,13.7369807 12.7737386,13.9773481 12.4984096,13.9985272 C12.4856504,13.9995087 12.4728582,14 12.4600614,14 L11.5399386,14 C11.2637963,14 11.0399386,13.7761424 11.0399386,13.5 C11.0399386,13.4872031 11.0404299,13.4744109 11.0414114,13.4616518 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>';
        
                var contador = 1;
                resultadoHTML += '<div class="row gutter-b">';

                result.objetos.forEach( element =>  {
                    if(contador == 3){
                        resultadoHTML +=  '</div><div class="row gutter-b">';                        
                        contador = 1;
                    }
                    resultadoHTML += '<div class="col-6">';
                    
                    if(element['iframe'] == "1"){
                        urlFinal = element['rutaForma'] + "|1";                        
                    }
                    else{
                        urlFinal = element['rutaForma'] + "|0";                        
                    }

                    resultadoHTML += '	<a href=javascript:SetApp("' + urlFinal + '"); class="btn btn-block btn-light btn-hover-primary text-dark-50 text-center py-10 px-5 link-app">';
                    resultadoHTML += '		<span class="svg-icon svg-icon-3x svg-icon-primary m-0">';
                    resultadoHTML += '			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">';
                    resultadoHTML += '				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">';

                    switch(element['formaId']){
                        case '275':
                            resultadoHTML += ListadoEstudiantes;
                            break;
                        case '276':
                            resultadoHTML += ConsultarHorarios;
                            break;
                        case '277':
                            resultadoHTML += ZonaDescarga;
                            break;
                        case '278':
                            resultadoHTML += ReporteClasesVirtuales;
                            break;
                        default:
                            resultadoHTML += ReporteClasesVirtuales;
                            break;
                    }
                            
                    resultadoHTML += '				</g>';
                    resultadoHTML += '			</svg>';
                    resultadoHTML += '		</span>';
                    resultadoHTML += '		<span class="d-block font-weight-bold font-size-h6 mt-2">'+ element['nombre'] +'</span>';
                    resultadoHTML += '	</a>';
                    resultadoHTML += '</div>';
                    
                    contador += 1;
                    
                });

                if(contador == 3)
                {
                    resultadoHTML +=  '</div>';
                    contador = 1;
                }

                $("#divMenu").html(resultadoHTML).fadeIn("fast");
            }            
        },
        error: function (request, status, error) {
            $.unblockUI();

            Swal.fire({
                text: error,
                icon: "error",
                allowOutsideClick: false,
                buttonsStyling: false,
                confirmButtonText: "Ok, entendido!",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            });
        }
    });
}
function AccesosDirectos(ModuloId){
    $.ajax({
        type: "POST",
        url: "src/IniciarOpciones.php",
        dataType: "json",
        data: {
            moduloId: ModuloId
        },
        beforeSend: function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Configurando opciones en el sistema, un momento por favor...</span></div>'
            });
        },
        success: function (result) {
            $.unblockUI();

            var urlFinal = "";

            if(result.mensaje == "OK"){
                var resultadoHTML = "";

                var ListadoEstudiantes = '<polygon points="0 0 24 0 24 24 0 24"/>';
                ListadoEstudiantes += '<path d="M5.74714567,13.0425758 C4.09410362,11.9740356 3,10.1147886 3,8 C3,4.6862915 5.6862915,2 9,2 C11.7957591,2 14.1449096,3.91215918 14.8109738,6.5 L17.25,6.5 C19.3210678,6.5 21,8.17893219 21,10.25 C21,12.3210678 19.3210678,14 17.25,14 L8.25,14 C7.28817895,14 6.41093178,13.6378962 5.74714567,13.0425758 Z" fill="#000000" opacity="0.3"/>';
                ListadoEstudiantes += '<path d="M11.1288761,15.7336977 L11.1288761,17.6901712 L9.12120481,17.6901712 C8.84506244,17.6901712 8.62120481,17.9140288 8.62120481,18.1901712 L8.62120481,19.2134699 C8.62120481,19.4896123 8.84506244,19.7134699 9.12120481,19.7134699 L11.1288761,19.7134699 L11.1288761,21.6699434 C11.1288761,21.9460858 11.3527337,22.1699434 11.6288761,22.1699434 C11.7471877,22.1699434 11.8616664,22.1279896 11.951961,22.0515402 L15.4576222,19.0834174 C15.6683723,18.9049825 15.6945689,18.5894857 15.5161341,18.3787356 C15.4982803,18.3576485 15.4787093,18.3380775 15.4576222,18.3202237 L11.951961,15.3521009 C11.7412109,15.173666 11.4257142,15.1998627 11.2472793,15.4106128 C11.1708299,15.5009075 11.1288761,15.6153861 11.1288761,15.7336977 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.959697, 18.661508) rotate(-270.000000) translate(-11.959697, -18.661508) "/>';

                var ConsultarHorarios = '<rect x="0" y="0" width="24" height="24"/>';
                ConsultarHorarios += '<path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z" fill="#000000"/>';
                ConsultarHorarios += '<path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"/>';

                var ZonaDescarga = '<rect x="0" y="0" width="24" height="24"/>';
                ZonaDescarga += '<path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>';
                ZonaDescarga += '<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>';
                ZonaDescarga += '<path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>';

                var FichaEstudiante = '<rect x="0" y="0" width="24" height="24"/>';
                FichaEstudiante += '<path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"/>';

                var ReporteClasesVirtuales = '<rect x="0" y="0" width="24" height="24"/>';
                ReporteClasesVirtuales += '<path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z" fill="#000000" opacity="0.3"/>';
                ReporteClasesVirtuales += '<path d="M13,5.06189375 C12.6724058,5.02104333 12.3386603,5 12,5 C11.6613397,5 11.3275942,5.02104333 11,5.06189375 L11,4 L10,4 C9.44771525,4 9,3.55228475 9,3 C9,2.44771525 9.44771525,2 10,2 L14,2 C14.5522847,2 15,2.44771525 15,3 C15,3.55228475 14.5522847,4 14,4 L13,4 L13,5.06189375 Z" fill="#000000"/>';
                ReporteClasesVirtuales += '<path d="M16.7099142,6.53272645 L17.5355339,5.70710678 C17.9260582,5.31658249 18.5592232,5.31658249 18.9497475,5.70710678 C19.3402718,6.09763107 19.3402718,6.73079605 18.9497475,7.12132034 L18.1671361,7.90393167 C17.7407802,7.38854954 17.251061,6.92750259 16.7099142,6.53272645 Z" fill="#000000"/>';
                ReporteClasesVirtuales += '<path d="M11.9630156,7.5 L12.0369844,7.5 C12.2982526,7.5 12.5154733,7.70115317 12.5355117,7.96165175 L12.9585886,13.4616518 C12.9797677,13.7369807 12.7737386,13.9773481 12.4984096,13.9985272 C12.4856504,13.9995087 12.4728582,14 12.4600614,14 L11.5399386,14 C11.2637963,14 11.0399386,13.7761424 11.0399386,13.5 C11.0399386,13.4872031 11.0404299,13.4744109 11.0414114,13.4616518 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>';

                result.objetos.forEach( element =>  {
                    resultadoHTML += '<div class="col-sm-3 col-xs-6">';

                    if(element['iframe'] == "1"){
                        urlFinal = element['rutaForma'] + "|1";                        
                    }
                    else{
                        urlFinal = element['rutaForma'] + "|0";                        
                    }

                    resultadoHTML += '	<a href=javascript:SetApp("' + urlFinal + '"); class="btn btn-block btn-light btn-hover-primary text-dark-50 text-center py-10 px-5 link-app">';
                    resultadoHTML += '		<span class="svg-icon svg-icon-3x svg-icon-primary m-0">';
                    resultadoHTML += '			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">';
                    resultadoHTML += '				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">';

                    switch(element['formaId']){
                        case '275':
                            resultadoHTML += ListadoEstudiantes;
                            break;
                        case '276':
                            resultadoHTML += ConsultarHorarios;
                            break;
                        case '277':
                            resultadoHTML += ZonaDescarga;
                            break;
                        case '278':
                            resultadoHTML += ReporteClasesVirtuales;
                            break;
                        default:
                            resultadoHTML += ReporteClasesVirtuales;
                            break;
                    }
                            
                    resultadoHTML += '				</g>';
                    resultadoHTML += '			</svg>';
                    resultadoHTML += '		</span>';
                    resultadoHTML += '		<span class="d-block font-weight-bold font-size-h6 mt-2">'+ element['nombre'] +'</span>';
                    resultadoHTML += '	</a><br />';
                    resultadoHTML += '</div>';
                    
                });

                $("#divShortCuts").html(resultadoHTML).fadeIn("fast");
            }            
        },
        error: function (request, status, error) {
            $.unblockUI();

            Swal.fire({
                text: error,
                icon: "error",
                allowOutsideClick: false,
                buttonsStyling: false,
                confirmButtonText: "Ok, entendido!",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            });
        }
    });
}