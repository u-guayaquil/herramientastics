var contenidoHorario;
var tabla;

function DatosTablaReporte(IdTabla){
    $(IdTabla).DataTable({
        autoWidth: false,
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true,
        "iDisplayLength": -1,
        //"aaSorting": [[ 3, "asc" ]],
        pageLength : 10,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'Todos']]
    });
};
function ConsultarHorario(){
    $(".card-borrar").remove();
    var DocenteAula = document.getElementById('DocenteAula').value;
    var Clave = document.getElementById('Clave').value;
    
    if(DocenteAula)
    {   var datos = new FormData();
        datos.append("DocenteAula", DocenteAula);
        datos.append("Clave", Clave);
        $.ajax({
            url:"src/horarios/CargaHorario.php",
            method:"POST",
            data:datos,
            cache:false,
            contentType:false,
            processData:false,
            dataType: "json",
            beforeSend:function () {
                $.blockUI({
                    message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Buscando horario, un momento por favor...</span></div>'
                });
            },
            success:function(respuesta){
                $.unblockUI();
                if(respuesta['status'] == 1){
                    document.getElementById('DocenteAula').value = '';
                    document.getElementById('Clave').value = '';
                    $(".card-body-table").prepend(respuesta['message']);
                    DatosTablaReporte("#tablas");
                    
                }
                else{
                    document.getElementById('DocenteAula').value = '';
                    document.getElementById('Clave').value = '';
                    Swal.fire({
                        icon: "error",
                        title: "¡NO HAY DATOS!",
                        text: "¡"+respuesta['message']+"!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    });
                }
            },
        });
    }
    else{
        document.getElementById('DocenteAula').value = '';
        Swal.fire({
            icon: "error",
            title: "¡CAMPO VACÌOS!",
            text: "¡Llene el campo Docente/Aula por favor!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
}

$(document).ajaxStop($.unblockUI);
$(document).ready(function() {
    $('#DocenteAula').bind('keypress',function(event){
        var regex = new RegExp("^[a-zA-Z0-9Ññ ]+$");
        var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(str)){
            event.preventDefault();
            return false;
        }
    });
    
    $('#DocenteAula, #Clave').on('keydown keypress',function(e){
        if(e.keyCode == 13) {
            ConsultarHorario();
        }
    });

    $("#btnConsultar").click(function (){
        ConsultarHorario();
    });
});