function IniciarSesion() {
    var Usuario = $("#usuario-institucional").val();
    var Clave = $("#clave-institucional").val();
    var Ip = $("#hfldIpCliente").val();

    $.ajax({
        type: "POST",
        url: "src/IniciarSesion.php",
        dataType: "json",
        data: {
            ip: Ip,
            transaccion: "Iniciosesion",
            usuario: Usuario,
            clave: Clave
        },
        beforeSend: function () {
            $.blockUI({
                message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
            });
        },
        success: function (result) {
            $.unblockUI();   
            switch (result.Respuesta) {
                case "OK":
                    location.href = "principal.php";
                    break;
                case "TOKEN":
                    location.href = result.URL;
                    break;
                default:
                    Swal.fire({
                        html: result.Leyenda,
                        icon: "error",
                        allowOutsideClick: false,
                        buttonsStyling: false,
                        confirmButtonText: "Ok, entendido!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    });
                    break;
            }
        },
        error: function (request, status, error) {
            $.unblockUI();

            Swal.fire({
                text: error,
                icon: "error",
                allowOutsideClick: false,
                buttonsStyling: false,
                confirmButtonText: "Ok, entendido!",
                customClass: {
                    confirmButton: "btn font-weight-bold btn-light-primary"
                }
            });
        }
    });
}