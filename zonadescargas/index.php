<!DOCTYPE html> 
<meta charset="UTF-8">
<html> 	
<head>
<!-- Metas -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>DESCARGAS TICS - UG</title>
<link  rel="icon"   href="img/favicon.png" type="image/png" />
<!-- Bootstrap core CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>   
<!-- CSS Personalizable -->
<link href="css/style.css" rel="stylesheet">	
  <!-- FontAwesome -->
  <link href="plugins/fontawesome/css/all.css" rel="stylesheet">	
</head>
<body>
<div class="col-md-12 center-items row hijo">
    <div class="col-md-12 center-items row" style="margin-top:25px;">
        <div class="col-md-12 center-items">
            <a href="./" class="center" style="text-decoration:none">
                <h2 class="center">DESCARGAS TICS - UG</h2>
            </a> 
        </div>
    </div>

<!--================================ SISTEMA INTEGRAL DE LA UNIVERSIDAD DE GUAYAQUIL - SIUG ======================================= -->

    <div class="col-md-12 center-items row card" style="margin-top:25px;">
        <div class="col-md-12 center-items card-body">
            <a href="./" class="center" style="text-decoration:none">
                <h5 class="center">SISTEMA INTEGRAL DE LA UNIVERSIDAD DE GUAYAQUIL - SIUG</h5>
            </a> 
        </div>
    </div>
    <div class="col-md-12 center row hijo">
        <div class="col-md-3  center-items">
            <a href="uploads/INSTRUCTIVO DE TERCERAS MATRICULAS  CI-2022.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/notas.png" class="card-img-top" alt="descarga de excel" style="margin:30px 0px 20px;width:180px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Instructivo de Terceras Matriculas - SIUG</h5>
                        <a href="uploads/INSTRUCTIVO DE TERCERAS MATRICULAS  CI-2022.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="uploads/Manual Retiro Voluntario.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/notas.png" class="card-img-top" alt="descarga de excel" style="margin:30px 0px 20px;width:180px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Manual de Retiro Voluntario - SIUG</h5>
                        <a href="uploads/Manual Retiro Voluntario.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="uploads/MANUAL PARA CREAR CUENTA EN EL SIUG.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/notas.png" class="card-img-top" alt="descarga de excel" style="margin:30px 0px 20px;width:180px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Manual Para Crear Cuenta en el SIUG - SIUG</h5>
                        <a href="uploads/MANUAL PARA CREAR CUENTA EN EL SIUG.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="uploads/MANUAL_RESULTADOS_DE_EVALUACION.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/notas.png" class="card-img-top" alt="descarga de excel" style="margin:30px 0px 20px;width:180px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Manual Resultado de Evaluación - SIUG</h5>
                        <a href="uploads/MANUAL_RESULTADOS_DE_EVALUACION.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
    </div>

<!--================================ CAMPUS VIRTUAL UG - MOODLE UNIVERSIDAD DE GUAYAQUIL ======================================= -->

    <div class="col-md-12 center-items row card" style="margin-top:25px;">
        <div class="col-md-12 center-items card-body">
            <a href="./" class="center" style="text-decoration:none">
                <h5 class="center">CAMPUS VIRTUAL UG - MOODLE UNIVERSIDAD DE GUAYAQUIL</h5>
            </a> 
        </div>
    </div>
    <div class="col-md-12 center row hijo">
        <div class="col-md-3  center-items">
            <a href="uploads/carga de preguntas por lotes formato GIF.mp4" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/moodle.png" class="card-img-top" alt="ingreso de notas" style="margin:30px 0px 0px;width:180px;padding:0px;">
                    <div class="card-body center">
                        <h5 class="card-title">Carga de banco de preguntas (Por archivos) - Campus Virtual UG</h5> </br>
                        <a href="uploads/carga de preguntas por lotes formato GIF.mp4" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="https://www.youtube.com/watch?v=UVfnyov1Ui4" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/moodle.png" class="card-img-top" alt="ingreso de notas" style="margin:30px 0px 0px;width:180px;padding:0px;">
                    <div class="card-body center">
                        <h5 class="card-title">Restauración de la copia de seguridad de las clases del aula virtual - Campus Virtual UG</h5> </br>
                        <a href="https://www.youtube.com/watch?v=UVfnyov1Ui4" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="https://campusvirtual.ug.edu.ec/files/manualAccesoCampusVirtual.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/moodle.png" class="card-img-top" alt="ingreso de notas" style="margin:30px 0px 0px;width:180px;padding:0px;">
                    <div class="card-body center">
                        <h5 class="card-title">Acceso al Campus Virtual - Campus Virtual UG</h5> </br>
                        <a href="https://campusvirtual.ug.edu.ec/files/manualAccesoCampusVirtual.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="https://www.youtube.com/watch?v=_1zcoz51eZE" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/moodle.png" class="card-img-top" alt="ingreso de notas" style="margin:30px 0px 0px;width:180px;padding:0px;">
                    <div class="card-body center">
                        <h5 class="card-title">Foros, mensajes y calendario - Campus Virtual UG</h5>
                        <a href="https://www.youtube.com/watch?v=_1zcoz51eZE" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
    </div>

<!--================================ SISTEMA DE TITULACIÓN UNIVERSIDAD DE GUAYAQUIL - UGCORE ======================================= -->

    <div class="col-md-12 center-items row card" style="margin-top:25px;">
        <div class="col-md-12 center-items card-body">
            <a href="./" class="center" style="text-decoration:none">
                <h5 class="center">SISTEMA DE TITULACIÓN UNIVERSIDAD DE GUAYAQUIL - UGCORE</h5>
            </a> 
        </div>
    </div>
    <div class="col-md-12 center row hijo">
        <div class="col-md-3  center-items">
            <a href="https://www.youtube.com/watch?v=Uj74LqYXMto" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/documento.png" class="card-img-top" alt="descarga de excel" style="margin:30px 0px 20px;width:180px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Registro de Tutorías en el Sistema - UGCORE</h5>
                        <a href="https://www.youtube.com/watch?v=Uj74LqYXMto" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="https://www.youtube.com/watch?v=BJVwgBT-xnc" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/documento.png" class="card-img-top" alt="descarga de excel" style="margin:30px 0px 20px;width:180px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Como Subir Calificaciones al Sistema - UGCORE</h5>
                        <a href="https://www.youtube.com/watch?v=BJVwgBT-xnc" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
    </div>

<!--===================================== PLUGIN UGZOOM - UNIVERSIDAD DE GUAYAQUIL ============================================ -->

    <div class="col-md-12 center-items row card" style="margin-top:25px;">
        <div class="col-md-12 center-items card-body">
            <a href="./" class="center" style="text-decoration:none">
                <h5 class="center">PLUGIN UGZOOM - UNIVERSIDAD DE GUAYAQUIL</h5>
            </a> 
        </div>
    </div>
    <div class="col-md-12 center row hijo">
        <div class="col-md-3  center-items">
            <a href="https://www.posgrados.ug.edu.ec/Instructivo-Windows.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/plugin1.png" class="card-img-top" alt="ingreso de notas" style="margin:30px 0px 0px;width:180px;padding:0px;">
                    <div class="card-body center">
                        <h5 class="card-title">Instalación Y Uso Del Aplicativo UGZoom - Windows</h5> </br>
                        <a href="https://www.posgrados.ug.edu.ec/Instructivo-Windows.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="https://www.posgrados.ug.edu.ec/Instructivo-MacOs.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/plugin2.png" class="card-img-top" alt="ingreso de notas" style="margin:30px 0px 0px;width:180px;padding:0px;">
                    <div class="card-body center">
                        <h5 class="card-title">Instalación Y Uso Del Aplicación UGZoom - MacOs</h5> </br>
                        <a href="https://www.posgrados.ug.edu.ec/Instructivo-MacOs.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="https://www.posgrados.ug.edu.ec/Intalacion-UGzoom-Linux.mp4" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/plugin3.png" class="card-img-top" alt="ingreso de notas" style="margin:30px 0px 0px;width:180px;padding:0px;">
                    <div class="card-body center">
                        <h5 class="card-title">Instalación Y Uso Del Aplicación UGZoom - Linux</h5> </br>
                        <a href="https://www.posgrados.ug.edu.ec/Intalacion-UGzoom-Linux.mp4" target="_blank" class="btn btn-primary" style="margin-top: 30px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="https://www.posgrados.ug.edu.ec/Instructivo-Actualizar-Plugin-UGZoomV3.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/plugin4.png" class="card-img-top" alt="ingreso de notas" style="margin:30px 0px 0px;width:180px;padding:0px;">
                    <div class="card-body center">
                        <h5 class="card-title">Actualización Del Plugin UGZoom V3 - (Windows/MacOs/Linux)</h5> </br>
                        <a href="https://www.posgrados.ug.edu.ec/Instructivo-Actualizar-Plugin-UGZoomV3.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
    </div>

<!--============================================= TEMAS VARIOS ==================================================== -->

    <div class="col-md-12 center-items row card" style="margin-top:25px;">
        <div class="col-md-12 center-items card-body">
            <a href="./" class="center" style="text-decoration:none">
                <h5 class="center">TEMAS VARIOS</h5>
            </a> 
        </div>
    </div>
    <div class="col-md-12 center row hijo">
        <div class="col-md-3  center-items">
            <a href="http://www.ug.edu.ec/logos_bibliotecas_virtuales/Instructivo_Proxy_UG_V1.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/proxy-icon.png" class="card-img-top" alt="descarga de excel" style="margin:30px 0px 20px;width:240px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Instructivo de Configuración Proxy - UG</h5>
                        <a href="http://www.ug.edu.ec/logos_bibliotecas_virtuales/Instructivo_Proxy_UG_V1.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3  center-items">
            <a href="uploads/INTRUCTIVO_DE_SOLUCIÓN_AL_ERROR_1142_EN_ZOOM.pdf" target="_blank">
                <div class="card" style="width: 18rem;">
                    <img src="img/zoom.png" class="card-img-top" alt="descarga de excel" style="width:210px;padding:5px;">
                    <div class="card-body center">
                        <h5 class="card-title">Instructivo de solución del error "1142" en zoom - Zoom UG</h5>
                        <a href="uploads/INTRUCTIVO_DE_SOLUCIÓN_AL_ERROR_1142_EN_ZOOM.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
</body>
</html>