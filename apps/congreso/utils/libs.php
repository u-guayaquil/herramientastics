<?php
    class Libs{
        public function ObtenerTXDia($NumeroDia){
            switch ($NumeroDia) {
                case 1:
                    return "LUNES";
                    break;
                case 2:
                    return "MARTES";
                    break;
                case 3:
                    return "MIERCOLES";
                    break;
                case 4:
                    return "JUEVES";
                    break;
                case 5:
                    return "VIERNES";
                    break;
                case 6:
                    return "SABADO";
                    break;
                case 7:
                    return "DOMINGO";
                    break;
            }
        }

        public function ObtenerTxMes($NumeroMes){
            switch ($NumeroMes) {
                case 1:
                    return "ENERO";
                    break;
                case 2:
                    return "FEBRERO";
                    break;
                case 3:
                    return "MARZO";
                    break;
                case 4:
                    return "ABRIL";
                    break;
                case 5:
                    return "MAYO";
                    break;
                case 6:
                    return "JUNIO";
                    break;
                case 7:
                    return "JULIO";
                    break;
                case 8:
                    return "AGOSTO";
                    break;
                case 9:
                    return "SEPTIEMBRE";
                    break;
                case 10:
                    return "OCTUBRE";
                    break;
                case 11:
                    return "NOVIEMBRE";
                    break;
                case 12:
                    return "DICIEMBRE";
                    break;
            }
        }
    }
?>