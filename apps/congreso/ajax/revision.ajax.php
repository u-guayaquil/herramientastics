<?php
require_once "../controllers/ControlRevision.php";
require_once "../models/ModelRevision.php";
class AjaxRevision{
    public $CI;
    public $PrimerNombre;
    public $SegundoNombre;
    public $PrimerApellido;
    public $SegundoApellido;
    public $Email;
    public $Valor;
    public $Rubro;
    public $Periodo;
    public $Carrera;
    public function ajaxValidarUsuario(){
        if( preg_match('/^[a-zA-Z0-9]+$/', $this->CI)){
            if(preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})+$/', $this->Email)){
                $DataCollection = array(
                    "CI"                =>  $this->CI,
                    "PrimerNombre"      =>  $this->PrimerNombre,
                    "SegundoNombre"     =>  $this->SegundoNombre,
                    "PrimerApellido"    =>  $this->PrimerApellido,
                    "SegundoApellido"   =>  $this->SegundoApellido,
                    "Email"             =>  $this->Email,
                    "Valor"             =>  $this->Valor,
                    "Rubro"             =>  $this->Rubro,
                    "Periodo"           =>  $this->Periodo,
                    "Carrera"           =>  $this->Carrera
                );
                $respuesta = ControladorRevision::ctrEjecutarUsuario($DataCollection);
            echo json_encode($respuesta);
            }
            else{
                $respuesta=array('status'=>0,'message'=>"No es un correo electrónico válido.");;
                echo json_encode($respuesta);
            }
        }
        else{
            $respuesta=array('status'=>0,'message'=>"La Cédula no puede incluir caracteres especiales.");;
            echo json_encode($respuesta);
        }
    }

    public $CIUsuario;
    public function ajaxBuscarUsuario(){
        $respuesta = ModeloRevision::mdlBuscarUsuario($this->CIUsuario);
        echo json_encode($respuesta);
    }
}

/*====================================
        BUSCAR USUARIO
 ====================================*/
 if (isset($_POST["CedulaUsuario"])){
    $Servidores = new AjaxRevision();
    $Servidores -> CIUsuario = $_POST["CedulaUsuario"];
    $Servidores -> ajaxBuscarUsuario();
}
/*====================================
        VALIDAR USUARIO
 ====================================*/
if (isset($_POST["CI"])){
    $Servidores = new AjaxRevision();
    $Servidores -> CI = $_POST["CI"];
    $Servidores -> PrimerNombre = $_POST["PrimerNombre"];
    $Servidores -> SegundoNombre = $_POST["SegundoNombre"];
    $Servidores -> PrimerApellido = $_POST["PrimerApellido"];
    $Servidores -> SegundoApellido = $_POST["SegundoApellido"];
    $Servidores -> Email = $_POST["Email"];
    $Servidores -> Valor = $_POST["Valor"];
    $Servidores -> Rubro = $_POST["Rubro"];
    $Servidores -> Periodo = $_POST["Periodo"];
    $Servidores -> Carrera = $_POST["Carrera"];
    $Servidores -> ajaxValidarUsuario();
}