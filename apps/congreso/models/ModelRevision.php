<?php
    require "conexion.php";
    class ModeloRevision{
        static public function mdlValidarDocente($Cedula)
        {   $QUERY = "SELECT COD_DOCENTE FROM BdAcademico.dbo.TB_DOCENTE_DPERSONAL
                        WHERE COD_DOCENTE = :CI";
            $stmt = Conexion::conectar()->prepare($QUERY);
            $stmt -> bindParam(":CI", $Cedula, PDO::PARAM_STR);
            $stmt -> execute();
            if($stmt ->fetch())
            {    return array('status'=>1,'message'=>"ok");   }
            else
            {   return array('status'=>0,'message'=>"USTED NO CONSTA EN LOS REGISTROS COMO DOCENTE"); }
        }

        static public function mdlValidarEstudiante($Cedula)
        {   $QUERY = "SELECT COD_ESTUDIANTE FROM BdAcademico.dbo.TB_ESTUDIANTE_DPERSONAL
                        WHERE COD_ESTUDIANTE = :CI";
            $stmt = Conexion::conectar()->prepare($QUERY);
            $stmt -> bindParam(":CI", $Cedula, PDO::PARAM_STR);
            $stmt -> execute();
            if($stmt ->fetch())
            {    return array('status'=>1,'message'=>"ok");   }
            else
            {   return array('status'=>0,'message'=>"USTED NO CONSTA EN LOS REGISTROS COMO ESTUDIANTE"); }
        }

        static public function mdlValidarRegistro($Cedula)
        {   $QUERY = "SELECT IDENTIFICACION FROM BdClases_online.dbo.INSCRIPCION_CONGRESO_FILOSOFIA
                        WHERE IDENTIFICACION = :CI AND DELETED_AT IS NULL";
            $stmt = Conexion::conectar()->prepare($QUERY);
            $stmt -> bindParam(":CI", $Cedula, PDO::PARAM_STR);
            $stmt -> execute();
            if($stmt ->fetch())
            {    return array('status'=>0,'message'=>"USTED YA ESTA INSCRIPTO PARA ESTE PROCESO");   }
            else
            {   return array('status'=>1,'message'=>"ok"); }
        }

        static public function mdlBuscarUsuario($Cedula)
        {   $QUERY = "SELECT COD_ESTUDIANTE, NOMBRE, APELLIDO, EMAIL, PERSONAL_EMAIL FROM BdAcademico.dbo.TB_ESTUDIANTE_DPERSONAL
                        WHERE COD_ESTUDIANTE = :CI ";
            $stmt = Conexion::conectar()->prepare($QUERY);
            $stmt -> bindParam(":CI", $Cedula, PDO::PARAM_STR);
            $stmt -> execute();
            $registro = $stmt ->fetch();
            if($registro)
            {    return array('status'=>1,'message'=>$registro);   }
            else
            {   return array('status'=>0,'message'=>"No existe como estudiante."); }
        }
    }
?>