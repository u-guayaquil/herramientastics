$(document).ajaxStop($.unblockUI);

$(document).ready(function() {
    $('#tablas').DataTable({
      autoWidth: false,
      language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true,
        "iDisplayLength": -1,
        //"aaSorting": [[ 3, "asc" ]],
        pageLength : 20,
        lengthMenu: [[20, 40, 80, -1], [20, 40, 80, 'Todos']]
    });
});

$('#CI').keypress(function (e) {
    var regex = new RegExp("^[0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        if($(this).val().length < 10){
            $(this).val($(this).val() + e.key);
        }
    }
    e.preventDefault();
    return false;
});
function Mayus(e) {
    e.value = e.value.toUpperCase();
}

/*============FUNCIONES================*/

function DatosTablaReporte(IdTabla){
    $(IdTabla).DataTable({
        autoWidth: false,
      language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true,
        "iDisplayLength": -1,
        //"aaSorting": [[ 3, "asc" ]],
        pageLength : 20,
        lengthMenu: [[20, 40, 80, -1], [20, 40, 80, 'Todos']]
    });
};

function ConsultarUsuario(){
    $(".card-borrar").remove();
    $(".card-body-table").prepend('<div class="overlay dark col-md-1 offset-md-6"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');

    var CI = document.getElementById('CI').value;
    if(CI)
    {   var datos = new FormData();
        datos.append("Cedula", CI);
        $.ajax({
            url:"ajax/consulta.ajax.php",
            method:"POST",
            data:datos,
            cache:false,
            contentType:false,
            processData:false,
            dataType: "json",
            beforeSend:function () {
                $.blockUI({
                    message: '<br /><img src="img/loader.gif" width="40" height="40" />&nbsp;&nbsp;Buscando usuario...<br /><br />'
                });
            },
            success:function(respuesta){
                $.unblockUI();
                if(respuesta){
                    $.unblockUI();
                    $(".card-body-table").prepend(respuesta);
                    DatosTablaReporte("#tablas");
                    $(document).ready(function(){
                        $(".overlay").remove();
                    });
                }
                else{
                    document.getElementById('CI').value = '';
                    Swal.fire({
                        icon: "error",
                        title: "¡Cédula no encontrada!",
                        text: "¡Ingrese otro número de cédula por favor!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    });
                }
            }
        });
    }
    else{
        document.getElementById('CI').value = '';
        Swal.fire({
            icon: "error",
            title: "¡Cédula no ingresada!",
            text: "¡Ingrese una cédula por favor!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
}