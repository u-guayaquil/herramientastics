<?php
    class VistaConsulta{
        static public function viewPintarTablaRevision($DataCollection)
        {   $HTML = '
            <div class="col-md-10 offset-md-1 card-borrar">
                <div class="card"> 
                    <h5 class="card-header text-center">Resultados de búsqueda</h5>
                    <div class="card-body form-horizontal">
                        <div class="form-group row">
                          <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                            <thead>
                              <tr>
                                <th>N°</th>
                                <th>CEDULA</th>
                                <th>NOMBRES</th>
                                <th>APELLIDOS</th>
                                <th>EMAIL</th>
                                <th>CONCEPTO</th>
                                <th>FECHA</th>
                                <th>VALOR</th>
                                <th>PAGADO</th>
                                <th class="text-center">ORDEN PAGO</th>
                              </tr> </thead> <tbody>';
                              $count = 1;
                            foreach($DataCollection as $Row){
                                $HTML.= '<tr>';
                                $HTML.= '<td>'.$count.'</td>';
                                $HTML.= '<td>'.$Row['IDENTIFICACION'].'</td>';
                                $HTML.= '<td>'.$Row['PRIMER_NOMBRE'].' '.$Row['SEGUNDO_NOMBRE'].'</td>';
                                $HTML.= '<td>'.$Row['PRIMER_APELLIDO'].' '.$Row['SEGUNDO_APELLIDO'].'</td>';
                                $HTML.= '<td>'.$Row['EMAIL_CONTACTO'].'</td>';
                                $HTML.= '<td>'.$Row['CONCEPTO'].'</td>';
                                $HTML.= '<td>'.$Row['FECHA_EMITE'].'</td>';
                                $HTML.= '<td align="right">'.$Row['VALOR'].'</td>';
                                if($Row['CANCEL'] == 'S' && $Row['STATUS'] == ' '){
                                  $HTML.= '<td style="color:blue; text-align: center;">
                                                <i class="fas fa-check"></i>
                                            </td>';
                                }
                                if($Row['CANCEL'] == 'N'){
                                  $HTML.= '<td style="color:red; text-align: center;">
                                                <i class="fas fa-times"></i>
                                            </td>';
                                }
                                $HTML.= '<td align="right" class="text-center">
                                            <a href="https://servicioenlinea.ug.edu.ec/SIUG/TEMPORAL/WEB_CONSULTA_ORDEN_PAGO_EXT.aspx?COD_OPAGO='.$Row['COD_OPAGO'].'" target="_blank">
                                              <i class="fas fa-file-pdf"></i>
                                            </a>
                                          </td>';
                                $HTML.= '</tr>';
                                $count++;
                            }
                            $HTML.= '
                            </tbody> 
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>';
            return $HTML;
        }
    }
?>