<?php
require_once "../models/ModelConsulta.php";
require_once "../view/VistaConsulta.php";
require_once "../utils/utils.php";
class AjaxConsulta{
    public $Cedula;
    public function ajaxBuscarOrdenPago(){
        $DataCollection = ModeloConsulta::mdlObtenerOrdenPago($this->Cedula);
        if($DataCollection){
            $respuesta = VistaConsulta::viewPintarTablaRevision($DataCollection);
            echo json_encode($respuesta);
        }
        else{
            echo 0;
        }        
    }
}
/*====================================
        BUSCAR USUARIO
 ====================================*/
 if (isset($_POST["Cedula"])){
    $Servidores = new AjaxConsulta();
    $Servidores -> Cedula = $_POST["Cedula"];
    $Servidores -> ajaxBuscarOrdenPago();
}