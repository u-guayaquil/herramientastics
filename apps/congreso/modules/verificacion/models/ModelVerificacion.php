<?php
    class ModeloVerificacion{
        static public function mdlObtenerEncargadoServidor()
        {   require "conexion.php";
            $DataCollection = [];
            $QUERY = "SELECT ICF.IDENTIFICACION, ICF.PRIMER_NOMBRE, ICF.SEGUNDO_NOMBRE, ICF.PRIMER_APELLIDO, ICF.SEGUNDO_APELLIDO, 
                                ICF.EMAIL_CONTACTO,OP.CONCEPTO,CONVERT(VARCHAR,OP.FECHA_EMITE,103) AS FECHA_EMITE,
                                CONVERT(DECIMAL(18,2),OP.VALOR) AS VALOR, OP.CANCEL, OP.STATUS, OP.COD_OPAGO
                            FROM BdClases_online.dbo.INSCRIPCION_CONGRESO_FILOSOFIA ICF
                            LEFT JOIN BdAcademico.dbo.TB_ORDEN_PAGO OP ON OP.COD_ESTUDIANTE = ICF.identificacion COLLATE Modern_Spanish_CI_AS
                            WHERE ICF.DELETED_AT IS NULL AND OP.COD_OPAGO = ICF.cod_orden_pago COLLATE Modern_Spanish_CI_AS";
            $stmt = sqlsrv_query($con, $QUERY);

            if(sqlsrv_fetch($stmt))
            {   $items = sqlsrv_query($con, $QUERY);
                while($fila = sqlsrv_fetch_array($items)){
                    $DataCollection[] = $fila;
                }
                return $DataCollection;
            }
            else
            {   return false;   }
        }
    }
?>