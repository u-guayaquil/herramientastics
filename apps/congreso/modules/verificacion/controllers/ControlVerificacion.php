<?php
require_once "utils/utils.php";  
    class ControladorVerificacion{
        static public function ConsultarEncargadoServidor()
        {   $items = ModeloVerificacion::mdlObtenerEncargadoServidor();
            $html = VistaVerificacion::viewPintarTablaRevision($items);
            return $html;
        }

        public static function ExportarExcel(){
            {   if(isset($_POST['Validacion'])){
                    $ModeloResp = new ModeloVerificacion();
                    $DataCollection = $ModeloResp->mdlObtenerEncargadoServidor();
                    Utils::GenerarExcel($DataCollection);
                }
            }
        }
    }
?>