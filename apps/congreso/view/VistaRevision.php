<?php
    class VistaRevision{
        static public function viewPintarTablaRevision($DataCollection)
        {   $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
            <thead>
              <tr>
                <th>N°</th>
                    <th>ENCARGADO AREA</th>
                    <th>CEDULA</th>
                    <th>NOMBRES</th>
                    <th>APELLIDOS</th>
                    <th>ESTADO</th>
              </tr> </thead> <tbody>';
              $count = 1;
            foreach($DataCollection as $Row){
            $HTML.= '<tr>';
            $HTML.= '<td>'.$count.'</td>';
            if($Row['CEDULA_E']){
                $HTML.= '<td>'.$Row['NOMBRES_E'].' '.$Row['APELLIDOS_E'].'</td>';
            }
            else{
                $HTML.= '<td style="color:red;">
                    <i class="fas fa-times"></i> SIN ASIGNAR <i class="fas fa-times"></i>
                </td>';
            }
            $HTML.= '<td>'.$Row['CEDULA'].'</td>';
            $HTML.= '<td>'.$Row['NOMBRES'].'</td>';
            $HTML.= '<td>'.$Row['APELLIDOS'].'</td>';
            if($Row['ESTADO_RELACION'] == 1){
                $HTML.= '<td style="color:blue;text-align: center;">
                            <i class="fas fa-check"></i>
                        </td>';
            }
            else if($Row['ESTADO_RELACION'] == 0){
                $HTML.= '<td style="color:red;text-align: center;">
                            <i class="fas fa-times"></i>
                        </td>';
            } 
            $count++;
        }   
        $HTML.= '</tbody> </table>';
            return $HTML;
        }
    }
?>