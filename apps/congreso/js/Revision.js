document.oncontextmenu = function (e) {
    return false;
}

$(document).keydown(function (event) {
    if (event.keyCode == 123) { // Prevent F12
        return false;
    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I
        return false;
    }
});

$(document).ajaxStop($.unblockUI);

$('input:text').bind('cut copy paste', function (e) {
    e.preventDefault();
});

function CLS(){
    document.getElementById('CI').value = '';
    document.getElementById('PrimerNombre').value = '';
    document.getElementById('PrimerNombre').disabled = false;
    document.getElementById('SegundoNombre').value = '';
    document.getElementById('SegundoNombre').disabled = false;
    document.getElementById('PrimerApellido').value = '';
    document.getElementById('PrimerApellido').disabled = false;
    document.getElementById('SegundoApellido').value = '';
    document.getElementById('SegundoApellido').disabled = false;
    document.getElementById('Email').value = '';
    document.getElementById('Email').disabled = false;
}

function ChangeSelect(){
    $("#Rubros option[value='']").prop("disabled",true);
    var SelectRubros = document.getElementById('Rubros');
    var Valor = SelectRubros.options[SelectRubros.selectedIndex].value;
    document.getElementById('Valor').value = '$'+Valor+'.00';
}

$('#CI').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if(e.keyCode == 13) {
        BuscarUsuario();
        return false;
    }
    if (regex.test(str)) {
        if($(this).val().length < 10){
            $(this).val($(this).val() + e.key);
            BuscarUsuario();
        }
    }
    e.preventDefault();
    return false;
});

$('#PrimerNombre, #SegundoNombre, #PrimerApellido, #SegundoApellido').bind('keypress',function(event){
    var regex = new RegExp("^[a-zA-ZÑñ ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)){
        event.preventDefault();
        return false;
    }
});

function Mayus(e) {
    e.value = e.value.toUpperCase();
}

function Minus(e) {
    e.value = e.value.toLowerCase();
}

$('input').keyup(function () {
    if ($(this).attr('name') == 'Email') {
        this.value = this.value.toLowerCase();
    }
    else {
        this.value = this.value.toUpperCase();
    }
});

function Guardar(){
    var CI = document.getElementById('CI').value;
    var PrimerNombre = document.getElementById('PrimerNombre').value;
    var SegundoNombre = document.getElementById('SegundoNombre').value;
    var PrimerApellido = document.getElementById('PrimerApellido').value;
    var SegundoApellido = document.getElementById('SegundoApellido').value;
    var Email = document.getElementById('Email').value;
    var SelectRubros = document.getElementById('Rubros');
    var Valor = SelectRubros.options[SelectRubros.selectedIndex].value;
    var Rubro = SelectRubros.options[SelectRubros.selectedIndex].getAttribute("rubro");
    var Periodo = SelectRubros.options[SelectRubros.selectedIndex].getAttribute("periodo");
    var Carrera = SelectRubros.options[SelectRubros.selectedIndex].getAttribute("carrera");

    if(CI && PrimerNombre && PrimerApellido && Email && Valor)
    {   var DataResponse = new FormData();
        DataResponse.append("CI", CI);
        DataResponse.append("PrimerNombre", PrimerNombre);
        DataResponse.append("SegundoNombre", SegundoNombre);
        DataResponse.append("PrimerApellido", PrimerApellido);
        DataResponse.append("SegundoApellido", SegundoApellido);
        DataResponse.append("Email", Email);
        DataResponse.append("Valor", Valor);
        DataResponse.append("Rubro", Rubro);
        DataResponse.append("Periodo", Periodo);
        DataResponse.append("Carrera", Carrera);
        $.ajax({
            url:"ajax/revision.ajax.php",
            method:"POST",
            data:DataResponse,
            cache:false,
            contentType:false,
            processData:false,
            dataType: "json",
            beforeSend:function () {
                $.blockUI({
                    message: '<br /><img src="img/loader.gif" width="40" height="40" />&nbsp;&nbsp;Registrado usuario...<br /><br />'
                });
            },
            success:function(response){
                $.unblockUI();
                if(response["status"] == 1){
                    Swal.fire({
                        title: '¡EXCELENTE!',
                        text: "¡"+response["message"]+"!",
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: '¡Imprimir Comprobante!',
                      }).then(function(result){
                        if (result.value) {
                            window.open(response["op"]);
                            location.reload();
                        }
                    });
                }
                else{
                    Swal.fire({
                        icon: "error",
                        title: "¡ERROR!",
                        text: "¡"+response["message"]+"!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    });
                }
            }
        });
    }
    else{
        Swal.fire({
            icon: "error",
            title: "¡CAMPOS VACÍOS!",
            text: "¡Llenar todos los campos, por favor!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
}

function BuscarUsuario(){
    var CI = document.getElementById('CI').value;
    if(CI)
    {   var datos = new FormData();
        datos.append("CedulaUsuario", CI);
        $.ajax({
            url:"ajax/revision.ajax.php",
            method:"POST",
            data:datos,
            cache:false,
            contentType:false,
            processData:false,
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                });
            },
            success:function(respuesta){
                $.unblockUI();
                if(respuesta["status"] == 1){
                    var Data = respuesta["message"];
                    Nombres = Data['NOMBRE'].split(' ', 2);
                    Apellidos = Data['APELLIDO'].split(' ', 2);
                    document.getElementById('PrimerNombre').value = Nombres[0];
                    document.getElementById('PrimerNombre').disabled = true;
                    document.getElementById('SegundoNombre').value = Nombres[1];
                    document.getElementById('SegundoNombre').disabled = true;
                    document.getElementById('PrimerApellido').value = Apellidos[0];
                    document.getElementById('PrimerApellido').disabled = true;
                    document.getElementById('SegundoApellido').value = Apellidos[1];
                    document.getElementById('SegundoApellido').disabled = true;
                    if(Data['EMAIL'] != null)
                    {   document.getElementById('Email').value = Data['EMAIL'];}
                    else
                    {document.getElementById('Email').value = Data['PERSONAL_EMAIL'];}
                }
                else{
                    document.getElementById('PrimerNombre').value = '';
                    document.getElementById('PrimerNombre').disabled = false;
                    document.getElementById('SegundoNombre').value = '';
                    document.getElementById('SegundoNombre').disabled = false;
                    document.getElementById('PrimerApellido').value = '';
                    document.getElementById('PrimerApellido').disabled = false;
                    document.getElementById('SegundoApellido').value = '';
                    document.getElementById('SegundoApellido').disabled = false;
                    document.getElementById('Email').value = '';
                }
            }
        });
    }
    else{
        document.getElementById('PrimerNombre').value = '';
        document.getElementById('PrimerNombre').disabled = false;
        document.getElementById('SegundoNombre').value = '';
        document.getElementById('SegundoNombre').disabled = false;
        document.getElementById('PrimerApellido').value = '';
        document.getElementById('PrimerApellido').disabled = false;
        document.getElementById('SegundoApellido').value = '';
        document.getElementById('SegundoApellido').disabled = false;
        document.getElementById('Email').value = '';
    }
}

