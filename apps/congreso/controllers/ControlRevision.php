<?php
    class ControladorRevision{
        static public function ctrEjecutarUsuario($DataCollection)
        {   $Excepciones = ['0916205263','0909269242','0916281256','0917713026','0201741972','0915069850','0960517910',
            '0923921639','0931547848','0913597241','0920220357'];
            $Valor = $DataCollection['Valor'];
            if(in_array($DataCollection['CI'],$Excepciones)){
                $Valor = 0;
            }
            if($DataCollection['Valor'] == 30){
                $respuesta = ModeloRevision::mdlValidarDocente($DataCollection['CI']);
                if($respuesta["status"] == 1)
                {   $response = ModeloRevision::mdlValidarRegistro($DataCollection['CI']);
                    if($response["status"] == 1)
                    {   $url = "http://10.87.151.205:9090/api/public/inscripcion/ordenpago";
                        $datos = array( 'identificacion'    => $DataCollection['CI'],
                                        'primer_nombre'     => $DataCollection['PrimerNombre'],
                                        'segundo_nombre'    => $DataCollection['SegundoNombre'],
                                        'primer_apellido'   => $DataCollection['PrimerApellido'],
                                        'segundo_apellido'  => $DataCollection['SegundoApellido'],
                                        'email_contacto'    => $DataCollection['Email'],
                                        'cod_carrera'       => $DataCollection['Carrera'],
                                        'rubro'             => $DataCollection['Rubro'],
                                        'valor'             => $Valor,
                                        'periodo'           => $DataCollection['Periodo']
                                    );
                        $data_json = json_encode($datos);
                        $opciones = array('http' => array(
                                        'method'  => 'POST',
                                        'header'  => 'Content-type: application/json',
                                        'content' => $data_json
                                    ));
                        $contexto  = stream_context_create($opciones);
                        $resAPI = file_get_contents($url, false, $contexto);
                        $VerificarAPI = json_decode($resAPI);
                        if($VerificarAPI['status'] == 1){
                            return json_decode($resAPI);  
                        }
                        else{
                            $hoy = date('l jS \of F Y h:i:s A');
                            $file = fopen("../error/error-log.txt", "a");
                            fwrite($file, "Fecha: ".$hoy. PHP_EOL);
                            fwrite($file, "Status API: ".$VerificarAPI->status. PHP_EOL);
                            fwrite($file, $VerificarAPI->statuscode. PHP_EOL);
                            fwrite($file, $VerificarAPI->message. PHP_EOL);
                            fwrite($file, $VerificarAPI->error. PHP_EOL);
                            fwrite($file, "*************************". PHP_EOL);
                            fclose($file);
                            return array('status'=>0,'message'=>"FALLÓ LA RESPUESTA A SU SOLICITUD, INTENTE MAS TARDE POR FAVOR");
                        }
                    }
                    else
                    {   return $response;   }
                }
                else
                {   return $respuesta;  }
            }
            if($DataCollection['Valor'] == 15){
                $respuesta = ModeloRevision::mdlValidarEstudiante($DataCollection['CI']);
                if($respuesta["status"] == 1)
                {   $response = ModeloRevision::mdlValidarRegistro($DataCollection['CI']);
                    if($response["status"] == 1)
                    {   $url = "http://10.87.151.205:9090/api/public/inscripcion/ordenpago";
                        $datos = array( 'identificacion'    => $DataCollection['CI'],
                                        'primer_nombre'     => $DataCollection['PrimerNombre'],
                                        'segundo_nombre'    => $DataCollection['SegundoNombre'],
                                        'primer_apellido'   => $DataCollection['PrimerApellido'],
                                        'segundo_apellido'  => $DataCollection['SegundoApellido'],
                                        'email_contacto'    => $DataCollection['Email'],
                                        'cod_carrera'       => $DataCollection['Carrera'],
                                        'rubro'             => $DataCollection['Rubro'],
                                        'valor'             => $Valor,
                                        'periodo'           => $DataCollection['Periodo']
                                    );
                        $data_json = json_encode($datos);
                        $opciones = array('http' => array(
                                        'method'  => 'POST',
                                        'header'  => 'Content-type: application/json',
                                        'content' => $data_json
                                    ));
                        $contexto  = stream_context_create($opciones);
                        $resAPI = file_get_contents($url, false, $contexto);
                        $VerificarAPI = json_decode($resAPI);
                        if($VerificarAPI->status == 1){
                            return json_decode($resAPI);  
                        }
                        else{
                            $hoy = date('l jS \of F Y h:i:s A');
                            $file = fopen("../error/error-log.txt", "a");
                            fwrite($file, "Fecha: ".$hoy. PHP_EOL);
                            fwrite($file, "Status API: ".$VerificarAPI->status. PHP_EOL);
                            fwrite($file, $VerificarAPI->statuscode. PHP_EOL);
                            fwrite($file, $VerificarAPI->message. PHP_EOL);
                            fwrite($file, $VerificarAPI->error. PHP_EOL);
                            fwrite($file, "*************************". PHP_EOL);
                            fclose($file);
                            return array('status'=>0,'message'=>"FALLÓ LA RESPUESTA A SU SOLICITUD, INTENTE MAS TARDE POR FAVOR");
                        }
                    }
                    else
                    {   return $response;   }
                }
                else
                {   return $respuesta;  }
            }
            if($DataCollection['Valor'] == 35 || $DataCollection['Valor'] == 10){
                $response = ModeloRevision::mdlValidarRegistro($DataCollection['CI']);
                if($response["status"] == 1)
                {  $url = "http://10.87.151.205:9090/api/public/inscripcion/ordenpago";
                    $datos = array( 'identificacion'    => $DataCollection['CI'],
                                    'primer_nombre'     => $DataCollection['PrimerNombre'],
                                    'segundo_nombre'    => $DataCollection['SegundoNombre'],
                                    'primer_apellido'   => $DataCollection['PrimerApellido'],
                                    'segundo_apellido'  => $DataCollection['SegundoApellido'],
                                    'email_contacto'    => $DataCollection['Email'],
                                    'cod_carrera'       => $DataCollection['Carrera'],
                                    'rubro'             => $DataCollection['Rubro'],
                                    'valor'             => $Valor,
                                    'periodo'           => $DataCollection['Periodo']
                                );
                    $data_json = json_encode($datos);
                    $opciones = array('http' => array(
                                    'method'  => 'POST',
                                    'header'  => 'Content-type: application/json',
                                    'content' => $data_json
                                ));
                    $contexto  = stream_context_create($opciones);
                    $resAPI = file_get_contents($url, false, $contexto);
                    $VerificarAPI = json_decode($resAPI);
                    if($VerificarAPI['status'] == 1){
                        return json_decode($resAPI);  
                    }
                    else{
                        $hoy = date('l jS \of F Y h:i:s A');
                        $file = fopen("../error/error-log.txt", "a");
                        fwrite($file, "Fecha: ".$hoy. PHP_EOL);
                        fwrite($file, "Status API: ".$VerificarAPI->status. PHP_EOL);
                        fwrite($file, $VerificarAPI->statuscode. PHP_EOL);
                        fwrite($file, $VerificarAPI->message. PHP_EOL);
                        fwrite($file, $VerificarAPI->error. PHP_EOL);
                        fwrite($file, "*************************". PHP_EOL);
                        fclose($file);
                        return array('status'=>0,'message'=>"FALLÓ LA RESPUESTA A SU SOLICITUD, INTENTE MAS TARDE POR FAVOR");
                    }
                }
                else
                {   return $response;   }
            }
        }
    }
?>