<?php
    date_default_timezone_set('America/Guayaquil');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="57x57" href="images//favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images//favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images//favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images//favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images//favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images//favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images//favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images//favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images//favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images//favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images//favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images//favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images//favicon/favicon-16x16.png">
    <link rel="manifest" href="images//favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images//favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Congreso CIETEC</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="fonts/fontawesome/css/all.css" rel="stylesheet"> <!--load all styles -->
     <!-- Bootstrap core CSS - JS -->
     <link href="css/bootstrap.min.css" rel="stylesheet">
     <script src="js/bootstrap.min.js"></script>
     <!-- CSS Personalizable -->
     <link href="css/style.css" rel="stylesheet">
     <link href="plugins/fontawesome/css/all.css" rel="stylesheet">
     <?php require_once('utils/css.php'); ?>
</head>
<body class="bg-light">
    <div class="container">
      <main>
        <div class="py-5 text-center">
          <img class="d-block mx-auto mb-4" src="images/logo/UG.png" alt="">
          <h2>CONGRESO CIETEC</h2>
          <span class="lead"><b>Facultad de Filosofía, Letras y Ciencias de la Educación</b></span>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <h5 class="card-header text-center">Formulario de Inscripción de Participantes</h5>
                    <div class="card-body form-horizontal">
                        <div class="form-group row">
                            <label for="CI" class="col-sm-2 col-xs-12 col-form-label font-weight-bold">Cédula</label>
                            <div class="col-sm-10 col-xs-12">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-sm" type="text" id="CI" name="CI" onkeyup="Mayus(this);" onblur="return BuscarUsuario();" autocomplete="off" required>
                                    <span class="input-group-text">*</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="PrimerNombre" class="col-sm-2 col-xs-12 col-form-label font-weight-bold">Nombres</label>
                            <div class="col-sm-10 col-xs-12">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-sm" type="text" id="PrimerNombre" name="PrimerNombre" placeholder="Primer Nombre" onkeyup="Mayus(this);" autocomplete="off" required>
                                    <span class="input-group-text">*</span>
                                    <input class="form-control form-control-sm" type="text" id="SegundoNombre" name="SegundoNombre" placeholder="Segundo Nombre" onkeyup="Mayus(this);" autocomplete="off">
                                    <span class="input-group-text">*</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="PrimerApellido" class="col-sm-2 col-xs-12 col-form-label font-weight-bold">Apellidos</label>
                            <div class="col-sm-10 col-xs-12">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-sm" type="text" id="PrimerApellido" name="PrimerApellido" placeholder="Apellido Paterno" onkeyup="Mayus(this);" autocomplete="off" required>
                                    <span class="input-group-text">*</span>
                                    <input class="form-control form-control-sm" type="text" id="SegundoApellido" name="SegundoApellido" placeholder="Apellido Materno" onkeyup="Mayus(this);" autocomplete="off" required>
                                    <span class="input-group-text">*</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Email" class="col-sm-2 col-xs-12 col-form-label font-weight-bold">E-mail</label>
                            <div class="col-sm-10 col-xs-12">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-sm" type="email" id="Email" name="Email" autocomplete="off" onkeyup="Minus(this);" required>
                                    <span class="input-group-text">*</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="CI" class="col-sm-2 col-xs-12 col-form-label font-weight-bold">Forma de Inscripción</label>
                            <div class="col-sm-10 col-xs-12">
                                <div class="input-group input-group-sm">
                                    <select name="Rubros" class="form-control form-control-sm" id="Rubros" onchange="return ChangeSelect();">
                                        <option value="">SELECCIONE</option>
                                        <option Periodo="2021 - 2022 CI" Rubro="1356" Carrera="040122" value="30">PONENTES DOCENTES UNIVERSIDAD DE GUAYAQUIL</option>
                                        <option Periodo="2021 - 2022 CI" Rubro="1357" Carrera="040122" value="15">PONENTES ESTUDIANTES UNIVERSIDAD DE GUAYAQUIL</option>
                                        <option Periodo="2021 - 2022 CI" Rubro="1358" Carrera="040122" value="35">PONENTES EXTERNOS</option>
                                        <option Periodo="2021 - 2022 CI" Rubro="1359" Carrera="040122" value="10">ASISTENTES</option>
                                    </select>
                                    <span class="input-group-text">*</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-xs-12 col-form-label font-weight-bold">Valor</label>
                            <div class="col-sm-3 col-xs-2">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-sm" type="text" id="Valor" name="Valors" autocomplete="off" onkeyup="Minus(this);" disabled>
                                    <span class="input-group-text">*</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button type="button" class="btn btn-danger" onclick="return CLS();"><i class="fas fa-trash"></i>&nbsp;LIMPIAR FORMULARIO</button>
                        <button type="button" class="btn btn-primary" onclick="return Guardar();"><i class="fas fa-share-square"></i>&nbsp;ENVIAR</button>
                    </div>
                  </div>
            </div>
        </div>
      </main>
      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2021 – 2022 Universidad de Guayaquil</p>
      </footer>
    </div>
    <!-- All JS -->
    <?php require_once('utils/js.php');  ?>
    <!-- JS Personalizable -->
    <script src="js/revision.js"></script>
</body>
</html>