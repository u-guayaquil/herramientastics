<div class="d-flex flex-column flex-root">
	<!--begin::Login-->
	<div class="login login-4 login-signin-on d-flex flex-row-fluid" id="kt_login">
		<div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat" style="background-image: url('assets/media/bg/bg-3.jpg');">
			<div class="login-form text-center p-7 position-relative overflow-hidden">
				<!--begin::Login Header-->
				<div class="d-flex flex-center mb-15">
					<a href="#">
						<img src="img/UG2021.png" class="max-h-135px" alt="" />
					</a>
				</div>
				<!--end::Login Header-->
				<!--begin::Login Sign in form-->
				<div class="login-signin">
					<div class="mb-5">
						<h3>Ficha del Estudiante</h3>
						<div class="text-muted font-weight-bold">
                            <span id="spIpcliente"></span><br />
                                Por favor ingrese las credenciales de su correo institucional:
                        </div>
	    			</div>
					<form class="form" id="frmInicioSesionFichaEstudiante">
						<div class="form-group mb-5">
							<input class="form-control h-auto form-control-solid py-4 px-8" type="text" placeholder="Correo institucional" id="usuario-institucional" name="usuario-institucional" autocomplete="off" />
						</div>
						<div class="form-group mb-5">
							<input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="Contraseña" id="clave-institucional" name="clave-institucional" />
						</div>								
						<button id="btnIniciarSesion" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Iniciar sesión</button>
					</form>
				</div>
				<!--end::Login Sign in form-->
			</div>
		</div>
	</div>
    <input type="hidden" id="hfldIpCliente" />
	<!--end::Login-->
</div>
<script src="assets/js/fichaEstudiante.js?v=2"></script> 
<script>
    $(document).ajaxStop($.unblockUI);
    $(document).ready(function () {
        $("#btnIniciarSesion").click(function (e) {
            e.preventDefault();
            var flagValidacion = 1;
            var mensaje = "";

            if ($("#usuario-institucional").val().length <= 0) {
                flagValidacion = 0;
                mensaje += "- Debe ingresar un <b>Correo Institucional</b><br />";
            }

            if ($("#clave-institucional").val().length <= 0) {
                flagValidacion = 0;
                mensaje += "- Debe ingresar una <b>Clave</b> válida<br />";
            }

            if (parseInt(flagValidacion) == 1) {
                IniciarSesion();
            }
            else {
                Swal.fire({
                    html: mensaje,
                    icon: "warning",
                    allowOutsideClick: false,
                    buttonsStyling: false,
                    confirmButtonText: "Ok, entendido!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                });
            }
        });
        //============================================================================
        // CAPTURA DE IP PUBLICA
        //============================================================================
        $.ajax({
            type: "GET",
            url: "https://api.ipify.org?format=json",
            dataType: "json",
            async: false,
            success: function (result) {
                $("#spIpcliente").text("Su IP es: " + result.ip);
                $("#hfldIpCliente").val(result.ip);
            },
            error: function (request, status, error) {
                $.unblockUI();
            }
        });
    });
</script>