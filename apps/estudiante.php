<div class="col-md-12 col-md-offset-1 center-items">
    <div class="col-md-10 text-center" style="margin: 50px;">
        <h1 class="center text-dark font-weight-bold">EXPORTAR MATERIAS ESTUDIANTE</h1>
        <form id="frmEstudiante" method="POST" action="src/estudiantes/Excel.estudiante.php" autocomplete="nope">
            <div class="form-group row center">
                <div class="col-md-4"></div>
                <div class="col-md-4 center">
                    <label><b>EXPORTAR POR: </b></label>
                    <select  name="Tipo" id="Tipo" class="form-control mb-3" onchange="return ChangeSelect();" required>
                        <option value="" selected>SELECCIONE TIPO</option>
                        <option value="1">CARRERA</option>
                        <option value="2">MATERIA</option>
                        <option value="3">ESTUDIANTE</option>
                    </select>
                </div>
            </div>  
            <div class="card-body"></div>
        </form>
    </div>
</div>
<script src="assets/js/estudiante.js?v=2"></script> 