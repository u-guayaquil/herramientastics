<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EXPORTAR ESTUDIANTES - UG</title>
    <link  rel="icon"   href="img/favicon.png" type="image/png" />
    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>   
    <!-- CSS Personalizable -->
    <link href="css/style.css" rel="stylesheet">	
</head>
<body>
    <div class="col-md-12 col-md-offset-1 center-items">
        <div class="col-md-10" style="margin: 50px;">
            <a href="index.php" style="text-decoration:none">
                <h2 class="center">INGRESAR CLAVE A ENCRYPTAR</h2>
            </a>
            <form method="POST" action="index.php" autocomplete="nope">
            <div class="form-group row center">
                <div class="col-md-4"></div>
                <div class="col-md-4 center">
                <input type="text" name="Clave" id="Clave" class="form-control form-input mb-3" style="padding:20px;" autocomplete="off" required><br />
                </div>
            </div>   
            <div class="form-group row center">
                <div class="col-md-4"></div>
                <div class="col-md-4 center">
                <input type="submit" name="insert" class="btn btn-success btn-lg" value="ENVIAR"><br/>
                </div>
            </div> 
            <?php 
                if(isset($_POST['Clave']) && $_POST['Clave']!=""){
                    ?>  
                    <div class="form-group row" style="margin:20px;">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 center d-inline p-2 bg-dark text-white">
                            <?php echo md5($_POST['Clave']);?><br/>
                        </div>
                    </div> 
                    <?php
                } 
            ?>
            </form>
    </div>
</div>
</body>
</html>