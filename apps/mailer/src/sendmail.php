<?php 
date_default_timezone_set('America/Guayaquil');
require_once ("class.php");

    $HTML='
            <html lang="en">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1"> 
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="format-detection" content="telephone=no">
            <title>Notific.</title>
            </head>
            
            <body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="justify">
            <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
            <tr>
            
            
            <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
            <br>
            <table border="0" cellpadding="0" cellspacing="0" class="container" style="width:100%;max-width:850px;">
            <tr>
            <td class="container-padding header" align="Center" style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:blue;padding-left:24px;padding-right:24px;">
            
            </td>
            </tr>
            <tr>
            <td>
                <img src="http://www.ug.edu.ec/wp-content/uploads/2021/04/BANNER-MAIL.png" style="width:100%;max-width:900px;" >
            </td>
            </tr>
            <tr>
            <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff;">
            <br/>
            <div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;">
            </div>
            <br/>
            <div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550;"> 
                Estimado(a) Estudiante:
            </div>
            
            <div >
                <p align="justify">				
                Por medio del presente informo a usted que, debe acercarse a la Universidad de Guayaquil en la puerta G 3, 
                en las instalaciones del Centro de Atención al Usuario a realizar el retiro formal de su carnet estudiantil, 
                los días 8,9,10 de junio del presente año en horario de 08h30 am hasta las 16h00 pm. 
            </br></br>
                Es importante señalar que el retiro de su credencial es obligatorio, en caso de no poder asistir personalmente 
                en las fechas señaladas, puede delegar a un familiar o persona de su absoluta confianza a hacer el retiro del 
                mismo con una <b>AUTORIZACIÓN SIMPLE EN EL CUAL DEBERÁ ADJUNTAR COPIA A COLOR DE SU CEDULA Y DE LA PERSONA QUE RETIRA</b>.    
            </br></br>
                Cabe indicar que la obtención del carnet estudiantil será el único documento con el cual se les permitirá el 
                acceso a los predios Universitarios.  
            </br></br>
                <b>Nota aclaratoria:</b> Solo deberán de acercarse los estudiantes a quienes se hubiere convocado mediante correo electrónico, los demás tendrán que esperar que les llegue el comunicado; es importante señalar que los estudiantes deben actualizar los datos del SIUG para emitir su credencial. (Si usted ya retiro su credencial omita este correo) 
            </br></br>
                <b>ESTE CORREO ES PERSONAL E INTRANSFERIBLE</b>.   
                </br></br>
            
                </p>
            </div>
            </td>
            </tr>
            <tr>
            <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
            <br><br><br><br>					
            Direcci&oacute;n de Gesti&oacute;n Tecnol&oacute;gica de la Informaci&oacute;n
            <br>Universidad de Guayaquil &copy;
            <br>2021
            <br>
            <br><br>
            </td>
            </tr>
            </table>
            </td>
            </tr>  
            </table>
            </body>
            </html>
        ';  
    $DataCollection = [];
    $linea = 0;
    //Abrimos nuestro archivo
    $archivo = fopen("prueba.csv", "r");
    //Lo recorremos
    while (($datos = fgetcsv($archivo, ",")) == true) 
    {
    $num = count($datos);
    $linea++;
    //Recorremos las columnas de esa linea
        for ($columna = 0; $columna < $num; $columna++) 
        {   $Registro = $datos[$columna];
        }
        $DataCollection[] = $Registro;
    }
    //Cerramos el archivo
    fclose($archivo);
    //Empieza la ejecución de envío de correos
    $Correos = $DataCollection;
    $Cantidad = 1;
    $Count = 0;
    foreach($Correos as $Correo){
        if($Count >= 15){
            $Count = 0;
        }
        $Mail = new Mailer();
        $Mail->setFromName("UNIVERSIDAD DE GUAYAQUIL");
        $Mail->setSubject(utf8_decode("Encuesta para convocar a los diferentes géneros artísticos de la Universidad de Guayaquil"));
        $Mail->setBodyHTML(utf8_decode($HTML));
        $Mail->addTo($Correo);
        $respuesta = $Mail->enviaCorreo($Count);
        $NumeroCuenta = $Count+1;
        if($respuesta == 1){
            echo '-'.$Cantidad.'-OK-</br>';
            $hoy = date('l jS \of F Y h:i:s A');
            $file = fopen("debug/debug.log", "a");
            fwrite($file, "Fecha: ".$hoy. PHP_EOL);
            fwrite($file, "Status Procces: OK". PHP_EOL);
            fwrite($file, "Número de Cuenta:".$NumeroCuenta. PHP_EOL);
            fwrite($file, "Cuenta Correo:".$Correo. PHP_EOL);
            fwrite($file, "Counter Email:".$Cantidad. PHP_EOL);
            fwrite($file, "*************************". PHP_EOL);
            fclose($file);
        }
        else{
            echo '-'.$Cantidad.'-ERROR-</br>';
            $hoy = date('l jS \of F Y h:i:s A');
            $file = fopen("error/error.log", "a");
            fwrite($file, "Fecha: ".$hoy. PHP_EOL);
            fwrite($file, "Status Procces: ERROR". PHP_EOL);
            fwrite($file, "Número de Cuenta:".$NumeroCuenta. PHP_EOL);
            fwrite($file, "Cuenta Correo:".$Correo. PHP_EOL);
            fwrite($file, "Counter Email:".$Cantidad. PHP_EOL);
            fwrite($file, "*************************". PHP_EOL);
            fclose($file);
        }
        $Count++; $Cantidad++;
    }
?>
