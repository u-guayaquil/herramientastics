<?php
        require_once("PHPMailer.php");
        require_once("SMTP.php");

        class  Mailer
        {       private $ObjMail;
                private $FrmName;
                private $Subject;
                private $Destiny;
                private $BodyHTM;
                private $CopyTo;
                private $Cuenta = array('webmaster1@ug.edu.ec','webmaster2@ug.edu.ec','webmaster3@ug.edu.ec','webmaster4@ug.edu.ec'
                                        ,'webmaster5@ug.edu.ec','webmaster6@ug.edu.ec','webmaster7@ug.edu.ec','webmaster8@ug.edu.ec'
                                        ,'webmaster9@ug.edu.ec','webmaster10@ug.edu.ec','webmaster11@ug.edu.ec','webmaster12@ug.edu.ec'
                                        ,'webmaster13@ug.edu.ec','webmaster14@ug.edu.ec','webmaster15@ug.edu.ec');
                /*private $Cuenta = array('notify.posgrado@ug.edu.ec');*/
                private $Tokens = "WEB1.2*UG";
                /*private $Tokens = "Nposg21*";*/

                function __construct()
                {
                }

                function setFromName ($frmn) {  $this->FrmName   = $frmn;  }
                function setSubject  ($subj) {  $this->Subject   = $subj;  }
                function setBodyHTML ($html) {  $this->BodyHTM   = $html;  }
                function addTo       ($toem) {  $this->Destiny[]   = $toem;  }
                function addCopyTo   ($tocp) {  $this->CopyTo    = $tocp;  }

                /*protected function EmailDecoder($cuentas)
                {       $vcorreos = array();
                        $elements = explode(';',$cuentas);
                        for ($m=0; $m<count($elements); $m++)
                        {   if (filter_var($elements[$m], FILTER_VALIDATE_EMAIL))
                            {   $vcorreos [] = trim($elements[$m]);
                            }
                        }
                        return $vcorreos;
                }*/

                function enviaCorreo($NumeroCuenta)
                {       $ecorreos = $this->Destiny;
                        $Cuenta = $this->Cuenta[$NumeroCuenta];
                        if (count($ecorreos)>0)
                        {   $this->ObjMail = new PHPMailer();
                            $this->ObjMail->SetLanguage("es", '../language/');
                            $this->ObjMail->IsSMTP();
                            $this->ObjMail->Host = 'smtp.office365.com';
                            $this->ObjMail->Port = 587;
                            $this->ObjMail->SMTPSecure = 'tls';
                            $this->ObjMail->SMTPAuth = true;
                            $this->ObjMail->Username = $Cuenta;
                            $this->ObjMail->Password = $this->Tokens;
                            $this->ObjMail->From = $Cuenta;
                            $this->ObjMail->FromName = $this->FrmName;
                            $this->ObjMail->Timeout = 30;
   			    $this->ObjMail->SMTPOptions = ['ssl'=> ['allow_self_signed' => true]];
                            for ($m=0; $m<count($ecorreos); $m++)
                            {    $this->ObjMail->AddAddress($ecorreos[$m]);
                            }
                            
                            //print_r(count($this->CopyTo));
                                if(!empty($this->CopyTo))
                                {   $ccorreos = $this->CopyTo;
                                        for ($m=0; $m<count($ccorreos); $m++)
                                        {       $this->ObjMail->AddCC($ccorreos[$m]);
                                        }
                                }
                                $this->ObjMail->Subject = $this->Subject;
                                $this->ObjMail->IsHTML(true);
                                $this->ObjMail->Body = trim($this->BodyHTM);
                                return ($this->ObjMail->Send() ? "1":"0");
                        }
                        return array(false,"No existen destinatarios del correo.");
                }
        }
?>
