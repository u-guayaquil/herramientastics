    
<style>
    .k-grid tbody .k-button {
        min-width: 32px;
        width: 32px;
        text-align: center;
        margin-left: 2px;
        padding-left: 2px;
    }
    .k-grid-content {
        max-height: 400px;
    }
</style>
<form id="frmInventarioTIC" method="post" enctype="multipart/form-data">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-7 subheader-transparent" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Details-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Inventario TIC</h5>
                <!--end::Title-->
                <!--begin::Separator-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <!--end::Separator-->
            </div>
            <!--end::Details-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Nuevo-->
                <a id="btnNuevoInventario" href="#" class="btn btn-primary font-weight-bolder btn-sm px-4 font-size-base ml-2">
                    <span class="svg-icon svg-icon-md fas fa-file-invoice"></span>&nbsp;Nuevo
                </a>
                <!--end::Nuevo-->
                <!--begin::Consultar-->
                <a id="btnBuscarInventario" href="#" class="btn btn-primary font-weight-bolder btn-sm px-3 font-size-base ml-2">
                    <span class="svg-icon svg-icon-md fas fa-search"></span>&nbsp;Buscar
                </a>
                <!--end::Consultar-->
                <!--begin::Exportar Excel-->
                <a id="btnExportarExcelInventario" href="#" class="btn btn-primary font-weight-bolder btn-sm px-3 font-size-base ml-2">
                    <span class="svg-icon svg-icon-md fas fa-file-excel"></span>&nbsp;Exportar Excel
                </a>
                <!--end::Exportar Excel-->
                <!--begin::Regresar-->
                <a id="btnRegresarInventario" href="#" class="btn btn-primary font-weight-bolder btn-sm px-4 font-size-base ml-2" style="display:none;">
                    <span class="svg-icon svg-icon-md fas fa-arrow-alt-circle-left"></span>&nbsp;Regresar
                </a>
                <!--end::Regresar-->
                <!--begin::Grabar-->
                <a id="btnGrabarInventario" href="#" class="btn btn-primary font-weight-bolder btn-sm px-3 font-size-base ml-2" style="display:none;">
                    <span class="svg-icon svg-icon-md fas fa-save"></span>&nbsp;Grabar
                </a>
                <!--end::Grabar-->
                
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <div id="divConsultas">        
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container-fluid">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label text-left">
                                Criterio de búsquedas
                                <span class="d-block text-muted pt-2 font-size-sm">Seleccione los criterios de búsquedas a continuación y presione el botón Buscar para mostrar la información</span>
                            </h3>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtFacultadId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Facultad</label>
                                <input id="txtFacultadId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtFacultad" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtFacultad" type="text" class="form-control input-sm" placeholder="Facultad" readonly />
                                    <button id="btnBuscarFacultadI" class="btn btn-success btnInicial" data-accion="FACULTAD ESPECIFICO"><i class="fas fa-search"></i></button>
                                    <button id="btnBorrarFacultadI" class="btn btn-danger btnInicialLimpieza" data-accion="FACULTAD ESPECIFICO"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtEstadoIniId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Estado</label>
                                <input id="txtEstadoIniId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtEstadoIni" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtEstadoIni" type="text" class="form-control input-sm" placeholder="Estado" readonly />
                                    <button class="btn btn-success btnInicial" data-accion="ESTADO"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-danger btnInicialLimpieza" data-accion="ESTADO"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                        </div>           
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <div id="grdInventario"></div>
                    </div>
                </div>   
            </div>
        </div>  
    </div>
    <div id="divTransaccion" style="display:none;">
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container-fluid">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 id="hTitulo" class="card-label text-left"></h3>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtCodigo" class="font-weight-bold">Código</label>
                                <input id="txtCodigo" type="text" class="form-control input-sm" readonly />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtFacultadMantId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Facultad</label>
                                <input id="txtFacultadMantId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-10 col-xs-12">
                                <label for="txtFacultadMant" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtFacultadMant" type="text" class="form-control input-sm" placeholder="Facultad" readonly />
                                    <button id="btnBuscarFacultadM" class="btn btn-success btnBusquedas" data-accion="FACULTAD ESPECIFICO"><i class="fas fa-search"></i></button>
                                    <button id="btnBorrarFacultadM" class="btn btn-danger btnLimpieza" data-accion="FACULTAD ESPECIFICO"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="txtEdificio" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Edificio</label>
                                <input id="txtEdificio" type="text" class="form-control input-sm" />
                            </div>
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="txtDepartamento" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Departamento</label>
                                <input id="txtDepartamento" type="text" class="form-control input-sm" />
                            </div>                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtTipoId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Tipo Equipo</label>
                                <input id="txtTipoId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-10 col-xs-12">
                                <label for="txtTipo" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtTipo" type="text" class="form-control input-sm" placeholder="Tipo" readonly />
                                    <button class="btn btn-success btnBusquedas" data-accion="TIPO_ARTICULO"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-danger btnLimpieza" data-accion="TIPO_ARTICULO"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtMarcaId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Marca</label>
                                <input id="txtMarcaId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-10 col-xs-12">
                                <label for="txtMarca" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtMarca" type="text" class="form-control input-sm" placeholder="Marca" readonly />
                                    <button class="btn btn-success btnBusquedas" data-accion="MARCA"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-danger btnLimpieza" data-accion="MARCA"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtModelo" class="font-weight-bold">Modelo</label>
                                <input id="txtModelo" type="text" class="form-control input-sm" />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtSerie" class="font-weight-bold">Serie</label>
                                <input id="txtSerie" type="text" class="form-control input-sm" />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtCodigoInventario" class="font-weight-bold">Código Inventario</label>
                                <input id="txtCodigoInventario" type="text" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtEstadoId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Estado</label>
                                <input id="txtEstadoId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtEstado" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtEstado" type="text" class="form-control input-sm" placeholder="Estado" readonly />
                                    <button class="btn btn-success btnBusquedas" data-accion="ESTADO"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-danger btnLimpieza" data-accion="ESTADO"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="txtUsuarioInventario" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Usuario Responsable&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                <input type="checkbox" class="form-check-input" id="chkAdministradorEdificio">
                                <label class="form-check-label" for="chkAdministradorEdificio">Administrador Edificio</label>
                                <input id="txtUsuarioInventario" type="text" class="form-control input-sm" />
                            </div>
                        </div>                    
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <input id="hfldLista" type="hidden" />
    <input id="hfldTipoAccion" type="hidden" />
</form>
<div class="modal fade" id="diag-lista" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Consulta de datos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
            <div class="form-group col-sm-12 col-xs-12">
                <div id="grdLista"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="assets/js/inventarioTic.js?v=3"></script>
<script>
    function CargarDataGrid() {
        $("#grdInventario").data('kendoGrid').dataSource.data([]);
        $("#grdInventario").data("kendoGrid").refresh();
        $('#grdInventario').data('kendoGrid').dataSource.read();
        $("#grdInventario").data("kendoGrid").refresh();
    }
    function CargarLista() {
        $("#grdLista").data('kendoGrid').dataSource.data([]);
        $("#grdLista").data("kendoGrid").refresh();
        $('#grdLista').data('kendoGrid').dataSource.read();
        $("#grdLista").data("kendoGrid").refresh();
    }

	$(document).ready(function () {
        ConsultaFacultad("1");
        $('#txtUsuarioInventario').keypress(function (e) {
            var regex = new RegExp("^[a-zA-ZñÑ, ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;            
        });
        /*$('#txtModelo').keypress(function (e) {
            var regex = new RegExp("^[a-zA-ZñÑ0-9]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;
        });*/
        $('#txtSerie').keypress(function (e) {
            var regex = new RegExp("^[a-zA-ZñÑ0-9]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;
        });
        $('#txtCodigoInventario').keypress(function (e) {
            var regex = new RegExp("^[a-zA-ZñÑ0-9]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;
        });
        $('input').keyup(function () {
            if ($(this).attr('name') == 'txtEmail') {
                this.value = this.value.toLowerCase();
            }
            else {
                this.value = this.value.toUpperCase();
            }                
        });
        $('#chkAdministradorEdificio').change(function () {
            if (this.checked) {
                $("#txtUsuarioInventario").val("");
                $("#txtUsuarioInventario").attr('readonly','readonly');
            }
            else {
                $("#txtUsuarioInventario").removeAttr('readonly');
            }
        });
        $(".btnInicial").click(function (e){
            var transaccion = $(this).attr('data-accion');
            $("#hfldLista").val(transaccion);
            $("#hfldTipoAccion").val("1");
            CargarLista();
            $("#diag-lista").modal("show");
            return false;
        });
        $(".btnInicialLimpieza").click(function (e){
            e.preventDefault();
            var transaccion = $(this).attr('data-accion');
            
            switch(transaccion){
                case "FACULTAD":
                    $("#txtFacultadId").val("");
                    $("#txtFacultad").val("");
                    break;
                case "ESTADO":
                    $("#txtEstadoIniId").val("");
                    $("#txtEstadoIni").val("");                
                    break;
            }

            return false;
        });

        $(".btnBusquedas").click(function (e){
            var transaccion = $(this).attr('data-accion');
            $("#hfldLista").val(transaccion);
            $("#hfldTipoAccion").val("2");
            CargarLista();
            $("#diag-lista").modal("show");
            return false;
        });
        $(".btnLimpieza").click(function (e){
            e.preventDefault();
            var transaccion = $(this).attr('data-accion');
            
            switch(transaccion){
                case "FACULTAD":
                    $("#txtFacultadMantId").val("");
                    $("#txtFacultadMant").val("");
                    break;
                case "FACULTAD ESPECIFICO":
                    $("#txtFacultadMantId").val("");
                    $("#txtFacultadMant").val("");
                    break;
                case "TIPO_ARTICULO":
                    $("#txtTipoId").val("");
                    $("#txtTipo").val("");                     
                    break;
                case "AMBIENTE_EQUIPO":
                    $("#txtAmbienteId").val("");
                    $("#txtAmbiente").val("");                    
                    break;
                case "MARCA":
                    $("#txtMarcaId").val("");
                    $("#txtMarca").val("");                
                    break;
                case "ESTADO":
                    $("#txtEstadoId").val("");
                    $("#txtEstado").val("");                
                    break;
            }

            return false;
        });
        $("#btnNuevoInventario").click(function (e){
            e.preventDefault();
            var url = "apps/mantInventarioti.php?regId=0";
            SetApp(url);
        });
        $("#btnBuscarInventario").click(function (){
            if($("#txtFacultadId").val().length > 0 && $("#txtEstadoIniId").val().length > 0){
                CargarDataGrid();
            }
            else{
                Swal.fire({
                    html: "Debe escoger una opción de Facultad y de Estado para realizar la búsqueda",
                    icon: "info",
                    allowOutsideClick: false,
                    buttonsStyling: false,
                    confirmButtonText: "Ok, entendido!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                });
            }            
        });
        $("#btnExportarExcelInventario").click(function (){
            var grid = $("#grdInventario").data("kendoGrid");
            grid.saveAsExcel();
        });
        //=======================================================================
        //Configuracion de grid Lista
        //=======================================================================
        $("#grdLista").kendoGrid({
            autoBind: false,
            dataSource: {
                transport: {
                    read: {
                        url: 'src/inventario/consumirInventario.php',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: {
                            transaccion: function () { return $("#hfldLista").val(); },
                            tipoAccion: function () { return $("#hfldTipoAccion").val(); }
                        },
                        type: "GET"
                    }
                },
                schema: {
                    data: "objetos",
                    model: {
                        id: "id",
                        fields: {
                            id: { editable: false, type: "string" },
                            descripcion: { editable: false, type: "string" }
                        }
                    }
                },
                pageSize: 50,
                requestStart: function () {
                    $.blockUI({
                        message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                    });
                },
                requestEnd: function () {
                    $.unblockUI();
                },
                error: function (e) {
                    $.unblockUI();
                }
            },
            filterable: true,
            filterable: {
                mode: "row"
            },
            sortable: true,
            pageable: false,
            groupable: false,
            scrollable: true,
            navigatable: true,
            persistSelection: true,
            editable: false,
            mobile: true,
            columns: [
                {
                    field: "id",
                    title: "Código",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: right;"
                    },
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    width: "80px"
                },
                {
                    field: "descripcion",
                    title: "Descripción",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    width: "250px"
                }
            ]
        });
        $("#grdLista").on("dblclick", " tbody > tr", function () {
                var grid = $("#grdLista").data("kendoGrid");
                var data = grid.dataItem($(this));
                var Id = data.id;
                var Descripcion = data.descripcion;
                var Transaccion = $("#hfldLista").val();
                var TipoAccion = $("#hfldTipoAccion").val();
                
                switch(Transaccion){
                    case "FACULTAD":
                        if(TipoAccion == "2"){
                            $("#txtFacultadMantId").val(Id);
                            $("#txtFacultadMant").val(Descripcion);
                        }
                        else{
                            $("#txtFacultadId").val(Id);
                            $("#txtFacultad").val(Descripcion);
                        }
                        break;
                    case "FACULTAD ESPECIFICO":
                        if(TipoAccion == "2"){
                            $("#txtFacultadMantId").val(Id);
                            $("#txtFacultadMant").val(Descripcion);
                        }
                        else{
                            $("#txtFacultadId").val(Id);
                            $("#txtFacultad").val(Descripcion);
                        }
                        break;
                    case "TIPO_ARTICULO":
                        $("#txtTipoId").val(Id);
                        $("#txtTipo").val(Descripcion);
                        break;
                    case "AMBIENTE_EQUIPO":
                        $("#txtAmbienteId").val(Id);
                        $("#txtAmbiente").val(Descripcion);
                        break;
                    case "MARCA":
                        $("#txtMarcaId").val(Id);
                        $("#txtMarca").val(Descripcion);
                        break;
                    case "ESTADO":
                        if(TipoAccion == "2"){
                            $("#txtEstadoId").val(Id);
                            $("#txtEstado").val(Descripcion);
                        }
                        else{
                            $("#txtEstadoIniId").val(Id);
                            $("#txtEstadoIni").val(Descripcion);
                        }            
                    break;
                }

                $('#diag-lista').modal('hide');            
        });
        //=======================================================================
        //Configuracion del grid Datos
        //=======================================================================
        $("#grdInventario").kendoGrid({
            autoBind: false,
            dataSource: {
                transport: {
                    read: {
                        url: 'src/inventario/consumirInventario.php',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: {
                            transaccion: "CONSULTA_GENERAL",
                            tipoAccion: "1",
                            facultadId:  function () { return $("#txtFacultadId").val(); },
                            estadoId:  function () { return $("#txtEstadoIniId").val(); }
                        },
                    type: "GET"
                }
                },
                group: {
                    field: "facultad",
                    dir: "asc"
                },
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { editable: false, type: "number" },
                                tipo: { editable: false, type: "string" },
                                facultad: { editable: false, type: "string" },
                                edificio: { editable: false, type: "string" },
                                departamento: { editable: false, type: "string" },
                                marca: { editable: false, type: "string" },
                                modelo: { editable: false, type: "string" },
                                serie: { editable: false, type: "string" },
                                codigoInventario: { editable: false, type: "string" },
                                responsable: { editable: false, type: "string" },
                                administradorEdificio: { editable: false, type: "string" },
                                estadoId: { editable: false, type: "string" },
                                estado: { editable: false, type: "string" },
                                estadoRegistro: { editable: false, type: "string" },
                                usuarioRegistro: { editable: false, type: "string" },
                                usuarioEdicion: { editable: false, type: "string" },
                                fechaRegistro: { editable: false, type: "string" },
                                fechaEdicion: { editable: false, type: "string" }
                            }
                        }
                    },
                    requestStart: function () {
                        $.blockUI({
                            message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                        });
                    },
                    requestEnd: function () {
                        $.unblockUI();
                    },
                    error: function (e) {
                        $.unblockUI();
                    }
                },
                excel: {
                    fileName: "InventarioTIC.xlsx",
                    allPages: true,
                    filterable: true
                },
                filterable: true,
                filterable: {
                    mode: "row"
                },
                pageable: false,
                sortable: true,
                pageable: false,
                groupable: false,
                scrollable: true,
                navigatable: true,
                persistSelection: true,
                editable: false,
                mobile: true,
                columns: [
                    {
                        command:
                        [
                            {
                                text: "<span class='fas fa-edit'></span>",
                                click: function (e) {
                                    e.preventDefault();

                                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                    var url = "apps/mantInventarioti.php?regId=" + dataItem.id;
                                    SetApp(url);
                                },
                                className: "k-grid-custom erp-editar",
                                name: "EditarRegistro"
                            }
                        ],
                        title: "",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: middle; white-space: normal !important; font-weight: bold;"
                        },
                        attributes: {
                            style: "text-align: center;"
                        },
                        width: "40px"
                    },
                    {
                        field: "id",
                        title: "Código",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: rigth;"
                        },
                        width: "120px",
                        filterable: false
                    },
                    {
                        field: "facultad",
                        title: "Facultad",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "150px",
                        filterable: false,
                        hidden: true
                    },
                    {
                        field: "edificio",
                        title: "Edificio",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "150px",
                        filterable: false
                    },
                    {
                        field: "departamento",
                        title: "Departamento",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "150px",
                        filterable: false
                    },
                    {
                        field: "marca",
                        title: "Marca",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "150px",
                        filterable: false
                    },
                    {
                        field: "modelo",
                        title: "Modelo",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "150px",
                        filterable: false
                    },
                    {
                        field: "serie",
                        title: "Serie",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "150px",
                        filterable: false
                    },
                    {
                        field: "codigoInventario",
                        title: "Código Inventario",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "200px",
                        filterable: false
                    },
                    {
                        field: "administradorEdificio",
                        title: "Administrador Edificio",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: center;"
                        },
                        width: "110px",
                        filterable: false
                    },                    
                    {
                        field: "responsable",
                        title: "Responsable",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "160px",
                        filterable: false
                    },
                    {
                        field: "estadoId",
                        hidden: true
                    },
                    {
                        field: "estado",
                        title: "Estado",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "140px",
                        filterable: false
                    },
                    {
                        field: "estadoRegistro",
                        title: "Estado Registro",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "160px",
                        filterable: false
                    },
                    {
                        field: "usuarioRegistro",
                        title: "Usuario Registro",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "160px",
                        filterable: false,
                        hidden: true
                    },
                    {
                        field: "usuarioEdicion",
                        title: "Usuario Edición",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "160px",
                        filterable: false,
                        hidden: true
                    },
                    {
                        field: "fechaRegistro",
                        title: "Fecha Registro",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "160px",
                        filterable: false
                    },
                    {
                        field: "fechaEdicion",
                        title: "Fecha Edición",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                        },
                        attributes: {
                            style: "text-align: left;"
                        },
                        width: "160px",
                        filterable: false,
                        hidden: true
                    }
                ]
            });
		});
	</script>