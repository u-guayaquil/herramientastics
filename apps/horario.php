<?php
    include("../src/utils/libs.php");
?>
<div class="col-md-12 col-md-offset-1 center-items">
    <div class="col-md-12 text-center" style="margin-top: 50px;">
        <h1 class="center text-dark font-weight-bold">CONSULTA DE HORARIOS - UG</h1>
        <form id="frmHorario" method="POST" autocomplete="nope" style="margin-top:25px;">
            <div class="form-row center">
				<div class="form-group col-sm-6 col-xs-12">
					<label for="DocenteAula" class="font-weight-bold">Apellidos de Docente o Número de Aula*</label>
					<input id="DocenteAula" name="DocenteAula" type="text" class="form-control form-control-primary" size = "50" />
				</div>
				<div class="form-group col-sm-5 col-xs-12">
					<label for="Clave" class="font-weight-bold">Clave de Administrador (OPCIONAL)</label>
					<input id="Clave" name="Clave" type="password" class="form-control form-control-primary" size = "50" />
				</div>
				<div class="form-group col-sm-1 col-xs-12">
					<label class="font-weight-bold" style="color:white;">Consultar</label>
					<button id="btnConsultar" class="btn btn-primary" type="button"><i class="fas fa-search icon-sm"></i></button>												
				</div>
			</div>
            <div class="form-row card-body-table"></div>
        </form>
    </div>
</div>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="assets/js/horario.js?v=6"></script> 