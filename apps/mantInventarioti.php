<?php
    $RegistroId = $_GET["regId"];    
?>
<style>
    .k-grid tbody .k-button {
        min-width: 32px;
        width: 32px;
        text-align: center;
        margin-left: 2px;
        padding-left: 2px;
    }
    .k-grid-content {
        max-height: 400px;
    }
</style>
<form id="frmMantInventarioTIC" method="post" enctype="multipart/form-data">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-7 subheader-transparent" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Details-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Inventario TIC</h5>
                <!--end::Title-->
                <!--begin::Separator-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <!--end::Separator-->
            </div>
            <!--end::Details-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Regresar-->
                <a id="btnRegresarInventario" href="#" class="btn btn-primary font-weight-bolder btn-sm px-4 font-size-base ml-2">
                    <span class="svg-icon svg-icon-md fas fa-arrow-alt-circle-left"></span>&nbsp;Regresar
                </a>
                <!--end::Regresar-->
                <!--begin::Grabar-->
                <button id="btnGrabarInventario" class="btn btn-primary font-weight-bolder btn-sm px-3 font-size-base ml-2">
                    <span class="svg-icon svg-icon-md fas fa-save"></span>&nbsp;Grabar
                </button>
                <!--end::Grabar-->                
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->    
    <div id="divTransaccion">
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container-fluid">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 id="hTitulo" class="card-label text-left"></h3>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtCodigo" class="font-weight-bold">Código</label>
                                <input id="txtCodigo" type="text" class="form-control input-sm" value="<?php echo $_GET["regId"]; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtFacultadMantId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Facultad</label>
                                <input id="txtFacultadMantId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-10 col-xs-12">
                                <label for="txtFacultadMant" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtFacultadMant" type="text" class="form-control input-sm" placeholder="Facultad" readonly />
                                    <button id="btnBuscarFacultadM" class="btn btn-success btnBusquedas" data-accion="FACULTAD ESPECIFICO"><i class="fas fa-search"></i></button>
                                    <button id="btnBorrarFacultadM" class="btn btn-danger btnLimpieza" data-accion="FACULTAD ESPECIFICO"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="txtEdificio" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Edificio</label>
                                <input id="txtEdificio" type="text" class="form-control input-sm" />
                            </div>
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="txtDepartamento" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Departamento</label>
                                <input id="txtDepartamento" type="text" class="form-control input-sm" />
                            </div>                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtTipoId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Tipo Equipo</label>
                                <input id="txtTipoId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-10 col-xs-12">
                                <label for="txtTipo" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtTipo" type="text" class="form-control input-sm" placeholder="Tipo" readonly />
                                    <button class="btn btn-success btnBusquedas" data-accion="TIPO_ARTICULO"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-danger btnLimpieza" data-accion="TIPO_ARTICULO"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtMarcaId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Marca</label>
                                <input id="txtMarcaId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-10 col-xs-12">
                                <label for="txtMarca" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtMarca" type="text" class="form-control input-sm" placeholder="Marca" readonly />
                                    <button class="btn btn-success btnBusquedas" data-accion="MARCA"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-danger btnLimpieza" data-accion="MARCA"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtModelo" class="font-weight-bold">Modelo</label>
                                <input id="txtModelo" type="text" class="form-control input-sm" />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtSerie" class="font-weight-bold">Serie</label>
                                <input id="txtSerie" type="text" class="form-control input-sm" />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtCodigoInventario" class="font-weight-bold">Código Inventario</label>
                                <input id="txtCodigoInventario" type="text" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtEstadoId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Estado</label>
                                <input id="txtEstadoId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtEstado" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtEstado" type="text" class="form-control input-sm" placeholder="Estado" readonly />
                                    <button class="btn btn-success btnBusquedas" data-accion="ESTADO"><i class="fas fa-search"></i></button>
                                    <button class="btn btn-danger btnLimpieza" data-accion="ESTADO"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-6 col-xs-12">
                                <label for="txtUsuarioInventario" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Usuario Responsable&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                <input type="checkbox" class="form-check-input" id="chkAdministradorEdificio">
                                <label class="form-check-label" for="chkAdministradorEdificio">Administrador Edificio</label>
                                <input id="txtUsuarioInventario" type="text" class="form-control input-sm" />
                            </div>
                        </div>                    
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <input id="hfldLista" type="hidden" />
    <input id="hfldTipoAccion" type="hidden" />
</form>
<div class="modal fade" id="diag-lista" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Consulta de datos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
            <div class="form-group col-sm-12 col-xs-12">
                <div id="grdLista"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="assets/js/inventarioTic.js?v=3"></script>
<script>
    function CargarLista() {
        $("#grdLista").data('kendoGrid').dataSource.data([]);
        $("#grdLista").data("kendoGrid").refresh();
        $('#grdLista').data('kendoGrid').dataSource.read();
        $("#grdLista").data("kendoGrid").refresh();
    }
    function GrabarArticulo(transaccion){
        $("#btnGrabarInventario").prop("disabled",true);
        $("#btnGrabarInventario").hide();

        $.ajax({
            type: "POST",
            url: "src/inventario/consumirInventario.php",
            dataType: "json",
            async: false,
            data: {
                id:  function () { return $("#txtCodigo").val(); },
                tipoId:  function () { return $("#txtTipoId").val(); },
                facultadId:  function () { return $("#txtFacultadMantId").val(); },
                edificio:  function () { return $("#txtEdificio").val(); },
                departamento:  function () { return $("#txtDepartamento").val(); },
                marcaId:  function () { return $("#txtMarcaId").val(); },
                modelo:  function () { return $("#txtModelo").val(); },
                serie:  function () { return $("#txtSerie").val(); },
                codigoInventario:  function () { return $("#txtCodigoInventario").val(); },
                responsable:  function () { return $("#txtUsuarioInventario").val(); },
                administradorEdificio : function () {
                    if ($('#chkAdministradorEdificio').is(":checked") == true) {
                        return "1";
                    }
                    else{
                        return "0";
                    }
                },
                estadoId:  function () { return $("#txtEstadoId").val(); },
                estadoRegistro: "1",
                transaccion: transaccion
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                });
            },
            success: function (result) {
                $.unblockUI();   

                switch (result.mensaje) {
                    case "OK":
                        SetApp("apps/inventarioti.php");
                        break;
                    default:
                        $("#btnGrabarInventario").prop("disabled",false);
                        $("#btnGrabarInventario").show();

                        Swal.fire({
                            html: result.respuesta,
                            icon: "error",
                            allowOutsideClick: false,
                            buttonsStyling: false,
                            confirmButtonText: "Ok, entendido!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        });
                        break;
                }
            },
            error: function (request, status, error) {
                $.unblockUI();

                $("#btnGrabarInventario").prop("disabled",false);
                $("#btnGrabarInventario").show();

                Swal.fire({
                    text: error,
                    icon: "error",
                    allowOutsideClick: false,
                    buttonsStyling: false,
                    confirmButtonText: "Ok, entendido!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                });
            }
        });
    }

	$(document).ready(function () {
        //=======================================================================
        //Configuracion de combo inicial
        //=======================================================================
        $('#frmMantInventarioTIC').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) { 
                e.preventDefault();
                return false;
            }
        });

        ConsultaFacultad("2");
        var Id = $("#txtCodigo").val();

        if (parseInt(Id) != 0){
            ConsultarRegistro(Id,"CONSULTA_ESPECIFICA");
        }
        //=======================================================================
        //Configuracion de campos
        //=======================================================================
        $('#txtUsuarioInventario').keypress(function (e) {
            var regex = new RegExp("^[a-zA-ZñÑ, ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;            
        });
        $('#txtSerie').keypress(function (e) {
            var regex = new RegExp("^[a-zA-ZñÑ0-9-/]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;
        });
        $('#txtCodigoInventario').keypress(function (e) {
            var regex = new RegExp("^[a-zA-ZñÑ0-9_-]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;
        });
        $('input').keyup(function () {
            if ($(this).attr('name') == 'txtEmail') {
                this.value = this.value.toLowerCase();
            }
            else {
                this.value = this.value.toUpperCase();
            }                
        });
        $('#chkAdministradorEdificio').change(function () {
            if (this.checked) {
                $("#txtUsuarioInventario").val("");
                $("#txtUsuarioInventario").attr('readonly','readonly');
            }
            else {
                $("#txtUsuarioInventario").removeAttr('readonly');
            }
        });
        //=======================================================================
        //Configuracion de botones
        //=======================================================================        
        $(".btnInicial").click(function (e){
            var transaccion = $(this).attr('data-accion');
            $("#hfldLista").val(transaccion);
            $("#hfldTipoAccion").val("1");
            CargarLista();
            $("#diag-lista").modal("show");
            return false;
        });
        $(".btnInicialLimpieza").click(function (e){
            e.preventDefault();
            var transaccion = $(this).attr('data-accion');
            
            switch(transaccion){
                case "FACULTAD":
                    $("#txtFacultadId").val("");
                    $("#txtFacultad").val("");
                    break;
                case "ESTADO":
                    $("#txtEstadoIniId").val("");
                    $("#txtEstadoIni").val("");                
                    break;
            }

            return false;
        });
        $(".btnBusquedas").click(function (e){
            var transaccion = $(this).attr('data-accion');
            $("#hfldLista").val(transaccion);
            $("#hfldTipoAccion").val("2");
            CargarLista();
            $("#diag-lista").modal("show");
            return false;
        });
        $(".btnLimpieza").click(function (e){
            e.preventDefault();
            var transaccion = $(this).attr('data-accion');
            
            switch(transaccion){
                case "FACULTAD":
                    $("#txtFacultadMantId").val("");
                    $("#txtFacultadMant").val("");
                    break;
                case "FACULTAD ESPECIFICO":
                    $("#txtFacultadMantId").val("");
                    $("#txtFacultadMant").val("");
                    break;
                case "TIPO_ARTICULO":
                    $("#txtTipoId").val("");
                    $("#txtTipo").val("");                     
                    break;
                case "AMBIENTE_EQUIPO":
                    $("#txtAmbienteId").val("");
                    $("#txtAmbiente").val("");                    
                    break;
                case "MARCA":
                    $("#txtMarcaId").val("");
                    $("#txtMarca").val("");                
                    break;
                case "ESTADO":
                    $("#txtEstadoId").val("");
                    $("#txtEstado").val("");                
                    break;
            }

            return false;
        });
        $("#btnRegresarInventario").click(function (){
            SetApp("apps/inventarioti.php");
        });
        $("#btnGrabarInventario").click(function (e){
            e.preventDefault();

            var id = $("#txtCodigo").val();
            var flagValidacion = 0;
            var mensaje = "";           

            if($("#txtFacultadMantId").val() == "0" || $("#txtFacultadMantId").val().length <=0){
                flagValidacion = 1;
                mensaje += "- Debe seleccionar una Facultad<br />";
            }
            if($("#txtEdificio").val().length <= 0){
                flagValidacion = 1;
                mensaje += "- Campo Edificio es obligatorio<br />";
            }
            if($("#txtDepartamento").val().length <= 0){
                flagValidacion = 1;
                mensaje += "- Campo Departamento es obligatorio<br />";
            }
            if($("#txtTipoId").val() == "0" || $("#txtTipoId").val().length <=0){
                flagValidacion = 1;
                mensaje += "- Debe seleccionar un Tipo de Equipo<br />";
            }
            if($("#txtMarcaId").val() == "0" || $("#txtMarcaId").val().length <=0){
                flagValidacion = 1;
                mensaje += "- Debe seleccionar una Marca<br />";
            }
            if ($('#chkAdministradorEdificio').is(":checked") != true) {
                if($("#txtUsuarioInventario").val().length <= 0){
                    flagValidacion = 1;
                    mensaje += "- Debe ingresar el Usuario Responsable<br />";
                }
            }            
            if($("#txtEstadoId").val() == "0" || $("#txtEstadoId").val().length <=0){
                flagValidacion = 1;
                mensaje += "- Debe seleccionar un Estado del Equipo<br />";
            }
            
            if(parseInt(flagValidacion) == 0){
                if(parseInt(id) <= 0){
                    GrabarArticulo("INGRESAR");
                }
                else{
                    GrabarArticulo("MODIFICAR");
                }
            }
            else{
                Swal.fire({
                    html: mensaje,
                    icon: "warning",
                    allowOutsideClick: false,
                    buttonsStyling: false,
                    confirmButtonText: "Ok, entendido!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                });
            }

            return false;
        });
        //=======================================================================
        //Configuracion grid Lista
        //=======================================================================        
        $("#grdLista").kendoGrid({
            autoBind: false,
            dataSource: {
                transport: {
                    read: {
                        url: 'src/inventario/consumirInventario.php',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: {
                            transaccion: function () { return $("#hfldLista").val(); },
                            tipoAccion: function () { return $("#hfldTipoAccion").val(); }
                        },
                        type: "GET"
                    }
                },
                schema: {
                    data: "objetos",
                    model: {
                        id: "id",
                        fields: {
                            id: { editable: false, type: "string" },
                            descripcion: { editable: false, type: "string" }
                        }
                    }
                },
                //pageSize: 50,
                requestStart: function () {
                    $.blockUI({
                        message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                    });
                },
                requestEnd: function () {
                    $.unblockUI();
                },
                error: function (e) {
                    $.unblockUI();
                }
            },
            filterable: true,
            filterable: {
                mode: "row"
            },
            sortable: true,
            pageable: false,
            groupable: false,
            scrollable: true,
            navigatable: true,
            persistSelection: true,
            editable: false,
            columns: [
                {
                    field: "id",
                    title: "Código",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: right;"
                    },
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    width: "80px"
                },
                {
                    field: "descripcion",
                    title: "Descripción",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    width: "250px"
                }
            ]
        });
        $("#grdLista").on("dblclick", " tbody > tr", function () {
                var grid = $("#grdLista").data("kendoGrid");
                var data = grid.dataItem($(this));
                var Id = data.id;
                var Descripcion = data.descripcion;
                var Transaccion = $("#hfldLista").val();
                var TipoAccion = $("#hfldTipoAccion").val();
                
                switch(Transaccion){
                    case "FACULTAD":
                        if(TipoAccion == "2"){
                            $("#txtFacultadMantId").val(Id);
                            $("#txtFacultadMant").val(Descripcion);
                        }
                        else{
                            $("#txtFacultadId").val(Id);
                            $("#txtFacultad").val(Descripcion);
                        }
                        break;
                    case "FACULTAD ESPECIFICO":
                        if(TipoAccion == "2"){
                            $("#txtFacultadMantId").val(Id);
                            $("#txtFacultadMant").val(Descripcion);
                        }
                        else{
                            $("#txtFacultadId").val(Id);
                            $("#txtFacultad").val(Descripcion);
                        }
                        break;
                    case "TIPO_ARTICULO":
                        $("#txtTipoId").val(Id);
                        $("#txtTipo").val(Descripcion);
                        break;
                    case "AMBIENTE_EQUIPO":
                        $("#txtAmbienteId").val(Id);
                        $("#txtAmbiente").val(Descripcion);
                        break;
                    case "MARCA":
                        $("#txtMarcaId").val(Id);
                        $("#txtMarca").val(Descripcion);
                        break;
                    case "ESTADO":
                        if(TipoAccion == "2"){
                            $("#txtEstadoId").val(Id);
                            $("#txtEstado").val(Descripcion);
                        }
                        else{
                            $("#txtEstadoIniId").val(Id);
                            $("#txtEstadoIni").val(Descripcion);
                        }            
                    break;
                }

                $('#diag-lista').modal('hide');            
        });
        
	});
</script>