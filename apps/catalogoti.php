    
<style>
    .k-grid tbody .k-button {
        min-width: 25px;
        width: 25px;
        text-align: center;
        margin-left: 2px;
        padding-left: 2px;
    }
    .k-grid-content {
        max-height: 400px;
    }
</style>
<form id="frmCatalogoTIC" method="post" enctype="multipart/form-data">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-7 subheader-transparent" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Details-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Catálogo Inventario TIC</h5>
                <!--end::Title-->
                <!--begin::Separator-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <!--end::Separator-->
            </div>
            <!--end::Details-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Nuevo-->
                <a id="btnNuevoCatalogo" href="#" class="btn btn-primary font-weight-bolder btn-sm px-4 font-size-base ml-2" style="display:none;">
                    <span class="svg-icon svg-icon-md fas fa-file-invoice"></span>&nbsp;Nuevo
                </a>
                <!--end::Nuevo-->
                <!--begin::Consultar-->
                <a id="btnBuscarCatalogo" href="#" class="btn btn-primary font-weight-bolder btn-sm px-3 font-size-base ml-2">
                    <span class="svg-icon svg-icon-md fas fa-search"></span>&nbsp;Buscar
                </a>
                <!--end::Consultar-->
                <!--begin::Exportar Excel-->
                <a id="btnExportarExcelCatalogo" href="#" class="btn btn-primary font-weight-bolder btn-sm px-3 font-size-base ml-2"style="display:none;">
                    <span class="svg-icon svg-icon-md fas fa-file-excel"></span>&nbsp;Exportar Excel
                </a>
                <!--end::Exportar Excel-->
                
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <div id="divConsultas">        
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container-fluid">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label text-left">
                                Criterio de búsquedas
                                <span class="d-block text-muted pt-2 font-size-sm">Seleccione los criterios de búsquedas a continuación y presione el botón Buscar para mostrar la información</span>
                            </h3>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-sm-2 col-xs-12">
                                <label for="txtTipoBId" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Tipo</label>
                                <input id="txtTipoBId" type="text" class="form-control input-sm" placeholder="Código" readonly />
                            </div>
                            <div class="form-group col-sm-4 col-xs-12">
                                <label for="txtTipoB" class="font-weight-bold">&nbsp;&nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <input id="txtTipoB" type="text" class="form-control input-sm" placeholder="Tipo" readonly />
                                    <button id="btnBuscarTipoB" class="btn btn-success btnInicial" data-accion="TIPO_CATALOGO" data-tipo="1"><i class="fas fa-search"></i></button>
                                    <button id="btnBorrarTipoB" class="btn btn-danger btnInicialLimpieza" data-accion="TIPO_CATALOGO" data-tipo="1"><i class="fas fa-eraser"></i></button>
                                </div>                                
                            </div>
                            <div class="form-group col-sm-3 col-xs-12">
                                <label for="cmbEstado" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Estado</label>
                                <select id="cmbEstado" name="cmbEstado" class="form-control input-sm">
                                    <option value="-1">TODOS</option>
                                    <option value="1">ACTIVO</option>
                                    <option value="0">INACTIVO</option>
                                </select>
                            </div>
                        </div>           
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <div id="grdCatalogo"></div>
                    </div>
                </div>   
            </div>
        </div>  
    </div>    
    <input id="hfldLista" type="hidden" />
    <input id="hfldTipoAccion" type="hidden" />
    <input id="hfldTipoCatalogoId" type="hidden" />
    <input id="hfldCatalogoId" type="hidden" />
    <input id="hfldEstadoReferencia" type="hidden" />
</form>
<div class="modal fade" id="diag-lista" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Consulta de datos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
            <div class="form-group col-sm-12 col-xs-12">
                <div id="grdLista"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="diag-tipocatalogo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 id="hTituloTipo" class="modal-title" id="exampleModalLongTitle">Ingreso Tipo Catálogo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
            <div class="form-group col-sm-12 col-xs-12">
                <label for="txtNombre" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Nombre</label>
                <input id="txtNombre" type="text" class="form-control input-sm" placeholder="Ingrese el nombre del Tipo de Catálogo" />
            </div>            
        </div>
        <div class="form-row">
            <div class="form-group col-sm-12 col-xs-12">
                <label for="txtBusquedaRapida" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Búsqueda Rápida</label>
                <input id="txtBusquedaRapida" type="text" class="form-control input-sm" placeholder="Ingrese la palabra clave para búsquedas" />
            </div>            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id="btnGrabarTipoCatalogo" type="button" class="btn btn-primary">Grabar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="diag-catalogo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 id="hTituloCatalogo" class="modal-title" id="exampleModalLongTitle">Ingreso Tipo Catálogo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
            <div class="form-group col-sm-12 col-xs-12">
                <label for="txtValor1" class="font-weight-bold"><i class="fas fa-asterisk" style="color:red;"></i>&nbsp;Nombre</label>
                <input id="txtValor1" type="text" class="form-control input-sm" placeholder="Ingrese el nombre del Catálogo" />
            </div>            
        </div>
        <div class="form-row">
            <div class="form-group col-sm-12 col-xs-12">
                <label for="txtValor2" class="font-weight-bold">&nbsp;Valor Adicional</label>
                <input id="txtValor2" type="text" class="form-control input-sm" placeholder="Ingrese el valor adicional del Catálogo" />
            </div>            
        </div>
      </div>
      <div class="modal-footer">
        <button id="btnGrabarCatalogo" type="button" class="btn btn-primary">Grabar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>        
      </div>
    </div>
  </div>
</div>

<script src="assets/js/inventarioTic.js?v=3"></script>
<script>
    function CargarDataGrid() {
        $("#grdCatalogo").data('kendoGrid').dataSource.data([]);
        $("#grdCatalogo").data("kendoGrid").refresh();
        $('#grdCatalogo').data('kendoGrid').dataSource.read();
        $("#grdCatalogo").data("kendoGrid").refresh();
    }
    function CargarLista() {
        $("#grdLista").data('kendoGrid').dataSource.data([]);
        $("#grdLista").data("kendoGrid").refresh();
        $('#grdLista').data('kendoGrid').dataSource.read();
        $("#grdLista").data("kendoGrid").refresh();
    }
    function iconoEstadoRegistro(id, estado) {
        var icono = " ";

        if (estado == '1') {
            icono = '<button class="btn btn-primary btn-sm" onclick=activacionesEstadoGeneral("' + id + '","' + estado + '")><span class="fas fa-check"></span></button>';
        }
        else {
            icono = '<button class="btn btn-danger btn-sm" onclick=activacionesEstadoGeneral("' + id + '","' + estado + '")><span class="fas fa-times"></span></button>';
        }
        return icono;
    }
    function iconoAsociarRegistro(id, estado, nombre, busquedaRapida) {
        var icono = " ";

        if (estado == '1') {
            icono = '<button class="btn btn-primary  btn-sm css-asociar-usuario" data-id="' + id + '" data-ref="' + nombre.trim() + '" data-desc="' + busquedaRapida.trim() + '"><span class="fas fa-cubes"></span></button>';
        }
        return icono;
    }        
    function iconoEstadoParametro(id, estado) {
        var icono = " ";
        var estadoGeneral = $("#hfldEstadoReferencia").val();

        if (estadoGeneral == '1') {
            if (estado == '1') {
                icono = '<button class="btn btn-primary btn-sm" onclick=activacionesEstadoParametro(event,"' + id + '","0")><span class="fas fa-check"></span></button>';
            }
            else {
                icono = '<button class="btn btn-danger btn-sm" onclick=activacionesEstadoParametro(event,"' + id + '","1")><span class="fas fa-times"></span></button>';
            }
        }
        return icono;
    }
    function asociarParametro(id,parametro,descripcion) {
        $("#hfldTipoCatalogoId").val(id);
        $("#hfldCatalogoId").val("0");
        $("#hTituloCatalogo").text("INGRESAR NUEVO CATÁLOGO DE TIPO: " + parametro);
        $("#txtValor1").val("");
        $("#txtValor2").val("");
        $("#diag-catalogo").modal("show");
    }
    function detailInit(e) {
        $("#hfldEstadoReferencia").val(e.data.estadoId);
        $("<div/>").appendTo(e.detailCell).kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: 'src/inventario/consumirCatalogo.php',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: {
                            transaccion: "CONSULTA_GENERAL",
                            tipoAccion: "1",
                            tipoId:  e.data.id,
                            estadoId:  e.data.estadoId
                        },
                        type: "GET"
                    }
                },
                pageSize: 20
            },
            scrollable: false,
            sortable: true,
            pageable: true,
            filterable: true,
            filterable: {
                mode: "row"
            },
            columns: [
                {
                    title: " ",
                    attributes: {
                        style: "text-align: center;"
                    },
                    template: "#= iconoEstadoParametro(data.id, data.estadoId)#",
                    width: "25px"
                },
                {
                    command:
                        [{
                            text: "<span class='fas fa-edit'></span>",
                            click: function(e){
                                e.preventDefault();
                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                
                                if(parseInt(dataItem.estadoId) == 1){
                                    $("#hfldTipoCatalogoId").val(dataItem.tipoId);
                                    $("#hfldCatalogoId").val(dataItem.id);
                                    $("#hTituloCatalogo").text("MODIFICAR CATÁLOGO");
                                    $("#txtValor1").val(dataItem.valor);
                                    $("#txtValor2").val(dataItem.valor2);
                                    $("#diag-catalogo").modal("show");
                                }
                                else{
                                    Swal.fire({
                                        html: "Solo se puede modificar datos si el registro esta activo",
                                        icon: "warning",
                                        allowOutsideClick: false,
                                        buttonsStyling: false,
                                        confirmButtonText: "Ok, entendido!",
                                        customClass: {
                                            confirmButton: "btn font-weight-bold btn-light-primary"
                                        }
                                    });
                                }                                
                            },
                            className: "k-grid-custom btn btn-primary btn-sm erp-editar-sub",
                            name: "EditarRegistroSub"
                        }],
                    title: " ",
                    attributes: {
                        style: "text-align: center;"
                    },
                    width: "25px"
                },
                {
                    field: "id",
                    title: "Código",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        "class": "table-cell",
                        style: "text-align: right;"
                    },
                    width: "40px",
                    filterable: {
                        cell: {
                            enabled: false,
                            operator: "eq",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    hidden:true
                },
                {
                    field: "tipoId",
                    hidden: true
                },                
                {
                    field: "orden",
                    title: "Orden",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        "class": "table-cell",
                        style: "text-align: right;"
                    },
                    width: 100,
                    hidden:true
                },
                {
                    field: "valor",
                    title: "Valor",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    width: 100
                },
                {
                    field: "valor2",
                    title: "Valor2",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    width: 100
                },
                {
                    field: "estadoId",
                    hidden: true
                }
            ],
            dataBound: onDataBound
        });
    }
    function exportChildData(datos, rowIndex) {
        var deferred = $.Deferred();
        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: 'src/inventario/consumirCatalogo.php',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: {
                        transaccion: "CONSULTA_GENERAL",
                        tipoAccion: "1",
                        tipoId:  e.data.busquedaRapida,
                        estadoId:  e.data.estadoId
                    },
                    type: "GET"
                }
            },
            pageSize: 10
        });

        detailExportPromises.push(deferred);

        var rows = [{
            cells: [
                { value: "id" },
                { value: "orden" },
                { value: "valor" },
                { value: "valor2" },
                { value: "estadoId" }
            ]
        }];

        var exporter = new kendo.ExcelExporter({
            columns: [
                {
                    field: "id",
                    title: "Código"
                },
                {
                    field: "orden",
                    title: "Orden"
                },
                {
                    field: "valor",
                    title: "Valor"
                },
                {
                    field: "valor2",
                    title: "Valor2"
                }
            ],
            dataSource: dataSource
        });

        exporter.workbook().then(function (book, data) {
            deferred.resolve({
                masterRowIndex: rowIndex,
                sheet: book.sheets[0]
            });
        });
    }
    function onDataBound(e) {
        $(".btn-primary").removeClass("k-button");
        $(".btn-danger").removeClass("k-button");
    }
    function activacionesEstadoParametro(e, id, estado) {
        e.preventDefault();

        $.ajax({
            url: "src/inventario/consumirCatalogo.php",
            type: "POST",
            data: {
                id: id,
                estadoId: estado,
                transaccion: "ESTADO CATALOGO"
            },
            dataType: "json",
            traditional: true,
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                });
            },
            success: function (result) {
                $.unblockUI();

                if (result.mensaje == "OK") {
                    CargarDataGrid();
                }
                else {
                    Swal.fire({
                        html: result.respuesta,
                        icon: "error",
                        allowOutsideClick: false,
                        buttonsStyling: false,
                        confirmButtonText: "Ok, entendido!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                    });
                }
            },
            error: function (request, status, error) {
                $.unblockUI();

                Swal.fire({
                    html: request.responseText,
                    icon: "error",
                    allowOutsideClick: false,
                    buttonsStyling: false,
                    confirmButtonText: "Ok, entendido!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                });
            }
        });

        return false;
    }

	$(document).ready(function () {
        $('#txtNombre').keypress(function (e) {
            var regex = new RegExp("^[a-zA-ZñÑ ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();
            return false;            
        });
        $('input').keyup(function () {
            if ($(this).attr('name') == 'txtEmail') {
                this.value = this.value.toLowerCase();
            }
            else {
                this.value = this.value.toUpperCase();
            }                
        });
        $(".btnInicial").click(function (e){
            var transaccion = $(this).attr('data-accion');
            var tipo = $(this).attr('data-tipo');
            $("#hfldLista").val(transaccion);
            $("#hfldTipoAccion").val(tipo);
            CargarLista();
            $("#diag-lista").modal("show");
            return false;
        });
        $(".btnInicialLimpieza").click(function (e){
            e.preventDefault();
            var transaccion = $(this).attr('data-accion');
            var tipo = $(this).attr('data-tipo');
            
            switch(tipo){
                case "1":
                    $("#txtTipoBId").val("");
                    $("#txtTipoB").val("");
                    break;
                case "2":
                    $("#txtTipoId").val("");
                    $("#txtTipo").val("");                
                    break;
            }

            return false;
        });

        $(".btnBusquedas").click(function (e){
            var transaccion = $(this).attr('data-accion');
            $("#hfldLista").val(transaccion);
            $("#hfldTipoAccion").val("2");
            CargarLista();
            $("#diag-lista").modal("show");
            return false;
        });
        $(".btnLimpieza").click(function (e){
            e.preventDefault();
            var transaccion = $(this).attr('data-accion');
            
            switch(transaccion){
                case "TIPO_CATALOGO":
                    $("#txtTipoId").val("");
                    $("#txtTipo").val("");
                    break;
            }

            return false;
        });
        $("#btnNuevoCatalogo").click(function (e){
            e.preventDefault();
            $("#hfldTipoCatalogoId").val("0");
            $("#txtNombre").val("");
            $("#txtBusquedaRapida").val("");
            $("#hTituloTipo").html("INGRESAR NUEVO TIPO DE CATÁLOGO");
            $("#diag-tipocatalogo").modal("show");
        });
        $("#btnBuscarCatalogo").click(function (){
            if($("#txtTipoBId").val().length > 0){
                CargarDataGrid();
            }
            else{
                Swal.fire({
                    html: "Debe escoger un tipo de catálogo para realizar la búsqueda",
                    icon: "info",
                    allowOutsideClick: false,
                    buttonsStyling: false,
                    confirmButtonText: "Ok, entendido!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                });
            }            
        });
        $("#btnExportarExcelCatalogo").click(function (){
            var grid = $("#grdCatalogo").data("kendoGrid");
            
            grid.bind("excelExport", function (e) {
                e.preventDefault();
                    
                var workbook = e.workbook;
                detailExportPromises = [];
                var masterData = e.data;

                for (var rowIndex = 0; rowIndex < masterData.length; rowIndex++) {
                    exportChildData(masterData[rowIndex], rowIndex);
                }

                $.when.apply(null, detailExportPromises).then(function () {
                    var detailExports = $.makeArray(arguments);

                    detailExports.sort(function (a, b) {
                        return a.masterRowIndex - b.masterRowIndex;
                    });

                    workbook.sheets[0].columns.unshift({
                        width: 30
                    });

                    for (var i = 0; i < workbook.sheets[0].rows.length; i++) {
                        workbook.sheets[0].rows[i].cells.unshift({});
                    }

                    for (var i = detailExports.length - 1; i >= 0; i--) {
                        var masterRowIndex = detailExports[i].masterRowIndex + 1;
                        var sheet = detailExports[i].sheet;

                        for (var ci = 0; ci < sheet.rows.length; ci++) {
                            if (sheet.rows[ci].cells[0].value) {
                                sheet.rows[ci].cells.unshift({});
                            }
                        }

                        [].splice.apply(workbook.sheets[0].rows, [masterRowIndex + 1, 0].concat(sheet.rows));
                    }

                    kendo.saveAs({
                        dataURI: new kendo.ooxml.Workbook(workbook).toDataURL(),
                        fileName: "Catalogo.xlsx"
                    });
                });
            });
            grid.saveAsExcel();
        });
        $("#btnGrabarCatalogo").click(function (e){
            e.preventDefault();

            /*$("#btnGrabarCatalogo").prop("disabled",true);
            $("#btnGrabarCatalogo").hide();*/

            var id = $("#hfldCatalogoId").val();
            var flagValidacion = 0;
            var mensaje = "";
            var transaccion = "";

            if(parseInt(id) <= 0){
                transaccion = "INGRESAR CATALOGO";
            }
            else{
                transaccion = "MODIFICAR CATALOGO";
            }

            if($("#txtValor1").val().length <= 0){
                mensaje += "Debe ingresar Nombre";
                flagValidacion = 1;
            }

            if(parseInt(flagValidacion) == 0){
                $.ajax({
                    type: "POST",
                    url: "src/inventario/consumirCatalogo.php",
                    dataType: "json",
                    async: false,
                    data: {
                        id:  function () { return $("#hfldCatalogoId").val(); },
                        tipoId:  function () { return $("#hfldTipoCatalogoId").val(); },
                        valor:  function () { return $("#txtValor1").val(); },
                        valor2:  function () { return $("#txtValor2").val(); },
                        estadoId:  "1",
                        transaccion: transaccion
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                        });
                    },
                    success: function (result) {
                        $.unblockUI();   

                        $("#btnGrabarCatalogo").prop("disabled",false);
                        $("#btnGrabarCatalogo").show();

                        switch (result.mensaje) {
                            case "OK":
                                Swal.fire({
                                    html: result.respuesta,
                                    icon: "success",
                                    allowOutsideClick: false,
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, entendido!",
                                    customClass: {
                                        confirmButton: "btn font-weight-bold btn-light-primary"
                                    }
                                });
                                CargarDataGrid();
                                $("#diag-catalogo").modal("hide");
                                break;
                            default:
                                Swal.fire({
                                    html: result.respuesta,
                                    icon: "error",
                                    allowOutsideClick: false,
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, entendido!",
                                    customClass: {
                                        confirmButton: "btn font-weight-bold btn-light-primary"
                                    }
                                });
                                break;
                        }
                    },
                    error: function (request, status, error) {
                        $.unblockUI();

                        Swal.fire({
                            html: error,
                            icon: "error",
                            allowOutsideClick: false,
                            buttonsStyling: false,
                            confirmButtonText: "Ok, entendido!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        });
                    }
                });
            }
            else{
                Swal.fire({
                    html: mensaje,
                    icon: "warning",
                    allowOutsideClick: false,
                    buttonsStyling: false,
                    confirmButtonText: "Ok, entendido!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                });
            }
        })
        //=======================================================================
        //Configuracion de grid Lista
        //=======================================================================
        $("#grdLista").kendoGrid({
            autoBind: false,
            dataSource: {
                transport: {
                    read: {
                        url: 'src/inventario/consumirInventario.php',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: {
                            transaccion: function () { return $("#hfldLista").val(); },
                            tipoAccion: function () { return $("#hfldTipoAccion").val(); }
                        },
                        type: "GET"
                    }
                },
                schema: {
                    data: "objetos",
                    model: {
                        id: "id",
                        fields: {
                            id: { editable: false, type: "string" },
                            descripcion: { editable: false, type: "string" }
                        }
                    }
                },
                requestStart: function () {
                    $.blockUI({
                        message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                    });
                },
                requestEnd: function () {
                    $.unblockUI();
                },
                error: function (e) {
                    $.unblockUI();
                }
            },
            filterable: true,
            filterable: {
                mode: "row"
            },
            sortable: true,
            pageable: false,
            groupable: false,
            scrollable: true,
            navigatable: true,
            persistSelection: true,
            editable: false,
            columns: [
                {
                    field: "id",
                    title: "Código",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: right;"
                    },
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    width: "80px"
                },
                {
                    field: "descripcion",
                    title: "Descripción",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    width: "250px"
                }
            ]
        });
        $("#grdLista").on("dblclick", " tbody > tr", function () {
                var grid = $("#grdLista").data("kendoGrid");
                var data = grid.dataItem($(this));
                var Id = data.id;
                var Descripcion = data.descripcion;
                var Transaccion = $("#hfldLista").val();
                var TipoAccion = $("#hfldTipoAccion").val();
                
                switch(Transaccion){
                    case "TIPO_CATALOGO":
                        if(TipoAccion == "2"){
                            $("#txtTipoId").val(Id);
                            $("#txtTipo").val(Descripcion);
                        }
                        else{
                            $("#txtTipoBId").val(Id);
                            $("#txtTipoB").val(Descripcion);
                        }
                        break;
                }

                $('#diag-lista').modal('hide');            
        });
        //=======================================================================
        //Configuracion del grid Datos
        //=======================================================================
        $("#grdCatalogo").kendoGrid({
            autoBind: false,
            dataSource: {
                transport: {
                    read: {
                        url: 'src/inventario/consumirCatalogo.php',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: {
                            transaccion: "CONSULTA_TIPO_CATALOGO",
                            tipoAccion: "1",
                            tipoId:  function () { return $("#txtTipoBId").val(); },
                            estadoId:  function () { return $("#cmbEstado").val(); }
                        },
                        type: "GET"
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { editable: false, type: "number" },
                            nombre: { editable: false, type: "string" },
                            busquedaRapida: { editable: false, type: "string" },
                            estadoId: { editable: false, type: "string" },
                            susuarioRegistro: { editable: false, type: "string" },
                            usuarioEdicion: { editable: false, type: "string" },
                            fechaRegistro: { editable: false, type: "string" },
                            fechaEdicion: { editable: false, type: "string" }
                        }
                    }
                },
                pageSize: 30,
                requestStart: function () {
                    $.blockUI({
                        message: '<div style="display: inline-block;  margin: 10px; padding: 10px;"><span class="fas fa-cog fa-spin fa-3x" style="vertical-align: middle;"></span>&nbsp;&nbsp;<span id="spMensajeCargando">Procesando, un momento por favor...</span></div>'
                    });
                },
                requestEnd: function () {
                    $.unblockUI();
                },
                error: function (e) {
                    $.unblockUI();
                }
            },
            excel: {
                allPages: true,
                filterable: true
            },
            filterable: true,
            filterable: {
                mode: "row"
            },
            sortable: true,
            pageable: true,
            groupable: false,
            persistSelection: true,
            detailInit: detailInit,
            columns: [
                /*{
                    title: " ",
                    attributes: {
                        style: "text-align: center;"
                    },
                    template: "#= iconoEstadoRegistro(data.id, data.estadoId)#",
                    width: "25px"
                },
                {
                    command:
                    [{
                        text: "<span class='fas fa-edit'></span>",
                        click: function (e) {
                            e.preventDefault();

                            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                            $("#hfldTipoCatalogoId").val(dataItem.id);
                            $("#txtNombre").val(dataItem.nombre);
                            $("#txtBusquedaRapida").val(dataItem.busquedaRapida);
                            $("#hTituloTipo").html("MODIFICAR CATALOGO");
                            $("#diag-tipocatalogo").modal("show");            
                        },
                        className: "k-grid-custom btn btn-primary btn-sm erp-editar",
                        name: "EditarRegistro"
                    }],
                    title: "",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: middle; white-space: normal !important; font-weight: bold;"
                    },
                    attributes: {
                        style: "text-align: center;"
                    },
                    width: "25px"
                },*/
                {
                    title: " ",
                    attributes: {
                        style: "text-align: center;"
                    },
                    template: "#= iconoAsociarRegistro(data.id, data.estadoId, data.nombre, data.busquedaRapida)#",
                    width: "25px"
                },
                {
                    field: "id",
                    title: "Código",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: right;"
                    },
                    width: "40px",
                    filterable: {
                        cell: {
                            enabled: false,
                            operator: "eq",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    hidden:true
                },
                {
                    field: "nombre",
                    title: "Tipo Catálogo",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    width: "200px",
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    }
                },
                {
                    field: "busquedaRapida",
                    title: "Búsqueda Rápida",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    width: "150px",
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    }
                },
                {
                    field: "estadoId",
                    hidden: true
                },
                {
                    field: "usuarioRegistro",
                    title: "Usuario Registro",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    width: "160px",
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    hidden: true
                },
                {
                    field: "usuarioEdicion",
                    title: "Usuario Edición",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    width: "160px",
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    hidden: true
                },
                {
                    field: "fechaRegistro",
                    title: "Fecha Registro",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    width: "160px",
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    hidden: true
                },
                {
                    field: "fechaEdicion",
                    title: "Fecha Edición",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; white-space: nowrap; vertical-align: text-top; white-space: normal !important;"
                    },
                    attributes: {
                        style: "text-align: left;"
                    },
                    width: "160px",
                    filterable: {
                        cell: {
                            enabled: true,
                            operator: "contains",
                            showOperators: false,
                            delay: 200
                        }
                    },
                    hidden: true
                }
            ],
            dataBound: onDataBound
        });
        $("#grdCatalogo").on("click", ".css-asociar-usuario", function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            var referencia = $(this).data("ref");
            var descripcion = $(this).data("desc");
            asociarParametro(id, referencia, descripcion);
        });
        $("#grdCatalogo").kendoTooltip({
            filter: ".erp-editar",
            content: function (e) {
                "Editar";
            }
        });
	});
	</script>