<script>
    $(document).ready(function() {
        $.ajax({
            url:"src/fichaEstudiante/SensarSesion.php",
            method:"GET",
            cache:false,
            contentType:false,
            processData:false,
            dataType: "json",
            beforeSend:function () {
                $.blockUI({
                    message: '<br /><img src="img/loader.gif" width="40" height="40" />&nbsp;&nbsp;Buscando usuario...<br /><br />'
                });
            },
            success:function(respuesta){
                $.unblockUI();
                SetApp('apps/principalFichaEstudiante.php');
            }
        });
    });
</script>
