$(document).ajaxStop($.unblockUI);

$('#CI').on('keydown keypress',function(e){
    
    if(e.keyCode == 13) {
        ConsultarJefeArea();
        return false;
    }
    
    if(e.key.length === 1){
        if($(this).val().length < 10 && !isNaN(parseFloat(e.key))){
            $(this).val($(this).val() + e.key);
        }
       
        return false;
    }
});

$(document).ready(function() {
    $('#tablas').DataTable({
      autoWidth: false,
      language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true,
        "iDisplayLength": -1,
        "paging": false,
    });
});

function DatosTablaReporte(IdTabla){
    $(IdTabla).DataTable({
        autoWidth: false,
        "iDisplayLength": -1,
        //"aaSorting": [[ 3, "asc" ]],
        pageLength : 10,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        language: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            responsive: true,
            dom: 'Bfrtilp'
    });
};

function ConsultarJefeArea(){
    $("#tablas_wrapper").remove();
    $(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');

    var CI = document.getElementById('CI').value;
    if(CI)
    {   var datos = new FormData();
        datos.append("Cedula", CI);
        $.ajax({
            url:"ajax/utils.ajax.php",
            method:"POST",
            data:datos,
            cache:false,
            contentType:false,
            processData:false,
            dataType: "json",
                beforeSend:function () {
                    $.blockUI({
                        message: '<br /><img src="img/loader.gif" width="40" height="40" />&nbsp;&nbsp;Buscando usuario...<br /><br />'
                    });
                },
                success:function(respuesta){
                    $.unblockUI();
                    if(respuesta){
                        $(".card-body").prepend(respuesta);
                        DatosTablaReporte("#tablas");
                        $(document).ready(function(){
                            $(".overlay").remove();
                        });
                    }
                    else{
                        Swal.fire({
                            icon: "error",
                            title: "¡No se encuentra en el Padrón!",
                            text: "¡Ingrese una nueva cédula, por favor!",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                    }
                }
            });
    }
    else{
        Swal.fire({
            icon: "error",
            title: "¡Cédula no ingresada!",
            text: "¡Ingrese una cédula, por favor!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
}