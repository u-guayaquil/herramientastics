<?php
    require_once "conexion.php";
    class ModeloIndex{
        static public function mdlObtenerUsuario($Cedula){
            $QUERY = "SELECT DISTINCT CONTRATO AS TIPO,NOMBRE,MESA,TXFAC,SEXO,
                                    (CASE WHEN NIVEL <3
                                        THEN '[NIVEL 1 Y 2]'
                                        ELSE 'DESDE 3ER NIVEL'
                                        END) AS PADRON_ELECTORAL
                        FROM PADRON WHERE PADRON=461 AND CEDULA=:Ci1
                        UNION
                        SELECT DISTINCT 'ADMINISTRATIVO' AS TIPO,NOMBRE,MESA,TXFAC,SEXO,
                                    (CASE ADCONTRATO WHEN 'P'
                                        THEN 'CONTRATADOS'
                                        ELSE 'TITULARES'
                                        END) AS PADRON_ELECTORAL
                        FROM PADRON WHERE PADRON=463 AND CEDULA=:Ci2
                        UNION
                        SELECT DISTINCT 'DOCENTE' AS TIPO,NOMBRE,MESA,TXFAC,SEXO,
                                    (CASE ADCONTRATO WHEN 'P'
                                        THEN 'CONTRATADOS'
                                        ELSE 'TITULARES'
                                            END) AS PADRON_ELECTORAL
                        FROM PADRON WHERE PADRON=465 AND CEDULA=:Ci3";
            $stmt = Conexion::conectar()->prepare($QUERY);
            $stmt -> bindParam(":Ci1", $Cedula, PDO::PARAM_STR);
            $stmt -> bindParam(":Ci2", $Cedula, PDO::PARAM_STR);
            $stmt -> bindParam(":Ci3", $Cedula, PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt ->fetch();
        }
    }
?>