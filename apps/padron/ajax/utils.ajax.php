<?php
require_once "../controllers/index_controlador.php";
require_once "../models/index_modelo.php";
class AjaxUtils{
    public $Cedula;
    public function ajaxPintarTablaUsuarios(){
        $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                  <thead>
                    <tr>
                        <th class="hidden-sm hidden-md hidden-lg hidden-xl"><i class="fas fa-arrow-down"></i></th>
                        <th>NOMBRE</th>
                        <th>TIPO</th>
                        <th>INFORMACION</th>
                    </tr> </thead> <tbody>';
        $Cedula = $this->Cedula;
        $Servidores = ControladorIndex::ConsultarUsuario($Cedula);
        if($Servidores == 0)
        {
            echo 0;
        }
        else{
            $HTML.= '<tr>';
            $HTML.= '   <td class="hidden-sm hidden-md hidden-lg hidden-xl"><i class="fas fa-arrow-down"></i></td>';
            $HTML.= '   <td>'.$Servidores['NOMBRE'].'</td>';
            $HTML.= '   <td>'.$Servidores['TIPO'].'</td>';
            if($Servidores['SEXO'] == 'M'){
                $HTML.= '<td><b>• Mesa:</b> '       .$Servidores['MESA'].' HOMBRES'.'<br>
                            <b> • Ubicación:</b> '  .$Servidores['TXFAC'].'<br>
                            <b> • Padrón:</b> '     .$Servidores['PADRON_ELECTORAL'].'<br>
                        </td>';
            }
            else{
                $HTML.= '<td><b>• Mesa:</b> '       .$Servidores['MESA'].' HOMBRES'.'<br>
                            <b> • Ubicación:</b> '  .$Servidores['TXFAC'].'<br>
                            <b> • Padrón:</b> '     .$Servidores['PADRON_ELECTORAL'].'<br>
                        </td>';
            }          
            $HTML.= '</tr>';
            $HTML.= '</tbody> </table>';
            echo json_encode($HTML);
        }
      }
}
/*====================================
    PINTAR TABLA DE USUARIOS BY JEFE
 ====================================*/
if ($_POST["Cedula"]){
    $Servidores = new AjaxUtils();
    $Servidores -> Cedula = $_POST["Cedula"];
    $Servidores -> ajaxPintarTablaUsuarios();
}