<?php
    include("models/conexion.php");
    include("controllers/index_controlador.php");
    include("models/index_modelo.php");
    date_default_timezone_set('America/Guayaquil');
?>

<!DOCTYPE html>
<meta charset="UTF-8">
<html>
	<head>
    <!-- Metas -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CONSULTA DE PADRON ELECTORAL UG - 2021</title>
    <link  rel="icon"   href="img/favicon.png" type="image/png" />
    <!-- Bootstrap core CSS - JS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- CSS Personalizable -->
    <link href="css/style.css" rel="stylesheet">
    <?php require_once('utils/css.php'); ?>
	</head>
<body>
    <div class="col-md-8 col-md-offset-2" style="margin-top:10px;margin-bottom:10px;border-style:solid;">
        <div class="row border-buttom-solid">
            <div class="col-md-12 center-items" style="margin-top:6px;">
                <a href="index.php" class="title" style="text-decoration:none; ">
                    <h3 class="center"><b>CONSULTA DE PADRON ELECTORAL UG - 2021</b></h3>
                </a>
            </div>
        </div>
        <br>
        <div class="form-group row border-buttom-solid">
            <div class="col-md-12">
                <div class="row top center-items">
                    <div class="col-md-2"><label>CÉDULA: </label></div>
                    <div class="col-md-3"><input class="form-control input-xs" type="text" id="CI" name="CI" onblur="return ConsultarJefeArea();"></div>
                    <input class="form-control input-xs" type="hidden" id="IdJefe" name="IdJefe">
                    <div class="col-md-3"><input class="input-ls" type="button" id="buscar" name="buscar" value="BUSCAR" onclick="return ConsultarJefeArea();"></div>
                </div>
                <br>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-10 col-md-offset-1">
                <div class="card-body">
                </div>
            </div>
        </div>
    </div>

    <!-- All JS -->
    <?php require_once('utils/js.php');  ?>
    <!-- JS Personalizable -->
    <script src="js/config.js"></script>
</body>
</html>