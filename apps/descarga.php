<div class="col-md-12 col-md-offset-1 center-items">
    <div class="col-md-10 text-center" style="margin: 50px;">
        <h1 class="center text-dark font-weight-bold">DESCARGAS TICS - UG</h1>
        <form id="frmDescarga" method="POST" autocomplete="nope" style="margin-top:30px;">
            <div class="form-row center">
                
                    <div class="form-group col-sm-3 col-xs-12">
                        <a href="uploads/INTRUCTIVO_DE_SOLUCIÓN_AL_ERROR_1142_EN_ZOOM.pdf" target="_blank">
                            <div class="card text-center" style="width: 18rem;">
                                <div class="card-header center">
                                    <span class="svg-icon svg-icon-primary svg-icon-6x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo2/dist/../src/media/svg/icons/Communication/Chat2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <polygon fill="#000000" opacity="0.3" points="5 15 3 21.5 9.5 19.5"/>
                                            <path d="M13.5,21 C8.25329488,21 4,16.7467051 4,11.5 C4,6.25329488 8.25329488,2 13.5,2 C18.7467051,2 23,6.25329488 23,11.5 C23,16.7467051 18.7467051,21 13.5,21 Z M9,8 C8.44771525,8 8,8.44771525 8,9 C8,9.55228475 8.44771525,10 9,10 L18,10 C18.5522847,10 19,9.55228475 19,9 C19,8.44771525 18.5522847,8 18,8 L9,8 Z M9,12 C8.44771525,12 8,12.4477153 8,13 C8,13.5522847 8.44771525,14 9,14 L14,14 C14.5522847,14 15,13.5522847 15,13 C15,12.4477153 14.5522847,12 14,12 L9,12 Z" fill="#000000"/>
                                        </g>
                                    </svg><!--end::Svg Icon--></span><br /><br />
                                </div>                    
                                <div class="card-body center">
                                    <h5 class="card-title">Instructivo de solución del error "1142" en zoom</h5>
                                    <a href="uploads/INTRUCTIVO_DE_SOLUCIÓN_AL_ERROR_1142_EN_ZOOM.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="form-group col-sm-3 col-xs-12">
                        <a href="http://www.ug.edu.ec/logos_bibliotecas_virtuales/Instructivo_Proxy_UG_V1.pdf" target="_blank">
                            <div class="card text-center" style="width: 18rem;">
                                <div class="card-header center">
                                    <span class="svg-icon svg-icon-primary svg-icon-6x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo2/dist/../src/media/svg/icons/Code/Git3.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M7,11 L15,11 C16.1045695,11 17,10.1045695 17,9 L17,8 L19,8 L19,9 C19,11.209139 17.209139,13 15,13 L7,13 L7,15 C7,15.5522847 6.55228475,16 6,16 C5.44771525,16 5,15.5522847 5,15 L5,9 C5,8.44771525 5.44771525,8 6,8 C6.55228475,8 7,8.44771525 7,9 L7,11 Z" fill="#000000" opacity="0.3"/>
                                                <path d="M6,21 C7.1045695,21 8,20.1045695 8,19 C8,17.8954305 7.1045695,17 6,17 C4.8954305,17 4,17.8954305 4,19 C4,20.1045695 4.8954305,21 6,21 Z M6,23 C3.790861,23 2,21.209139 2,19 C2,16.790861 3.790861,15 6,15 C8.209139,15 10,16.790861 10,19 C10,21.209139 8.209139,23 6,23 Z" fill="#000000" fill-rule="nonzero"/>
                                                <path d="M18,7 C19.1045695,7 20,6.1045695 20,5 C20,3.8954305 19.1045695,3 18,3 C16.8954305,3 16,3.8954305 16,5 C16,6.1045695 16.8954305,7 18,7 Z M18,9 C15.790861,9 14,7.209139 14,5 C14,2.790861 15.790861,1 18,1 C20.209139,1 22,2.790861 22,5 C22,7.209139 20.209139,9 18,9 Z" fill="#000000" fill-rule="nonzero"/>
                                                <path d="M6,7 C7.1045695,7 8,6.1045695 8,5 C8,3.8954305 7.1045695,3 6,3 C4.8954305,3 4,3.8954305 4,5 C4,6.1045695 4.8954305,7 6,7 Z M6,9 C3.790861,9 2,7.209139 2,5 C2,2.790861 3.790861,1 6,1 C8.209139,1 10,2.790861 10,5 C10,7.209139 8.209139,9 6,9 Z" fill="#000000" fill-rule="nonzero"/>
                                            </g>
                                        </svg><!--end::Svg Icon--></span><br /><br />
                                </div>
                                <div class="card-body center">
                                    <h5 class="card-title">Instructivo de Configuración Proxy-UG</h5>
                                    <a href="http://www.ug.edu.ec/logos_bibliotecas_virtuales/Instructivo_Proxy_UG_V1.pdf" target="_blank" class="btn btn-primary" style="margin-top: 5px;">Ir al sitio</a>
                                </div>
                            </div>
                        </a>
                    </div>
          
            </div>
        </form>
    </div>
</div>