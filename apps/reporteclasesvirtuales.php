<form id="frmFReporteClasesVirtuales" method="post" enctype="multipart/form-data">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-7 subheader-transparent" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Details-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Reporte Clases Virtuales</h5>
                <!--end::Title-->
                <!--begin::Separator-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <!--end::Separator-->
            </div>
            <!--end::Details-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Buscar-->
                <a id="btnBuscarRCV" href="#" class="btn btn-primary font-weight-bolder btn-sm px-4 font-size-base ml-2">
                    <span class="svg-icon svg-icon-md fas fa-search"></span>&nbsp;Buscar
                </a>
                <!--end::Buscar-->
                <!--begin::Exportar Excel-->
                <!--<a id="btnExportarExcelF" href="#" class="btn btn-primary font-weight-bolder btn-sm px-4 font-size-base ml-2">
                    <span class="svg-icon svg-icon-md fas fa-file-excel"></span>&nbsp;Exportar Excel
                </a>-->
                <!--end::Exportar Excel-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label text-left">
                            Criterio de búsquedas
                            <span class="d-block text-muted pt-2 font-size-sm">Seleccione los criterios de búsquedas a continuación y presione el botón Buscar para mostrar la información</span>
                        </h3>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-sm-4 col-xs-12">
                            <label for="CI" class="font-weight-bold">Nro. Identificación</label>
                            <input class="form-control" type="text" id="CI" name="CI" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div id="divGrid" class="col-sm-12 col-xs-12">
                            
                        </div>
                        <br />
                    </div>
                </div>
            </div>
            <!--begin::Card-->
        </div>
        <!--begin::Container-->
    </div>
    <p>&nbsp;</p>
    <!--begin::Entry-->
</form>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="assets/js/reporteclasesvirtuales.js?v=2"></script>
<script>
    $(document).ready(function (){
        $('#CI').keypress(function (e) {
            var regex = new RegExp("^[0-9]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                if($(this).val().length < 10){
                    $(this).val($(this).val() + e.key);
                }
            }
            e.preventDefault();
            return false;
        });
        $("#btnBuscarRCV").click(function (){
            ConsultarClasesVirtuales();
        });
        $('#CI').on('keydown keypress',function(e){
            if(e.keyCode == 13) {
                ConsultarClasesVirtuales();
                return false;
            }
        });
    });
</script>