
<!--begin::Header-->
<div id="kt_header" class="header header-fixed">
	<!--begin::Header Wrapper-->
	<div class="header-wrapper rounded-top-xl d-flex flex-grow-1 align-items-center">
		<!--begin::Container-->
		<div class="container-fluid d-flex align-items-center justify-content-end justify-content-lg-between flex-wrap">
			<!--begin::Toolbar-->
			<div class="d-flex align-items-center py-3 py-lg-12">
				<!--begin::Búsqueda de estudiantes-->
				<div class="dropdown mr-12" data-toggle="tooltip" title="Búsqueda de estudiantes" data-placement="left">
					<div class="form-row" style="margin-top: 25px;">
						<div class="form-group col-sm-6 col-xs-12">
							<label for="estudiante" class="font-weight-bold">Estudiante</label>
							<input id="estudiante" type="text" class="form-control form-control-primary" size = "50" placeholder="Buscar estudiante..." />
						</div>
						<div class="form-group col-sm-5 col-xs-12">
							<label for="periodo" class="font-weight-bold">Periodo</label>
							<select id="periodos" name="periodos" class="form-control form-control-primary">
								<option value="T">Último Vigente</option>
								<option value="2021 - 2022 CI">2021 - 2022 CI</option>
								<option value="2020 - 2021 CII">2020 - 2021 CII</option>
								<option value="2021 - 2022 CI">2020 - 2021 CI</option>
							</select>
						</div>
						<div class="form-group col-sm-1 col-xs-12">
							<label class="font-weight-bold">&nbsp;&nbsp;</label>
							<button class="btn btn-primary"><i class="fas fa-search icon-sm"></i></button>												
						</div>
					</div>													
				</div>
				<!--end::Búsqueda de estudiantes-->
                                    
			</div>
			<!--end::Toolbar-->
		</div>
		<!--end::Container-->
	</div>
	<!--end::Header Wrapper-->
</div>
<!--end::Header-->
<!--begin::Content-->
<div class="content d-flex flex-column" id="kt_content">
	<!--begin::Entry-->
	<div class="d-flex">
		<!--begin::Container-->
		<!--<div class="container">-->
			<!--begin::Dashboard-->
			<!--begin::Row-->
			<div class="row mt-0 mt-lg-8">
				<div class="col-xl-8">
					<!--begin::Datos Personales-->
					<!--begin::Card-->
					<div class="card card-custom">
						<!--begin::Card header-->
						<div class="card-header h-auto">
							<div class="card-title align-items-start flex-column">
								<h3 class="card-label font-weight-bolder text-dark">Datos Personales</h3>
								<span class="text-muted font-weight-bold font-size-sm mt-1">Información del estudiante registrada en el SIUG</span>
							</div>
						</div>
						<!--end::Card header-->
						<!--begin::Card body-->
						<div class="card-body">
							<div class="form-row">
								<div class="form-group col-lg-3 col-xl-3">
									<label class="font-weight-bold">Foto</label><br />
									<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url(assets/media/users/blank.png)">
										<div class="image-input-wrapper" style="background-image: url(assets/media/users/300_21.jpg)"></div>															
									</div>
								</div>													
								<div class="form-group col-lg-5 col-xl-5">
									<div class="form-group col-sm-12 col-xs-12">
										<label for="txtApellidos" class="font-weight-bold">Apellidos</label>
										<input id="txtApellidos" class="form-control form-control-lg form-control-solid" type="text" value="APELLIDOS" size="100" readonly="readonly" />
									</div>
									<div class="form-group col-sm-12 col-xs-12">
										<label for="txtNombres" class="font-weight-bold">Nombres</label>
										<input id="txtNombres" class="form-control form-control-lg form-control-solid" type="text" value="NOMBRES" size="100" readonly="readonly" />
									</div>
								</div>
								<div class="form-group col-lg-4 col-xl-4">
									<div class="form-group col-sm-12 col-xs-12">
										<label for="txtCorreoUg" class="font-weight-bold">Correo Institucional</label>
										<input id="txtCorreoUg" class="form-control form-control-lg form-control-solid" type="text" value="APELLIDOS" size="100" readonly="readonly" />
									</div>
									<div class="form-group col-sm-12 col-xs-12">
										<label for="txtCorreoPersonal" class="font-weight-bold">Correo Personal</label>
										<input id="txtCorreoPersonal" class="form-control form-control-lg form-control-solid" type="text" value="NOMBRES" size="100" readonly="readonly" />
									</div>
								</div>													
							</div>
							<div class="form-row">
								<div class="form-group col-sm-4 col-xs-12">
									<label for="txtNroIdentificacion" class="font-weight-bold">Nro. Identificación</label>
									<input id="txtNroIdentificacion" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="txtFechaNacimiento" class="font-weight-bold">Fecha Nacimiento</label>
									<input id="txtFechaNacimiento" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="txtEdad" class="font-weight-bold">Edad</label>
									<input id="txtEdad" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-sm-2 col-xs-12">
									<label for="txtGenero" class="font-weight-bold">Sexo</label>
									<input id="txtGenero" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtEstadoCivil" class="font-weight-bold">Estado Civil</label>
									<input id="txtEstadoCivil" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtEtnia" class="font-weight-bold">Étnia</label>
									<input id="txtEtnia" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="txtNroHijos" class="font-weight-bold">Nro. Hijos/Familiares que viven con Ud.</label>
									<input id="txtNroHijos" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtDiscapacidad" class="font-weight-bold">Posee discapacidad ?</label>
									<input id="txtDiscapacidad" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div id="divTipoDiscapacidad" class="form-group col-sm-6 col-xs-12">
									<label for="txtTipoDiscapacidad" class="font-weight-bold">Tipo</label>
									<input id="txtTipoDiscapacidad" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div id="divPorcentajeDiscapacidad" class="form-group col-sm-3 col-xs-12">
									<label for="txtPorcentajeDiscapacidad" class="font-weight-bold">Porcentaje</label>
									<input id="txtPorcentajeDiscapacidad" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtGrupoSanguineo" class="font-weight-bold">Grupo Sanguíneo</label>
									<input id="txtGrupoSanguineo" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtTelefono" class="font-weight-bold">Teléfono</label>
									<input id="txtTelefono" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtCelular" class="font-weight-bold">Celular</label>
									<input id="txtCelular" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-xl-12">
									<h5 class="font-weight-bold mt-10 mb-6">Lugar de Residencia</h5>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtPais" class="font-weight-bold">País</label>
									<input id="txtPais" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtProvincia" class="font-weight-bold">Provincia</label>
									<input id="txtProvincia" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtCiudad" class="font-weight-bold">Ciudad</label>
									<input id="txtCiudad" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtParroquia" class="font-weight-bold">Parroquia</label>
									<input id="txtParroquia" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-sm-5 col-xs-12">
									<label for="txtDireccion" class="font-weight-bold">Dirección Domiciliaria</label>
									<input id="txtDireccion" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="txtReferenciaDireccion" class="font-weight-bold">Referencias para llegar a su Domicilio</label>
									<input id="txtReferenciaDireccion" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
								<div class="form-group col-sm-3 col-xs-12">
									<label for="txtCodigoPostal" class="font-weight-bold">Código Postal</label>
									<input id="txtCodigoPostal" class="form-control form-control-lg form-control-solid" type="text" value="0" readonly="readonly" />
								</div>
							</div>
						</div>
						<!--end::Card body-->
					</div>
					<!--end::Card-->
					<!--end::Datos Personales-->
				</div>
				<div class="col-xl-4">
					<!--begin::Horario Clases-->
					<div class="card card-custom gutter-b">
						<!--begin::Card header-->
						<div class="card-header align-items-center border-0 mt-4">
							<h3 class="card-title align-items-start flex-column">
								<span class="font-weight-bolder text-dark">Horario de Clases</span>
								<span id="FechaHorario" class="text-muted mt-3 font-weight-bold font-size-sm">Hoy, viernes 18 de junio de 2021</span>
							</h3>								
						</div>
						<!--end::Card header-->
						<!--begin::Card body-->
						<div class="card-body pt-4">
							<!--begin::Timeline-->
							<div class="timeline timeline-6 mt-3">
								<!--begin::Item-->
								<div class="timeline-item align-items-start">
									<!--begin::Label-->
									<div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">08:42</div>
									<!--end::Label-->
									<!--begin::Badge-->
									<div class="timeline-badge">
										<i class="fa fa-genderless text-warning icon-xl"></i>
									</div>
									<!--end::Badge-->
									<!--begin::Text-->
									<div class="font-weight-mormal font-size-lg timeline-content text-muted pl-3">Outlines keep you honest. And keep structure</div>
									<!--end::Text-->
								</div>
								<!--end::Item-->
								<!--begin::Item-->
								<div class="timeline-item align-items-start">
									<!--begin::Label-->
									<div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">10:00</div>
									<!--end::Label-->
									<!--begin::Badge-->
									<div class="timeline-badge">
										<i class="fa fa-genderless text-success icon-xl"></i>
									</div>
									<!--end::Badge-->
									<!--begin::Content-->
									<div class="timeline-content d-flex">
										<span class="font-weight-bolder text-dark-75 pl-3 font-size-lg">AEOL meeting</span>
									</div>
									<!--end::Content-->
								</div>
								<!--end::Item-->
								<!--begin::Item-->
								<div class="timeline-item align-items-start">
									<!--begin::Label-->
									<div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">14:37</div>
									<!--end::Label-->
									<!--begin::Badge-->
									<div class="timeline-badge">
										<i class="fa fa-genderless text-danger icon-xl"></i>
									</div>
									<!--end::Badge-->
									<!--begin::Desc-->
									<div class="timeline-content font-weight-bolder font-size-lg text-dark-75 pl-3">Make deposit
									<a href="#" class="text-primary">USD 700</a>. to ESL</div>
									<!--end::Desc-->
								</div>
								<!--end::Item-->
								<!--begin::Item-->
								<div class="timeline-item align-items-start">
									<!--begin::Label-->
									<div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">16:50</div>
									<!--end::Label-->
									<!--begin::Badge-->
									<div class="timeline-badge">
										<i class="fa fa-genderless text-primary icon-xl"></i>
									</div>
									<!--end::Badge-->
									<!--begin::Text-->
									<div class="timeline-content font-weight-mormal font-size-lg text-muted pl-3">Indulging in poorly driving and keep structure keep great</div>
									<!--end::Text-->
								</div>
								<!--end::Item-->
								<!--begin::Item-->
								<div class="timeline-item align-items-start">
									<!--begin::Label-->
									<div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">21:03</div>
									<!--end::Label-->
									<!--begin::Badge-->
									<div class="timeline-badge">
										<i class="fa fa-genderless text-danger icon-xl"></i>
									</div>
									<!--end::Badge-->
									<!--begin::Desc-->
									<div class="timeline-content font-weight-bolder text-dark-75 pl-3 font-size-lg">New order placed
									<a href="#" class="text-primary">#XF-2356</a>.</div>
									<!--end::Desc-->
								</div>
								<!--end::Item-->
								<!--begin::Item-->
								<div class="timeline-item align-items-start">
									<!--begin::Label-->
									<div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">23:07</div>
									<!--end::Label-->
									<!--begin::Badge-->
									<div class="timeline-badge">
										<i class="fa fa-genderless text-info icon-xl"></i>
									</div>
									<!--end::Badge-->
									<!--begin::Text-->
									<div class="timeline-content font-weight-mormal font-size-lg text-muted pl-3">Outlines keep and you honest. Indulging in poorly driving</div>
									<!--end::Text-->
								</div>
								<!--end::Item-->
								<!--begin::Item-->
								<div class="timeline-item align-items-start">
									<!--begin::Label-->
									<div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">16:50</div>
									<!--end::Label-->
									<!--begin::Badge-->
									<div class="timeline-badge">
										<i class="fa fa-genderless text-primary icon-xl"></i>
									</div>
									<!--end::Badge-->
									<!--begin::Text-->
									<div class="timeline-content font-weight-mormal font-size-lg text-muted pl-3">Indulging in poorly driving and keep structure keep great</div>
									<!--end::Text-->
								</div>
								<!--end::Item-->
								<!--begin::Item-->
								<div class="timeline-item align-items-start">
									<!--begin::Label-->
									<div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">21:03</div>
									<!--end::Label-->
									<!--begin::Badge-->
									<div class="timeline-badge">
										<i class="fa fa-genderless text-danger icon-xl"></i>
									</div>
									<!--end::Badge-->
									<!--begin::Desc-->
									<div class="timeline-content font-weight-bolder font-size-lg text-dark-75 pl-3">New order placed
									<a href="#" class="text-primary">#XF-2356</a>.</div>
									<!--end::Desc-->
								</div>
								<!--end::Item-->
							</div>
							<!--end::Timeline-->
						</div>
						<!--end: Card Body-->
					</div>
					<!--end:Horario Clases-->
					<!--begin::Documentos Subidos-->
					<div class="card card-custom gutter-b">
						<!--begin::Header-->
						<div class="card-header border-0 pt-5">
							<h3 class="card-title align-items-start flex-column">
								<span class="card-label font-weight-bolder text-dark">Documentos subidos</span>
								<span class="text-muted mt-3 font-weight-bold font-size-sm">Lista de documentos subidos por el estudiante</span>
							</h3>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body pt-8">
							<!--begin::Item-->
							<div class="d-flex align-items-center mb-10">
								<!--begin::Symbol-->
								<div class="symbol symbol-40 symbol-light-primary mr-5">
									<span class="symbol-label">
										<span class="svg-icon svg-icon-xl svg-icon-primary">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Home/Library.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000" />
													<rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519)" x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
									</span>
								</div>
								<!--end::Symbol-->
								<!--begin::Text-->
								<div class="d-flex flex-column font-weight-bold">
									<a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Project Briefing</a>
									<span class="text-muted">Project Manager</span>
								</div>
								<!--end::Text-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex align-items-center mb-10">
								<!--begin::Symbol-->
								<div class="symbol symbol-40 symbol-light-warning mr-5">
									<span class="symbol-label">
										<span class="svg-icon svg-icon-lg svg-icon-warning">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Write.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)" />
													<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
									</span>
								</div>
								<!--end::Symbol-->
								<!--begin::Text-->
								<div class="d-flex flex-column font-weight-bold">
									<a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg">Concept Design</a>
									<span class="text-muted">Art Director</span>
								</div>
								<!--end::Text-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex align-items-center mb-10">
								<!--begin::Symbol-->
								<div class="symbol symbol-40 symbol-light-success mr-5">
									<span class="symbol-label">
										<span class="svg-icon svg-icon-lg svg-icon-success">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group-chat.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="#000000" />
													<path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="#000000" opacity="0.3" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
									</span>
								</div>
								<!--end::Symbol-->
								<!--begin::Text-->
								<div class="d-flex flex-column font-weight-bold">
									<a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Functional Logics</a>
									<span class="text-muted">Lead Developer</span>
								</div>
								<!--end::Text-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex align-items-center mb-10">
								<!--begin::Symbol-->
								<div class="symbol symbol-40 symbol-light-danger mr-5">
									<span class="symbol-label">
										<span class="svg-icon svg-icon-lg svg-icon-danger">
											<!--begin::Svg Icon | path:assets/media/svg/icons/General/Attachment2.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M11.7573593,15.2426407 L8.75735931,15.2426407 C8.20507456,15.2426407 7.75735931,15.6903559 7.75735931,16.2426407 C7.75735931,16.7949254 8.20507456,17.2426407 8.75735931,17.2426407 L11.7573593,17.2426407 L11.7573593,18.2426407 C11.7573593,19.3472102 10.8619288,20.2426407 9.75735931,20.2426407 L5.75735931,20.2426407 C4.65278981,20.2426407 3.75735931,19.3472102 3.75735931,18.2426407 L3.75735931,14.2426407 C3.75735931,13.1380712 4.65278981,12.2426407 5.75735931,12.2426407 L9.75735931,12.2426407 C10.8619288,12.2426407 11.7573593,13.1380712 11.7573593,14.2426407 L11.7573593,15.2426407 Z" fill="#000000" opacity="0.3" transform="translate(7.757359, 16.242641) rotate(-45.000000) translate(-7.757359, -16.242641)" />
													<path d="M12.2426407,8.75735931 L15.2426407,8.75735931 C15.7949254,8.75735931 16.2426407,8.30964406 16.2426407,7.75735931 C16.2426407,7.20507456 15.7949254,6.75735931 15.2426407,6.75735931 L12.2426407,6.75735931 L12.2426407,5.75735931 C12.2426407,4.65278981 13.1380712,3.75735931 14.2426407,3.75735931 L18.2426407,3.75735931 C19.3472102,3.75735931 20.2426407,4.65278981 20.2426407,5.75735931 L20.2426407,9.75735931 C20.2426407,10.8619288 19.3472102,11.7573593 18.2426407,11.7573593 L14.2426407,11.7573593 C13.1380712,11.7573593 12.2426407,10.8619288 12.2426407,9.75735931 L12.2426407,8.75735931 Z" fill="#000000" transform="translate(16.242641, 7.757359) rotate(-45.000000) translate(-16.242641, -7.757359)" />
													<path d="M5.89339828,3.42893219 C6.44568303,3.42893219 6.89339828,3.87664744 6.89339828,4.42893219 L6.89339828,6.42893219 C6.89339828,6.98121694 6.44568303,7.42893219 5.89339828,7.42893219 C5.34111353,7.42893219 4.89339828,6.98121694 4.89339828,6.42893219 L4.89339828,4.42893219 C4.89339828,3.87664744 5.34111353,3.42893219 5.89339828,3.42893219 Z M11.4289322,5.13603897 C11.8194565,5.52656326 11.8194565,6.15972824 11.4289322,6.55025253 L10.0147186,7.96446609 C9.62419433,8.35499039 8.99102936,8.35499039 8.60050506,7.96446609 C8.20998077,7.5739418 8.20998077,6.94077682 8.60050506,6.55025253 L10.0147186,5.13603897 C10.4052429,4.74551468 11.0384079,4.74551468 11.4289322,5.13603897 Z M0.600505063,5.13603897 C0.991029355,4.74551468 1.62419433,4.74551468 2.01471863,5.13603897 L3.42893219,6.55025253 C3.81945648,6.94077682 3.81945648,7.5739418 3.42893219,7.96446609 C3.0384079,8.35499039 2.40524292,8.35499039 2.01471863,7.96446609 L0.600505063,6.55025253 C0.209980772,6.15972824 0.209980772,5.52656326 0.600505063,5.13603897 Z" fill="#000000" opacity="0.3" transform="translate(6.014719, 5.843146) rotate(-45.000000) translate(-6.014719, -5.843146)" />
													<path d="M17.9142136,15.4497475 C18.4664983,15.4497475 18.9142136,15.8974627 18.9142136,16.4497475 L18.9142136,18.4497475 C18.9142136,19.0020322 18.4664983,19.4497475 17.9142136,19.4497475 C17.3619288,19.4497475 16.9142136,19.0020322 16.9142136,18.4497475 L16.9142136,16.4497475 C16.9142136,15.8974627 17.3619288,15.4497475 17.9142136,15.4497475 Z M23.4497475,17.1568542 C23.8402718,17.5473785 23.8402718,18.1805435 23.4497475,18.5710678 L22.0355339,19.9852814 C21.6450096,20.3758057 21.0118446,20.3758057 20.6213203,19.9852814 C20.2307961,19.5947571 20.2307961,18.9615921 20.6213203,18.5710678 L22.0355339,17.1568542 C22.4260582,16.76633 23.0592232,16.76633 23.4497475,17.1568542 Z M12.6213203,17.1568542 C13.0118446,16.76633 13.6450096,16.76633 14.0355339,17.1568542 L15.4497475,18.5710678 C15.8402718,18.9615921 15.8402718,19.5947571 15.4497475,19.9852814 C15.0592232,20.3758057 14.4260582,20.3758057 14.0355339,19.9852814 L12.6213203,18.5710678 C12.2307961,18.1805435 12.2307961,17.5473785 12.6213203,17.1568542 Z" fill="#000000" opacity="0.3" transform="translate(18.035534, 17.863961) scale(1, -1) rotate(45.000000) translate(-18.035534, -17.863961)" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
									</span>
								</div>
								<!--end::Symbol-->
								<!--begin::Text-->
								<div class="d-flex flex-column font-weight-bold">
									<a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Development</a>
									<span class="text-muted">DevOps</span>
								</div>
								<!--end::Text-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex align-items-center mb-2">
								<!--begin::Symbol-->
								<div class="symbol symbol-40 symbol-light-info mr-5">
									<span class="symbol-label">
										<span class="svg-icon svg-icon-lg svg-icon-info">
											<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Shield-user.svg-->
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3" />
													<path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3" />
													<path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3" />
												</g>
											</svg>
											<!--end::Svg Icon-->
										</span>
									</span>
								</div>
								<!--end::Symbol-->
								<!--begin::Text-->
								<div class="d-flex flex-column font-weight-bold">
									<a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Testing</a>
									<span class="text-muted">QA Managers</span>
								</div>
								<!--end::Text-->
							</div>
							<!--end::Item-->
						</div>
						<!--end::Body-->
					</div>
					<!--end::List Widget 1-->
				</div>
			</div>
			<!--end::Row-->
			<!--begin::Row-->
			<div class="row">
				<div class="col-xl-5">
					<div class="card-stretch gutter-b">
						<!--begin::Stats Widget 11-->
						<div class="card card-custom gutter-b">
							<!--begin::Body-->
							<div class="card-body p-0">
								<div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
									<span class="symbol symbol-50 symbol-light-success mr-2">
										<span class="symbol-label">
											<span class="svg-icon svg-icon-xl svg-icon-success">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
														<path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
													</g>
												</svg>
												<!--end::Svg Icon-->
											</span>
										</span>
									</span>
									<div class="d-flex flex-column text-right">
										<span class="text-dark-75 font-weight-bolder font-size-h3">750$</span>
										<span class="text-muted font-weight-bold mt-2">Weekly Income</span>
									</div>
								</div>
								<div id="kt_stats_widget_11_chart" class="card-rounded-bottom" data-color="success" style="height: 150px"></div>
							</div>
							<!--end::Body-->
						</div>
						<!--end::Stats Widget 11-->
						<!--begin::Stats Widget 10-->
						<div class="card card-custom">
							<!--begin::Body-->
							<div class="card-body p-0">
								<div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
									<span class="symbol symbol-50 symbol-light-info mr-2">
										<span class="symbol-label">
											<span class="svg-icon svg-icon-xl svg-icon-info">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
														<path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
													</g>
												</svg>
												<!--end::Svg Icon-->
											</span>
										</span>
									</span>
									<div class="d-flex flex-column text-right">
										<span class="text-dark-75 font-weight-bolder font-size-h3">+259</span>
										<span class="text-muted font-weight-bold mt-2">Sales Change</span>
									</div>
								</div>
								<div id="kt_stats_widget_10_chart" class="card-rounded-bottom" data-color="info" style="height: 150px"></div>
							</div>
							<!--end::Body-->
						</div>
						<!--end::Stats Widget 10-->
					</div>
				</div>
				<div class="col-xl-7">
					<!--begin::Base Table Widget 5-->
					<div class="card card-custom card-stretch gutter-b">
						<!--begin::Header-->
						<div class="card-header border-0 pt-5">
							<h3 class="card-title align-items-start flex-column">
								<span class="card-label font-weight-bolder text-dark">New Arrivals</span>
								<span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new members</span>
							</h3>
							<div class="card-toolbar">
								<ul class="nav nav-pills nav-pills-sm nav-dark-75">
									<li class="nav-item">
										<a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_5_1">Month</a>
									</li>
									<li class="nav-item">
										<a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_5_2">Week</a>
									</li>
									<li class="nav-item">
										<a class="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_tab_pane_5_3">Day</a>
									</li>
								</ul>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body pt-2 pb-0 mt-n3">
							<div class="tab-content mt-5" id="myTabTables5">
								<!--begin::Tap pane-->
								<div class="tab-pane fade" id="kt_tab_pane_5_1" role="tabpanel" aria-labelledby="kt_tab_pane_5_1">
									<!--begin::Table-->
									<div class="table-responsive">
										<table class="table table-borderless table-vertical-center">
											<thead>
												<tr>
													<th class="p-0 w-50px"></th>
													<th class="p-0 min-w-150px"></th>
													<th class="p-0 min-w-140px"></th>
													<th class="p-0 min-w-110px"></th>
													<th class="p-0 min-w-50px"></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="pl-0 py-5">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/015-telegram.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Popular Authors</a>
														<span class="text-muted font-weight-bold d-block">Most Successful</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">Python, MySQL</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-warning label-inline">In Progress</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/003-puzzle.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">New Users</a>
														<span class="text-muted font-weight-bold d-block">Awesome Users</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">Laravel,Metronic</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-success label-inline">Success</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/005-bebo.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Active Customers</a>
														<span class="text-muted font-weight-bold d-block">Best Customers</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">AngularJS, C#</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-danger label-inline">Rejected</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/006-plurk.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Top Authors</a>
														<span class="text-muted font-weight-bold d-block">Successful Fellas</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">ReactJs, HTML</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-primary label-inline">Approved</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/014-kickstarter.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Bestseller Theme</a>
														<span class="text-muted font-weight-bold d-block">Amazing Templates</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">ReactJS, Ruby</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-warning label-inline">In Progress</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--end::Table-->
								</div>
								<!--end::Tap pane-->
								<!--begin::Tap pane-->
								<div class="tab-pane fade" id="kt_tab_pane_5_2" role="tabpanel" aria-labelledby="kt_tab_pane_5_2">
									<!--begin::Table-->
									<div class="table-responsive">
										<table class="table table-borderless table-vertical-center">
											<thead>
												<tr>
													<th class="p-0 w-50px"></th>
													<th class="p-0 min-w-150px"></th>
													<th class="p-0 min-w-140px"></th>
													<th class="p-0 min-w-110px"></th>
													<th class="p-0 min-w-50px"></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/005-bebo.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Active Customers</a>
														<span class="text-muted font-weight-bold d-block">Best Customers</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">AngularJS, C#</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-danger label-inline">Rejected</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/014-kickstarter.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Bestseller Theme</a>
														<span class="text-muted font-weight-bold d-block">Amazing Templates</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">ReactJS, Ruby</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-warning label-inline">In Progress</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/006-plurk.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Top Authors</a>
														<span class="text-muted font-weight-bold d-block">Successful Fellas</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">ReactJs, HTML</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-primary label-inline">Approved</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="pl-0 py-5">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/015-telegram.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Popular Authors</a>
														<span class="text-muted font-weight-bold d-block">Most Successful</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">Python, MySQL</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-warning label-inline">In Progress</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/003-puzzle.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">New Users</a>
														<span class="text-muted font-weight-bold d-block">Awesome Users</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">Laravel,Metronic</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-success label-inline">Success</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--end::Table-->
								</div>
								<!--end::Tap pane-->
								<!--begin::Tap pane-->
								<div class="tab-pane fade show active" id="kt_tab_pane_5_3" role="tabpanel" aria-labelledby="kt_tab_pane_5_3">
									<!--begin::Table-->
									<div class="table-responsive">
										<table class="table table-borderless table-vertical-center">
											<thead>
												<tr>
													<th class="p-0 w-50px"></th>
													<th class="p-0 min-w-150px"></th>
													<th class="p-0 min-w-140px"></th>
													<th class="p-0 min-w-110px"></th>
													<th class="p-0 min-w-50px"></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/006-plurk.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Top Authors</a>
														<span class="text-muted font-weight-bold d-block">Successful Fellas</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">ReactJs, HTML</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-primary label-inline">Approved</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="pl-0 py-5">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/015-telegram.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Popular Authors</a>
														<span class="text-muted font-weight-bold d-block">Most Successful</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">Python, MySQL</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-warning label-inline">In Progress</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/003-puzzle.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">New Users</a>
														<span class="text-muted font-weight-bold d-block">Awesome Users</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">Laravel,Metronic</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-success label-inline">Success</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/005-bebo.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Active Customers</a>
														<span class="text-muted font-weight-bold d-block">Best Customers</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">AngularJS, C#</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-danger label-inline">Rejected</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
												<tr>
													<td class="py-5 pl-0">
														<div class="symbol symbol-50 symbol-light mr-2">
															<span class="symbol-label">
																<img src="assets/media/svg/misc/014-kickstarter.svg" class="h-50 align-self-center" alt="" />
															</span>
														</div>
													</td>
													<td class="pl-0">
														<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Bestseller Theme</a>
														<span class="text-muted font-weight-bold d-block">Amazing Templates</span>
													</td>
													<td class="text-right">
														<span class="text-muted font-weight-500">ReactJS, Ruby</span>
													</td>
													<td class="text-right">
														<span class="label label-lg label-light-warning label-inline">In Progress</span>
													</td>
													<td class="text-right pr-0">
														<a href="#" class="btn btn-icon btn-light btn-sm">
															<span class="svg-icon svg-icon-md svg-icon-success">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--end::Table-->
								</div>
								<!--end::Tap pane-->
							</div>
						</div>
						<!--end::Body-->
					</div>
					<!--end::Base Table Widget 5-->
				</div>
			</div>
			<!--end::Row-->
			<!--begin::Row-->
			<div class="row">
				<div class="col-xl-5">
					<!--begin::List Widget 4-->
					<div class="card card-custom card-stretch gutter-b">
						<!--begin::Header-->
						<div class="card-header border-0">
							<h3 class="card-title font-weight-bolder text-dark">Todo</h3>
							<div class="card-toolbar">
								<div class="dropdown dropdown-inline">
									<a href="#" class="btn btn-light btn-sm font-size-sm font-weight-bolder dropdown-toggle text-dark-75" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Create</a>
									<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header pb-1">
												<span class="text-primary text-uppercase font-weight-bold font-size-sm">Add new:</span>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-shopping-cart-1"></i>
													</span>
													<span class="navi-text">Order</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-calendar-8"></i>
													</span>
													<span class="navi-text">Event</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-graph-1"></i>
													</span>
													<span class="navi-text">Report</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-rocket-1"></i>
													</span>
													<span class="navi-text">Post</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-writing"></i>
													</span>
													<span class="navi-text">File</span>
												</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body pt-2">
							<!--begin::Item-->
							<div class="d-flex align-items-center">
								<!--begin::Bullet-->
								<span class="bullet bullet-bar bg-success align-self-stretch"></span>
								<!--end::Bullet-->
								<!--begin::Checkbox-->
								<label class="checkbox checkbox-lg checkbox-light-success checkbox-inline flex-shrink-0 m-0 mx-4">
									<input type="checkbox" name="select" value="1" />
									<span></span>
								</label>
								<!--end::Checkbox-->
								<!--begin::Text-->
								<div class="d-flex flex-column flex-grow-1">
									<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg mb-1">Create FireStone Logo</a>
									<span class="text-muted font-weight-bold">Due in 2 Days</span>
								</div>
								<!--end::Text-->
								<!--begin::Dropdown-->
								<div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
									<a href="#" class="btn btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-hor"></i>
									</a>
									<div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header font-weight-bold py-4">
												<span class="font-size-lg">Choose Label:</span>
												<i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>
											</li>
											<li class="navi-separator mb-3 opacity-70"></li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-success">Customer</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-danger">Partner</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-warning">Suplier</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-primary">Member</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-dark">Staff</span>
													</span>
												</a>
											</li>
											<li class="navi-separator mt-3 opacity-70"></li>
											<li class="navi-footer py-4">
												<a class="btn btn-clean font-weight-bold btn-sm" href="#">
												<i class="ki ki-plus icon-sm"></i>Add new</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
								<!--end::Dropdown-->
							</div>
							<!--end:Item-->
							<!--begin::Item-->
							<div class="d-flex align-items-center mt-10">
								<!--begin::Bullet-->
								<span class="bullet bullet-bar bg-primary align-self-stretch"></span>
								<!--end::Bullet-->
								<!--begin::Checkbox-->
								<label class="checkbox checkbox-lg checkbox-light-primary checkbox-inline flex-shrink-0 m-0 mx-4">
									<input type="checkbox" value="1" />
									<span></span>
								</label>
								<!--end::Checkbox-->
								<!--begin::Text-->
								<div class="d-flex flex-column flex-grow-1">
									<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg mb-1">Stakeholder Meeting</a>
									<span class="text-muted font-weight-bold">Due in 3 Days</span>
								</div>
								<!--end::Text-->
								<!--begin::Dropdown-->
								<div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
									<a href="#" class="btn btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-hor"></i>
									</a>
									<div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header font-weight-bold py-4">
												<span class="font-size-lg">Choose Label:</span>
												<i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>
											</li>
											<li class="navi-separator mb-3 opacity-70"></li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-success">Customer</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-danger">Partner</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-warning">Suplier</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-primary">Member</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-dark">Staff</span>
													</span>
												</a>
											</li>
											<li class="navi-separator mt-3 opacity-70"></li>
											<li class="navi-footer py-4">
												<a class="btn btn-clean font-weight-bold btn-sm" href="#">
												<i class="ki ki-plus icon-sm"></i>Add new</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
								<!--end::Dropdown-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex align-items-center mt-10">
								<!--begin::Bullet-->
								<span class="bullet bullet-bar bg-warning align-self-stretch"></span>
								<!--end::Bullet-->
								<!--begin::Checkbox-->
								<label class="checkbox checkbox-lg checkbox-light-warning checkbox-inline flex-shrink-0 m-0 mx-4">
									<input type="checkbox" value="1" />
									<span></span>
								</label>
								<!--end::Checkbox-->
								<!--begin::Text-->
								<div class="d-flex flex-column flex-grow-1">
									<a href="#" class="text-dark-75 text-hover-primary font-size-sm font-weight-bold font-size-lg mb-1">Scoping &amp; Estimations</a>
									<span class="text-muted font-weight-bold">Due in 5 Days</span>
								</div>
								<!--end::Text-->
								<!--begin: Dropdown-->
								<div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
									<a href="#" class="btn btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-hor"></i>
									</a>
									<div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header font-weight-bold py-4">
												<span class="font-size-lg">Choose Label:</span>
												<i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>
											</li>
											<li class="navi-separator mb-3 opacity-70"></li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-success">Customer</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-danger">Partner</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-warning">Suplier</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-primary">Member</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-dark">Staff</span>
													</span>
												</a>
											</li>
											<li class="navi-separator mt-3 opacity-70"></li>
											<li class="navi-footer py-4">
												<a class="btn btn-clean font-weight-bold btn-sm" href="#">
												<i class="ki ki-plus icon-sm"></i>Add new</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
								<!--end::Dropdown-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex align-items-center mt-10">
								<!--begin::Bullet-->
								<span class="bullet bullet-bar bg-info align-self-stretch"></span>
								<!--end::Bullet-->
								<!--begin::Checkbox-->
								<label class="checkbox checkbox-lg checkbox-light-info checkbox-inline flex-shrink-0 m-0 mx-4">
									<input type="checkbox" value="1" />
									<span></span>
								</label>
								<!--end::Checkbox-->
								<!--begin::Text-->
								<div class="d-flex flex-column flex-grow-1">
									<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg mb-1">Sprint Showcase</a>
									<span class="text-muted font-weight-bold">Due in 1 Day</span>
								</div>
								<!--end::Text-->
								<!--begin::Dropdown-->
								<div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
									<a href="#" class="btn btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-hor"></i>
									</a>
									<div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover py-5">
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-drop"></i>
													</span>
													<span class="navi-text">New Group</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-list-3"></i>
													</span>
													<span class="navi-text">Contacts</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-rocket-1"></i>
													</span>
													<span class="navi-text">Groups</span>
													<span class="navi-link-badge">
														<span class="label label-light-primary label-inline font-weight-bold">new</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-bell-2"></i>
													</span>
													<span class="navi-text">Calls</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-gear"></i>
													</span>
													<span class="navi-text">Settings</span>
												</a>
											</li>
											<li class="navi-separator my-3"></li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-magnifier-tool"></i>
													</span>
													<span class="navi-text">Help</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-bell-2"></i>
													</span>
													<span class="navi-text">Privacy</span>
													<span class="navi-link-badge">
														<span class="label label-light-danger label-rounded font-weight-bold">5</span>
													</span>
												</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
								<!--end::Dropdown-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex align-items-center mt-10">
								<!--begin::Bullet-->
								<span class="bullet bullet-bar bg-danger align-self-stretch"></span>
								<!--end::Bullet-->
								<!--begin::Checkbox-->
								<label class="checkbox checkbox-lg checkbox-light-danger checkbox-inline flex-shrink-0 m-0 mx-4">
									<input type="checkbox" value="1" />
									<span></span>
								</label>
								<!--end::Checkbox:-->
								<!--begin::Title-->
								<div class="d-flex flex-column flex-grow-1">
									<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg mb-1">Project Retro</a>
									<span class="text-muted font-weight-bold">Due in 12 Days</span>
								</div>
								<!--end::Text-->
								<!--begin: Dropdown-->
								<div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
									<a href="#" class="btn btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-hor"></i>
									</a>
									<div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header font-weight-bold py-4">
												<span class="font-size-lg">Choose Label:</span>
												<i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>
											</li>
											<li class="navi-separator mb-3 opacity-70"></li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-success">Customer</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-danger">Partner</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-warning">Suplier</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-primary">Member</span>
													</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-text">
														<span class="label label-xl label-inline label-light-dark">Staff</span>
													</span>
												</a>
											</li>
											<li class="navi-separator mt-3 opacity-70"></li>
											<li class="navi-footer py-4">
												<a class="btn btn-clean font-weight-bold btn-sm" href="#">
												<i class="ki ki-plus icon-sm"></i>Add new</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
								<!--end::Dropdown-->
							</div>
							<!--end::Item-->
						</div>
						<!--end::Body-->
					</div>
					<!--end:List Widget 4-->
				</div>
				<div class="col-xl-7">
					<!--begin::List Widget 12-->
					<div class="card card-custom card-stretch">
						<!--begin::Header-->
						<div class="card-header border-0">
							<h3 class="card-title font-weight-bolder text-dark">Latest Media</h3>
							<div class="card-toolbar">
								<div class="dropdown dropdown-inline">
									<a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-ver"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header pb-1">
												<span class="text-primary text-uppercase font-weight-bold font-size-sm">Add new:</span>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-shopping-cart-1"></i>
													</span>
													<span class="navi-text">Order</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-calendar-8"></i>
													</span>
													<span class="navi-text">Event</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-graph-1"></i>
													</span>
													<span class="navi-text">Report</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-rocket-1"></i>
													</span>
													<span class="navi-text">Post</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-writing"></i>
													</span>
													<span class="navi-text">File</span>
												</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body pt-2">
							<!--begin::Item-->
							<div class="d-flex flex-wrap align-items-center mb-10">
								<!--begin::Symbol-->
								<div class="symbol symbol-60 symbol-2by3 flex-shrink-0">
									<div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-20.jpg')"></div>
								</div>
								<!--end::Symbol-->
								<!--begin::Title-->
								<div class="d-flex flex-column ml-4 flex-grow-1 mr-2">
									<a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Cup &amp; Green</a>
									<span class="text-muted font-weight-bold">Size: 87KB</span>
								</div>
								<!--end::Title-->
								<!--begin::btn-->
								<span class="label label-lg label-light-primary label-inline mt-lg-0 mb-lg-0 my-2 font-weight-bold py-4">Approved</span>
								<!--end::Btn-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex flex-wrap align-items-center mb-10">
								<!--begin::Symbol-->
								<div class="symbol symbol-60 symbol-2by3 flex-shrink-0">
									<div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-19.jpg')"></div>
								</div>
								<!--end::Symbol-->
								<!--begin::Title-->
								<div class="d-flex flex-column ml-4 flex-grow-1 mr-2">
									<a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Yellow Background</a>
									<span class="text-muted font-weight-bold">Size: 1.2MB</span>
								</div>
								<!--end::Title-->
								<!--begin::btn-->
								<span class="label label-lg label-light-warning label-inline mt-lg-0 mb-lg-0 my-2 font-weight-bold py-4">In Progress</span>
								<!--end::Btn-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex flex-wrap align-items-center mb-10">
								<!--begin::Symbol-->
								<div class="symbol symbol-60 symbol-2by3 flex-shrink-0">
									<div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-25.jpg')"></div>
								</div>
								<!--end::Symbol-->
								<!--begin::Title-->
								<div class="d-flex flex-column ml-4 flex-grow-1 mr-2">
									<a href="#" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">Nike &amp; Blue</a>
									<span class="text-muted font-weight-bold">Size: 87KB</span>
								</div>
								<!--end::Title-->
								<!--begin::btn-->
								<span class="label label-lg label-light-success label-inline mt-lg-0 mb-lg-0 my-2 font-weight-bold py-4">Success</span>
								<!--end::btn-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex flex-wrap align-items-center">
								<!--begin::Symbol-->
								<div class="symbol symbol-60 symbol-2by3 flex-shrink-0">
									<div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-24.jpg')"></div>
								</div>
								<!--end::Symbol-->
								<!--begin::Title-->
								<div class="d-flex flex-column ml-4 flex-grow-1 mr-2">
									<a href="#" class="text-dark-75 font-weight-bold font-size-lg text-hover-primary mb-1">Red Boots</a>
									<span class="text-muted font-weight-bold">Size: 345KB</span>
								</div>
								<!--end::Title-->
								<!--begin::btn-->
								<span class="label label-lg label-light-danger label-inline mt-lg-0 mb-lg-0 my-2 font-weight-bold py-4">Rejected</span>
								<!--end::btn-->
							</div>
							<!--end::Item-->
						</div>
						<!--end::Body-->
					</div>
					<!--end::List Widget 12-->
				</div>
			</div>
			<!--end::Row-->
			<!--end::Dashboard-->
		<!--</div>-->
		<!--end::Container-->
	</div>
	<!--end::Entry-->
	<!--begin::Aside Secondary-->
				
</div>
<!--end::Content-->

<script>
	var date=new Date();

	$(document).ready(function (){
		var months=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
					"Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
		var val="Hoy, " + date.getDate()+" de "+months[date.getMonth()]+" del "+date.getFullYear();
		
		$("#FechaHorario").text(val);
	});
</script>