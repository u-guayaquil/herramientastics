<?php
date_default_timezone_set('America/Guayaquil');
require_once "../Models/ModelConsultar.php";
require_once "../View/ViewConsultar.php";
require_once "../utils/libs.php";
class ControladorConsulta{
    public static function ctrBuscarHorario($Datos){
        $Privado = 'UG20*';
        if(isset($Datos['Clave']) && $Datos['Clave'] == $Privado)
        {   $DocenteAula = rtrim(ltrim(strtoupper($_POST['DocenteAula']))); 
            $dia = date("N");
            $TxDia = Libs::ObtenerTXDia($dia);
            $Datos["TxDia"] =$TxDia; $Datos["Dia"] =$dia;
            if(is_numeric($DocenteAula)){
                $DataCollection = ModeloConsulta::mdlConsultarHorarioAulaClave($Datos);
                if($DataCollection['status']==1){
                    $respuesta = VistaConsulta::viewPintarTablaClave($DataCollection['message']);
                    return array("status"=> 1, "message"=>$respuesta);
                }
                else{
                    return $DataCollection;
                }
            }   else    {
                $DataCollection = ModeloConsulta::mdlConsultarHorarioDocenteClave($Datos);
                if($DataCollection['status']==1){
                    $respuesta = VistaConsulta::viewPintarTablaClave($DataCollection['message']);
                    return array("status"=> 1, "message"=>$respuesta);
                }
                else{
                    return $DataCollection;
                }
            }
        }   else    {
            $DocenteAula = rtrim(ltrim(strtoupper($_POST['DocenteAula']))); 
            $dia = date("N");
            $TxDia = Libs::ObtenerTXDia($dia);
            $Datos["TxDia"] =$TxDia; $Datos["Dia"] =$dia;
            if(is_numeric($DocenteAula)){
                $DataCollection = ModeloConsulta::mdlConsultarHorarioAula($Datos);
                if($DataCollection['status']==1){
                    $respuesta = VistaConsulta::viewPintarTabla($DataCollection['message']);
                    return array("status"=> 1, "message"=>$respuesta);
                }
                else{
                    return $DataCollection;
                }
            }   else    {
                $DataCollection = ModeloConsulta::mdlConsultarHorarioDocente($Datos);
                if($DataCollection['status']==1){
                    $respuesta = VistaConsulta::viewPintarTabla($DataCollection['message']);
                    return array("status"=> 1, "message"=>$respuesta);
                }
                else{
                    return $DataCollection;
                }
            }

        }
    }
}
?>