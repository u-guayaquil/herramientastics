<?php
date_default_timezone_set('America/Guayaquil');
    class VistaConsulta{
        static public function viewPintarTabla($DataCollection)
        {   $dia = date("N");$TxDia = Libs::ObtenerTXDia($dia); $TxMes = Libs::ObtenerTXMes(date('m'));
            $HOY = $TxDia.", ".date('d')." DE ".$TxMes." ".date('Y');
            $HTML = '<div class="col-md-12 offset-md-0 card-borrar">
                        <div class="card"> 
                            <h5 class="card-header text-center">Resultados de búsqueda</h5>
                            <div class="card-body form-horizontal">
                                <label for="" style="color:red">LOS DATOS DE LA CONSULTA PERTENECEN AL DÍA DE HOY - '.$HOY.'</label>
                                <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                                    <thead>
                                    <tr>
                                        <th>Facultad</th>
                                        <th>Docente</th>
                                        <th>Materia</th>
                                        <th>Hora</th>
                                        <th>Aula</th>
                                        <th>Correo Institucional</th>
                                    </tr> </thead> <tbody>';
                                    $count = 1;
                                    foreach($DataCollection as $Row){
                                        $HTML.= '<tr>';
                                        $HTML.= '<td>'.$Row['FACULTAD'].'</td>';
                                        $HTML.= '<td>'.$Row['DOCENTE'].'</td>';
                                        $HTML.= '<td>'.$Row['MATERIA'].'</td>';
                                        $HTML.= '<td>'.$Row['3'].'</td>';
                                        $HTML.= '<td>'.$Row['4'].'</td>';
                                        $HTML.= '<td>'.$Row['EMAIL'].'</td>';
                                        $count++;
                                    }   
                                $HTML.= '</tbody> 
                                </table>
                            </div>
                        </div>
                    </div>';
            return $HTML;
        }

        static public function viewPintarTablaClave($DataCollection)
        {   $dia = date("N");$TxDia = Libs::ObtenerTXDia($dia); $TxMes = Libs::ObtenerTXMes(date('m'));
            $HOY = $TxDia.", ".date('d')." DE ".$TxMes." ".date('Y');
            $HTML = '<div class="col-md-12 offset-md-0 card-borrar">
                        <div class="card"> 
                            <h5 class="card-header text-center">Resultados de búsqueda</h5>
                            <div class="card-body form-horizontal">
                                <label for="" style="color:red">LOS DATOS DE LA CONSULTA PERTENECEN AL DÍA DE HOY - '.$HOY.'</label>
                                <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                                    <thead>
                                    <tr>
                                        <th>Facultad</th>
                                        <th>Docente</th>
                                        <th>Materia</th>
                                        <th>Hora</th>
                                        <th>Aula</th>
                                        <th style="text-align:center">Correo Institucional</th>
                                        <th>Correo Zoom</th>
                                        <th>Clave Zoom</th>
                                    </tr> </thead> <tbody>';
                                    $count = 1;
                                    foreach($DataCollection as $Row){
                                        $HTML.= '<tr>';
                                        $HTML.= '<td>'.$Row['FACULTAD'].'</td>';
                                        $HTML.= '<td>'.$Row['DOCENTE'].'</td>';
                                        $HTML.= '<td>'.$Row['MATERIA'].'</td>';
                                        $HTML.= '<td>'.$Row['3'].'</td>';
                                        $HTML.= '<td>'.$Row['4'].'</td>';
                                        $HTML.= '<td style="text-align:center">'.$Row['EMAIL'].'</td>';
                                        $HTML.= '<td>'.$Row['CUENTA'].'</td>';
                                        $HTML.= '<td>'.$Row['CLAVE'].'</td>';
                                        $count++;
                                    }   
                                $HTML.= '</tbody> 
                                </table>
                            </div>
                        </div>
                    </div>';
            return $HTML;
        }
    }
?>