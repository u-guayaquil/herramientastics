    <!-- JQuery 3.5.1 -->
    <script src="plugins/jquery/jquery-3.5.1.min.js?v1"></script> 
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js?v1"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js?v1"></script>
    <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js?v1"></script>
    <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js?v1"></script>
    <!-- BlockUI -->
    <script src="plugins/blockui-js/jquery.blockUI.js?v1"></script>
    <!-- Data Picker -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- JS Personalizable -->
    <script src="js/consulta.js?v1.0"></script>