<?php 
    include("utils/conexion.php");
    include("utils/libs.php");
    $Privado = 'UG20*';
    date_default_timezone_set('America/Guayaquil');
?>
<!DOCTYPE html> 
<meta charset="UTF-8">
<html> 	
	<head>
    <!-- Metas -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HORARIOS - UG</title>
    <link  rel="icon"   href="img/favicon.png" type="image/png" />
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">     
    <!-- CSS Personalizable -->
    <link href="css/style.css" rel="stylesheet">			
	</head>
<body>
	<div class="col-md-10 col-md-offset-1">
		<a href="index.php" style="text-decoration:none"><h1 class="center">CONSULTA DE HORARIOS - UG</h1></a>

		<form method="POST" action="index.php" autocomplete="nope">
			<div class="form-group row">
                <div class="col-md-2"></div>
				<div class="col-md-4">
                    <label>Apellidos de Docente o Número de Aula*</label>
                    <input type="text" name="DocenteAula" id="DocenteAula" class="form-control" placeholder="Ingrese Datos"><br />
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <label>Clave de Administrador <i>(OPCIONAL)</i></label>
                    <input type="password" name="Clave" id="Clave" class="form-control col-xs-3" placeholder="Solo Para Administradores" style="padding:20px;"><br />
                </div>
            </div>
            <div class="row">
                
            </div>    
			<div class="form-group row">				
                <div class="col-md-4"></div>
                <div class="col-md-4 center">
                    <input type="submit" name="insert" class="btn btn-info" value="CONSULTAR"><br/>
                </div>
			</div>
		</form>
	</div>
	<div class="col-md-10 col-md-offset-1">
    <?php
    if(isset($_POST['DocenteAula']) && $_POST['DocenteAula'] != '')
    {
        if(isset($_POST['Clave']) && $_POST['Clave'] == $Privado)
        {
        ?>
            <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                <thead>
                    <tr>
                        <th>Facultad</th>
                        <th>Docente</th>
                        <th>Materia</th>
                        <th>Hora</th>
                        <th>Aula</th>
                        <th>Correo Institucional</th>
                        <th>ID Zoom</th>
                        <th>Correo Zoom</th>
                        <th>Clave Zoom</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if(isset($_POST['DocenteAula']) && $_POST['DocenteAula']!='' && $_POST['DocenteAula']!=' ' && $_POST['DocenteAula']!='  '
                    && $_POST['DocenteAula']!='   ' && $_POST['DocenteAula']!='    ' && $_POST['DocenteAula']!='      ' 
                    && $_POST['DocenteAula']!='      ' && $_POST['DocenteAula']!='.' && $_POST['DocenteAula']!='-' && $_POST['DocenteAula']!='{'
                    && $_POST['DocenteAula']!='}' && $_POST['DocenteAula']!='+' && $_POST['DocenteAula']!='*' && $_POST['DocenteAula']!='/'
                    && $_POST['DocenteAula']!='_' && $_POST['DocenteAula']!=':' && $_POST['DocenteAula']!=',' && $_POST['DocenteAula']!=';'
                    && $_POST['DocenteAula']!='(' && $_POST['DocenteAula']!=')' && $_POST['DocenteAula']!='&' && $_POST['DocenteAula']!='%'
                    && $_POST['DocenteAula']!='$' && $_POST['DocenteAula']!='#' && $_POST['DocenteAula']!='"' && $_POST['DocenteAula']!='!'
                    && $_POST['DocenteAula']!='¡' && $_POST['DocenteAula']!='?' && $_POST['DocenteAula']!='¿' && $_POST['DocenteAula']!='´'
                    && $_POST['DocenteAula']!='[' && $_POST['DocenteAula']!=']' && $_POST['DocenteAula']!='¨' && $_POST['DocenteAula']!='|' 
                    && $_POST['DocenteAula']!='<' && $_POST['DocenteAula']!='>' && $_POST['DocenteAula']!='°' && $_POST['DocenteAula']!='¬'){
                        $Variable = rtrim(ltrim(strtoupper($_POST['DocenteAula']))); 
                        $dia = date("N");
                        $TxDia = Libs::ObtenerTXDia($dia);

                        if(is_numeric($Variable)){
                            $QUERY = "SELECT DISTINCT FACULTAD,DOCENTE,MATERIA,".$TxDia.",LICENCIA_".$dia.",
                                        CASE
                                        WHEN EMAIL not LIKE '%@ug.edu.ec%' THEN 'NO' 
                                        ELSE 'SI'
                                        END EMAIL,
                                        L.CUENTA, L.CLAVE, L.LINK
                                        FROM CAMPUSVIRTUAL C
                                        LEFT JOIN LICENCIA L ON L.AULA = C.LICENCIA_".$dia."
                                        WHERE INSCRITOS>0 AND ESTADO = 1 AND LICENCIA_".$dia." = '$Variable' AND ".$TxDia." <> '' ORDER BY ".$TxDia;
                        }
                        else{
                            $QUERY = "SELECT DISTINCT FACULTAD,DOCENTE,MATERIA,".$TxDia.",LICENCIA_".$dia.", 
                                        CASE
                                        WHEN EMAIL not LIKE '%@ug.edu.ec%' THEN 'NO' 
                                        ELSE 'SI'
                                        END EMAIL
                                        ,
                                        L.CUENTA, L.CLAVE, L.LINK
                                        FROM CAMPUSVIRTUAL C
                                        LEFT JOIN LICENCIA L ON L.AULA = C.LICENCIA_".$dia."
                                        WHERE INSCRITOS>0 AND ESTADO = 1 AND DOCENTE LIKE UPPER('%$Variable%') AND ".$TxDia." <> '' ORDER BY ".$TxDia;
                        }
                        $ejecutar = sqlsrv_query($con, $QUERY);
                        ?>
                        <label for="" style="color:red">LOS DATOS DE LA CONSULTA PERTENECEN AL DÍA DE HOY - <?php $dia = date("N");
                        $TxDia = Libs::ObtenerTXDia($dia); $TxMes = Libs::ObtenerTXMes(date('m')); echo $TxDia.", ".date('d')." DE ".$TxMes." ".date('Y');?></label>
                        <?php
                            if($ejecutar)
                            {
                                while($fila = sqlsrv_fetch_array($ejecutar)){
                                    $facultad   = $fila['FACULTAD'];
                                    $docente    = $fila['DOCENTE'];
                                    $materia    = $fila['MATERIA'];
                                    $hora       = $fila[$TxDia];
                                    $aula       = $fila['LICENCIA_'.$dia];
                                    $email      = $fila['EMAIL'];
                                    $cuenta     = $fila['CUENTA'];
                                    $clave      = $fila['CLAVE'];
                                    $link       = $fila['LINK'];

                        ?>

                        <tr align="center">
                            <td><?php echo $facultad; ?></td>
                            <td><?php echo $docente; ?></td>
                            <td><?php echo $materia; ?></td>
                            <td><?php echo $hora; ?></td>
                            <td><?php echo $aula; ?></td>
                            <td><?php echo $email;?></td>
                            <td><?php echo $link;?></td>
                            <td><?php echo $cuenta;?></td>
                            <td><?php echo $clave;?></td>
                        </tr>

                        <?php }
                        } else {
                        ?>
                            <tr>
                                <td colspan="9" align="center"> No hay datos para mostrar</td>
                            </tr>
                        <?php
                        } 
                    }else {
                        ?>
                            <tr>
                                <td colspan="9" align="center"> No hay datos para mostrar</td>
                            </tr>
                        <?php
                        } ?>
                </tbody>
            </table>
        <?php
        }
        else{
        ?>
            <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                <thead>
                    <tr>
                        <th>Facultad</th>
                        <th>Docente</th>
                        <th>Materia</th>
                        <th>Hora</th>
                        <th>Aula</th>
                        <th>Correo Institucional</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if(isset($_POST['DocenteAula']) && $_POST['DocenteAula']!='' && $_POST['DocenteAula']!=' ' && $_POST['DocenteAula']!='  '
                    && $_POST['DocenteAula']!='   ' && $_POST['DocenteAula']!='    ' && $_POST['DocenteAula']!='      ' 
                    && $_POST['DocenteAula']!='      ' && $_POST['DocenteAula']!='.' && $_POST['DocenteAula']!='-' && $_POST['DocenteAula']!='{'
                    && $_POST['DocenteAula']!='}' && $_POST['DocenteAula']!='+' && $_POST['DocenteAula']!='*' && $_POST['DocenteAula']!='/'
                    && $_POST['DocenteAula']!='_' && $_POST['DocenteAula']!=':' && $_POST['DocenteAula']!=',' && $_POST['DocenteAula']!=';'
                    && $_POST['DocenteAula']!='(' && $_POST['DocenteAula']!=')' && $_POST['DocenteAula']!='&' && $_POST['DocenteAula']!='%'
                    && $_POST['DocenteAula']!='$' && $_POST['DocenteAula']!='#' && $_POST['DocenteAula']!='"' && $_POST['DocenteAula']!='!'
                    && $_POST['DocenteAula']!='¡' && $_POST['DocenteAula']!='?' && $_POST['DocenteAula']!='¿' && $_POST['DocenteAula']!='´'
                    && $_POST['DocenteAula']!='[' && $_POST['DocenteAula']!=']' && $_POST['DocenteAula']!='¨' && $_POST['DocenteAula']!='|' 
                    && $_POST['DocenteAula']!='<' && $_POST['DocenteAula']!='>' && $_POST['DocenteAula']!='°' && $_POST['DocenteAula']!='¬'){
                        $Variable = rtrim(ltrim(strtoupper($_POST['DocenteAula']))); 
                        $dia = date("N");
                        $TxDia = Libs::ObtenerTXDia($dia);

                        if(is_numeric($Variable)){
                            $QUERY = "SELECT FACULTAD,DOCENTE,MATERIA,".$TxDia.",LICENCIA_".$dia.",
                                        CASE
                                        WHEN EMAIL not LIKE '%@ug.edu.ec%' THEN 'NO' 
                                        ELSE 'SI'
                                        END EMAIL 
                                        FROM CAMPUSVIRTUAL 
                                        WHERE INSCRITOS>0 
                                        AND ESTADO = 1
                                        AND LICENCIA_".$dia." = '$Variable' 
                                        AND ".$TxDia." <> '' 
                                        AND NIVEL != 0
                                        ORDER BY ".$TxDia;
                        }
                        else{
                            $QUERY = "SELECT FACULTAD,DOCENTE,MATERIA,".$TxDia.",LICENCIA_".$dia.", 
                                        CASE
                                        WHEN EMAIL not LIKE '%@ug.edu.ec%' THEN 'NO' 
                                        ELSE 'SI'
                                        END EMAIL
                                        FROM CAMPUSVIRTUAL 
                                        WHERE INSCRITOS>0 
                                        AND ESTADO = 1
                                        AND DOCENTE LIKE UPPER('%$Variable%') 
                                        AND ".$TxDia." <> ''
                                        AND NIVEL != 0 
                                        ORDER BY ".$TxDia;
                        }
                        //print_r($QUERY);
                        $ejecutar = sqlsrv_query($con, $QUERY);
                        ?>
                        <label for="" style="color:red">LOS DATOS DE LA CONSULTA PERTENECEN AL DÍA DE HOY - <?php $dia = date("N");
                        $TxDia = Libs::ObtenerTXDia($dia); $TxMes = Libs::ObtenerTXMes(date('m')); echo $TxDia.", ".date('d')." DE ".$TxMes." ".date('Y');?></label>
                        <?php
                            if($ejecutar)
                            {
                                while($fila = sqlsrv_fetch_array($ejecutar)){
                                    $facultad   = $fila['FACULTAD'];
                                    $docente    = $fila['DOCENTE'];
                                    $materia    = $fila['MATERIA'];
                                    $hora       = $fila[$TxDia];
                                    $aula       = $fila['LICENCIA_'.$dia];
                                    $email      = $fila['EMAIL'];
                        ?>

                        <tr align="center">
                            <td><?php echo $facultad; ?></td>
                            <td><?php echo $docente; ?></td>
                            <td><?php echo $materia; ?></td>
                            <td><?php echo $hora; ?></td>
                            <td><?php echo $aula; ?></td>
                            <td><?php echo $email;?></td>
                        </tr>

                        <?php }
                        } else {
                        ?>
                            <tr>
                                <td colspan="6" align="center"> No hay datos para mostrar</td>
                            </tr>
                        <?php
                        } 
                    }else {
                        ?>
                            <tr>
                                <td colspan="6" align="center"> No hay datos para mostrar</td>
                            </tr>
                        <?php
                        } ?>
                </tbody>
            </table>
        <?php
        }
    }
    else{
    }
    ?>
    <!-- All JS -->
    <?php require_once('utils/js.php');  ?>
    <!-- JS Personalizable -->
    <script src="js/js.js"></script> 
</body>
</html>