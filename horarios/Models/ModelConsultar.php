<?php
require "conexion.php";
    class ModeloConsulta{
        static public function mdlConsultarHorarioDocente($Datos)
        {   $DataCollection = [];
            $QUERY = "SELECT FACULTAD,DOCENTE,MATERIA,".$Datos['TxDia'].",LICENCIA_".$Datos['Dia'].", 
                                        CASE
                                        WHEN EMAIL not LIKE '%@ug.edu.ec%' THEN 'NO' 
                                        ELSE 'SI'
                                        END EMAIL
                        FROM CAMPUSVIRTUAL 
                        WHERE INSCRITOS>0 
                        AND ESTADO = 1
                        AND DOCENTE LIKE UPPER('%".$Datos['DocenteAula']."%') 
                        AND ".$Datos['TxDia']." <> ''
                        AND NIVEL != 0 
                        ORDER BY ".$Datos['TxDia'];
            $stmt = Conexion::conectar1()->prepare($QUERY);
            $stmt -> execute();
            $DataCollection = $stmt ->fetchAll();
            if($DataCollection)
            {    return array('status'=>1,'message'=>$DataCollection);   }
            else
            {   return array('status'=>0,'message'=>"NO HAY INFORMACIÓN DEL USUARIO SOLICITADO"); }
        }

        static public function mdlConsultarHorarioDocenteClave($Datos)
        {   $DataCollection = [];
            $QUERY = "SELECT DISTINCT FACULTAD,DOCENTE,MATERIA,".$Datos['TxDia'].",LICENCIA_".$Datos['Dia'].", 
                                    CASE
                                    WHEN EMAIL not LIKE '%@ug.edu.ec%' THEN 'NO' 
                                    ELSE 'SI'
                                    END EMAIL
                                    ,
                                    L.CUENTA, L.CLAVE, L.LINK
                        FROM CAMPUSVIRTUAL C
                        LEFT JOIN LICENCIA L ON L.AULA = C.LICENCIA_".$Datos['Dia']."
                        WHERE INSCRITOS>0 AND ESTADO = 1 AND DOCENTE LIKE UPPER('%".$Datos['DocenteAula']."%') AND ".$Datos['TxDia']." <> '' 
                        ORDER BY ".$Datos['TxDia'];
                        //print_r($QUERY);
            $stmt = Conexion::conectar1()->prepare($QUERY);
            $stmt -> execute();
            $DataCollection = $stmt ->fetchAll();
            if($DataCollection)
            {    return array('status'=>1,'message'=>$DataCollection);   }
            else
            {   return array('status'=>0,'message'=>"NO HAY INFORMACIÓN DEL USUARIO SOLICITADO"); }
        }

        static public function mdlConsultarHorarioAula($Datos)
        {   $DataCollection = [];
            $QUERY = "SELECT FACULTAD,DOCENTE,MATERIA,".$Datos['TxDia'].",LICENCIA_".$Datos['Dia'].",
                                CASE
                                WHEN EMAIL not LIKE '%@ug.edu.ec%' THEN 'NO' 
                                ELSE 'SI'
                                END EMAIL 
                                FROM CAMPUSVIRTUAL 
                        WHERE INSCRITOS>0 
                        AND ESTADO = 1
                        AND LICENCIA_".$Datos['Dia']." = :Aula
                        AND ".$Datos['TxDia']." <> '' 
                        AND NIVEL != 0
                        ORDER BY ".$Datos['TxDia'];
            $stmt = Conexion::conectar1()->prepare($QUERY);
            $stmt -> bindParam(":Aula", $Datos['DocenteAula'], PDO::PARAM_STR);
            $stmt -> execute();
            $DataCollection = $stmt ->fetchAll();
            if($DataCollection)
            {    return array('status'=>1,'message'=>$DataCollection);   }
            else
            {   return array('status'=>0,'message'=>"NO HAY INFORMACIÓN DEL AULA SOLICITADA"); }
        }
        
        static public function mdlConsultarHorarioAulaClave($Datos)
        {   $DataCollection = [];
            $QUERY = "SELECT DISTINCT FACULTAD,DOCENTE,MATERIA,".$Datos['TxDia'].",LICENCIA_".$Datos['Dia'].",
                                    CASE
                                    WHEN EMAIL not LIKE '%@ug.edu.ec%' THEN 'NO' 
                                    ELSE 'SI'
                                    END EMAIL,
                                    L.CUENTA, L.CLAVE, L.LINK
                        FROM CAMPUSVIRTUAL C
                        LEFT JOIN LICENCIA L ON L.AULA = C.LICENCIA_".$Datos['Dia']."
                        WHERE INSCRITOS>0 AND ESTADO = 1 
                        AND CUENTA LIKE ('%".$Datos['DocenteAula']."%')  
                        AND ".$Datos['TxDia']." <> '' 
                        ORDER BY ".$Datos['TxDia'];
                        //print_r($QUERY);
            $stmt = Conexion::conectar1()->prepare($QUERY);
            $stmt -> execute();
            $DataCollection = $stmt ->fetchAll();
            if($DataCollection)
            {    return array('status'=>1,'message'=>$DataCollection);   }
            else
            {   return array('status'=>0,'message'=>"NO HAY INFORMACIÓN DEL AULA SOLICITADA"); }
        }
    }
?>