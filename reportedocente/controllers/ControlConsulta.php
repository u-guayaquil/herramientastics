<?php
require_once "../models/ModelConsulta.php";
require_once "../view/VistaConsulta.php";
require_once "../utils/utils.php";
class ControladorConsulta{
    public static function ctrConsultarRegistroDocente($Cedula){
        $DataCollection = ModeloConsulta::mdlDatosRegistroAulaVirtualByDocente($Cedula);
        if($DataCollection['status']==1){
            $DatosDocente = ControladorConsulta::PreparaDatos($DataCollection['message']);
            $respuesta = VistaConsulta::viewPintarTablaReporte($DatosDocente);
            return array("status"=> 1, "message"=>$respuesta);
        }
        else{
            return $DataCollection;
        }
    }

    public static function PreparaDatos($DataCollection){
        $Contador=1;
        $Datos = [];
        $ArrayDatos = [];
        $ArrayTemporal = [];
        $Grupo = '';
        foreach($DataCollection as $Count => $Row){
            if($Grupo != $Row['COD_GRUPO']){
                $Grupo = $Row['COD_GRUPO'];
                $ArrayDatos['USUARIO']  = $Row['USUARIO'];
                $ArrayDatos['DOCENTE']  = $Row['DOCENTE'];
                $ArrayDatos['GRUPO']    = $Row['GRUPO'];
                $ArrayDatos['MATERIA']  = $Row['MATERIA'];
                $ArrayDatos['LUNES']  = $Row['LUNES'];
                $ArrayDatos['MARTES']  = $Row['MARTES'];
                $ArrayDatos['MIERCOLES']  = $Row['MIERCOLES'];
                $ArrayDatos['JUEVES']  = $Row['JUEVES'];
                $ArrayDatos['VIERNES']  = $Row['VIERNES'];
                $ArrayDatos['SABADO']  = $Row['SABADO'];
                foreach($DataCollection as $Reg){
                    if($Grupo == $Reg['COD_GRUPO']){
                        $Reg['DIASEMANA'] = Utils::DevolverDiaSemana(date('Y-m-d',strtotime($Reg['FECHA'])));
                        $ArrayTemporal[]  = $Reg;
                        $Contador++;
                    }
                }
                $ArrayDatos['DATOS'] = $ArrayTemporal;
                $Datos[] = $ArrayDatos;
                $Contador = 1;
            }
            $ArrayTemporal = [];
            $ArrayDatos = [];
        }
        return $Datos;
    }
}
?>