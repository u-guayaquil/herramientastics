<?php
    require __DIR__ . "/vendor/autoload.php";
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    use PhpOffice\PhpSpreadsheet\Worksheet;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Style\Border;
    use PhpOffice\PhpSpreadsheet\Style\Fill;
    use PhpOffice\PhpSpreadsheet\Style\Style;

    class Utils{
        public static function GenerarExcel($DataCollection){
            $documento = new Spreadsheet();
            $fila = 4;
            $documento
                ->getProperties()
                ->setCreator("FILOSOFIA - UG")
                ->setLastModifiedBy('UG')
                ->setTitle('REPORTE DE ESTUDIANTES REGISTRADOS EN EL CONGRESO DE FILOSOFIA')
                ->setSubject('REPORTE DE ESTUDIANTES REGISTRADOS EN EL CONGRESO DE FILOSOFIA')
                ->setDescription('Este reporte fue generado en la plataforma de la Universidad de Guayaquil')
                ->setKeywords('reporte usuarios')
                ->setCategory('Repotes');

                $documento->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setSize(20);
                $documento->getActiveSheet()->getStyle('C3:I3')->getFont()->setBold(true)->setSize(14);
                $documento->getActiveSheet()->getStyle('C3:I3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('265792');
                $documento->getActiveSheet()->getStyle('C3:I3')->getFont()->getColor()->setRGB('FFFFFF');

                $documento->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal('center');
                $documento->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal('center');
                $documento->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal('center');
                $documento->getActiveSheet()->getStyle('F3')->getAlignment()->setHorizontal('center');
                $documento->getActiveSheet()->getStyle('G3')->getAlignment()->setHorizontal('center');
                $documento->getActiveSheet()->getStyle('H3')->getAlignment()->setHorizontal('center');
                $documento->getActiveSheet()->getStyle('I3')->getAlignment()->setHorizontal('center');

                $documento->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal('left');
                $documento->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $documento->getActiveSheet()->getStyle('D')->getAlignment()->setHorizontal('left');
                $documento->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $documento->getActiveSheet()->getStyle('E')->getAlignment()->setHorizontal('left');
                $documento->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $documento->getActiveSheet()->getStyle('F')->getAlignment()->setHorizontal('left');
                $documento->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $documento->getActiveSheet()->getStyle('G')->getAlignment()->setHorizontal('left');
                $documento->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $documento->getActiveSheet()->getStyle('H')->getAlignment()->setHorizontal('right');
                $documento->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                $documento->getActiveSheet()->getStyle('I')->getAlignment()->setHorizontal('right');
                $documento->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

                $hoja = $documento->getActiveSheet();
                $hoja->setTitle("CONGRESO FILOSOFIA");
                $hoja->setCellValue("B1", "USUARIOS REGISTRADOS EN EL CONGRESO DE FILOSOFIA - UG");
            
                $hoja->setCellValue("C3", "NOMBRES");
                $hoja->setCellValue("D3", "APELLIDOS");
                $hoja->setCellValue("E3", "EMAIL");
                $hoja->setCellValue("F3", "CONCEPTO");
                $hoja->setCellValue("G3", "FECHA");
                $hoja->setCellValue("H3", "VALOR");
                $hoja->setCellValue("I3", "PAGADO");

                if($DataCollection)
                {   foreach($DataCollection as $row){
                    $hoja->setCellValue("C".$fila, utf8_encode($row['PRIMER_NOMBRE']).' '.utf8_encode($row['SEGUNDO_NOMBRE']));
                    $hoja->setCellValue("D".$fila, utf8_encode($row['PRIMER_APELLIDO']).' '.utf8_encode($row['SEGUNDO_APELLIDO']));
                    $hoja->setCellValue("E".$fila, utf8_encode($row['EMAIL_CONTACTO']));
                    $hoja->setCellValue("F".$fila, utf8_encode($row['CONCEPTO']));
                    $hoja->setCellValue("G".$fila, utf8_encode($row['FECHA_EMITE']));
                    $hoja->setCellValue("H".$fila, utf8_encode($row['VALOR']));
                    if($row['CANCEL'] == 'S' && $row['STATUS'] == ' '){
                        $hoja->setCellValue("I".$fila, 'SI');
                    }
                    if($row['CANCEL'] == 'N'){
                        $hoja->setCellValue("I".$fila, 'NO');
                    }
                    $fila++;
                    }
                }
                $nombreDelDocumento = "Reporte_Estudiantes_Registrados_Congreso.xlsx";

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
                header('Cache-Control: max-age=0');

                $writer = IOFactory::createWriter($documento, 'Xlsx');
                file_put_contents('depuracion.txt', ob_get_contents());
                /* Limpiamos el búfer */
                ob_end_clean();
                $writer->save('php://output');
                exit;
        }

        public static function DevolverDiaSemana($Fecha){
            $dias = array(7,1,2,3,4,5,6);
            $diaSemana = $dias[date('N', strtotime($Fecha))];
            return $diaSemana;
        }
    }
?>