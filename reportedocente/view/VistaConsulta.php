<?php
    class VistaConsulta{
        static public function viewPintarTablaReporte($DataCollection)
        {   $HTML = '
            <div class="col-md-12 offset-md-0 card-borrar">
                <div class="card"> 
                    <h5 class="card-header text-center">Resultados de búsqueda</h5>
                    <div class="card-body form-horizontal">';
                    foreach($DataCollection as $Row){
                      $HTML.= '<div class="form-group row center-items">
                                  <label class="col-sm-12 col-xs-12 col-form-label font-weight-bold"><b>DOCENTE</b> '.$Row['USUARIO'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$Row['DOCENTE'].'</label>
                                  <label class="col-sm-12 col-xs-12 col-form-label font-weight-bold"><b>GRUPO</b> '.$Row['GRUPO'].'</label>
                                  <label class="col-sm-12 col-xs-12 col-form-label font-weight-bold"><b>MATERIA</b> '.$Row['MATERIA'].'</label>
                              </div>
                              <div class="form-group row">
                                <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                                  <thead>
                                    <tr>
                                      <th class="text-center">LUNES</br>'.$Row['LUNES'].'</th>
                                      <th class="text-center">MARTES</br>'.$Row['MARTES'].'</th>
                                      <th class="text-center">MIERCOLES</br>'.$Row['MIERCOLES'].'</th>
                                      <th class="text-center">JUEVES</br>'.$Row['JUEVES'].'</th>
                                      <th class="text-center">VIERNES</br>'.$Row['VIERNES'].'</th>
                                      <th class="text-center">SABADO</br>'.$Row['SABADO'].'</th>
                                    </tr> 
                                  </thead> 
                                  <tbody>';
                                    foreach($Row['DATOS'] as $Reg){
                                        $HTML.= '<tr>';
                                        if($Reg['DIASEMANA']==1){
                                          $HTML.= '<td>'.$Reg['FECHA'].'</td>';
                                          $HTML.='<td></td><td></td><td></td><td></td><td></td>';
                                        }
                                        if($Reg['DIASEMANA']==2){
                                          $HTML.= '<td></td><td>'.$Reg['FECHA'].'</td>';
                                          $HTML.='<td></td><td></td><td></td><td></td>';
                                        }
                                        if($Reg['DIASEMANA']==3){
                                          $HTML.= '<td></td><td><td>'.$Reg['FECHA'].'</td>';
                                          $HTML.='<td></td><td></td><td></td>';
                                        }
                                        if($Reg['DIASEMANA']==4){
                                          $HTML.='<td></td><td></td><td></td>';
                                          $HTML.= '<td>'.$Reg['FECHA'].'</td><td></td><td>';
                                        }
                                        if($Reg['DIASEMANA']==5){
                                          $HTML.='<td></td><td></td><td></td><td></td>';
                                          $HTML.= '<td>'.$Reg['FECHA'].'</td><td></td>';
                                        }
                                        if($Reg['DIASEMANA']==6){
                                          $HTML.='<td></td><td></td><td></td><td></td><td></td>';
                                          $HTML.= '<td>'.$Reg['FECHA'].'</td>';
                                        }
                                        $HTML.= '</tr>';
                                    }
                              $HTML.= '</tbody>';
                          $HTML.= '</table>
                            </div>';
                    }
                                  
                            $HTML.= '
                      </div>
                    </div>
                  </div>';
            return $HTML;
        }
    }
?>