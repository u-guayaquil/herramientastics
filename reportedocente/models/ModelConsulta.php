<?php
require "conexion.php";
    class ModeloConsulta{
        static public function mdlDatosRegistroAulaVirtualByDocente($Cedula)
        {   $DataCollection = [];
            $QUERY = "SELECT ADL.USUARIO, CV.MATERIA, CV.COD_GRUPO, 
                                CV.GRUPO, ADL.FECHA, CV.DOCENTE,
                                CV.LUNES, CV.MARTES, CV.MIERCOLES, CV.JUEVES, CV.VIERNES, CV.SABADO
                        FROM   BdClases_online.dbo.TB_ASISTENCIA_DOCENTE_EN_LINEA ADL
                            INNER JOIN BdAcademico.dbo.CAMPUSVIRTUAL CV ON (((ADL.USUARIO=CV.IDENTIFICACION COLLATE Modern_Spanish_CI_AS) 
                            AND (ADL.PLECTIVO=CV.COD_PLECTIVO COLLATE Modern_Spanish_CI_AS)) 
                            AND (ADL.MATERIA=CV.COD_MATERIA COLLATE Modern_Spanish_CI_AS)) 
                            AND (ADL.GRUPO=CV.COD_GRUPO COLLATE Modern_Spanish_CI_AS)
                        WHERE  ADL.USUARIO = :CI
                            AND CV.CALENDARIO = 1 AND ADL.FECHA >= '31-05-2021'
                        ORDER BY ADL.USUARIO, CV.GRUPO, CV.MATERIA";
            $stmt = Conexion::conectar1()->prepare($QUERY);
            $stmt -> bindParam(":CI", $Cedula, PDO::PARAM_STR);
            $stmt -> execute();
            $DataCollection = $stmt ->fetchAll(PDO::FETCH_ASSOC);
            if($DataCollection)
            {    return array('status'=>1,'message'=>$DataCollection);   }
            else
            {   return array('status'=>0,'message'=>"NO HAY INFORMACIÓN DEL USUARIO SOLICITADO"); }
        }
    }
?>