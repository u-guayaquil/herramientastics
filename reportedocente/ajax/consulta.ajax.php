<?php
require_once "../controllers/ControlConsulta.php";
class AjaxConsulta{
    public $Cedula;
    public function ajaxBuscarRegistroDocente(){
        $Response = ControladorConsulta::ctrConsultarRegistroDocente($this->Cedula);
        echo json_encode($Response);
    }
}
/*====================================
        BUSCAR USUARIO
 ====================================*/
 if (isset($_POST["Cedula"])){
    $Servidores = new AjaxConsulta();
    $Servidores -> Cedula = $_POST["Cedula"];
    $Servidores -> ajaxBuscarRegistroDocente();
}