<?php
    date_default_timezone_set('America/Guayaquil');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="57x57" href="images//favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images//favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images//favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images//favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images//favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images//favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images//favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images//favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images//favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images//favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images//favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images//favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images//favicon/favicon-16x16.png">
    <link rel="manifest" href="images//favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images//favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Reporte Clases Virtuales Docente -UG</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="fonts/fontawesome/css/all.css" rel="stylesheet"> <!--load all styles -->
     <!-- Bootstrap core CSS - JS -->
     <link href="css/bootstrap.min.css" rel="stylesheet">
     <script src="js/bootstrap.min.js"></script>
     <!-- CSS Personalizable -->
     <link href="css/style.css" rel="stylesheet">
     <link href="plugins/fontawesome/css/all.css" rel="stylesheet">
     <?php require_once('utils/css.php'); ?>
</head>
<body class="bg-light">
    <div class="container">
      <main>
        <div class="py-2 text-center">
          <a href="./">
            <img class="d-block mx-auto mb-4" src="images/logo/UG.png" alt="" width="200px">
            <h5>REPORTE CLASES VIRTUALES - UG</h5>
          </a>
        </div>
        <div class="row">
            <div class="col-md-12 offset-md-0">
                <div class="card">
                    <h5 class="card-header text-center">Criterio de búsqueda</h5>
                    <div class="card-body form-horizontal">
                        <div class="form-group row center-items">
                            <label for="CI" class="col-sm-2 col-xs-12 col-form-label font-weight-bold">CÉDULA</label>
                            <div class="col-sm-3 col-xs-12">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-sm" type="text" id="CI" name="CI" onkeyup="Mayus(this);" required>
                                    <button class="btn btn-primary" type="button" id="btnConsultar" name="btnConsultar" onclick="return ConsultarUsuario();"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
            </div>
        </div>

        <div class="row card-body-table">
        </div>
      </main>
      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2021–2022 Universidad de Guayaquil</p>
      </footer>
    </div>
    <!-- All JS -->
    <?php require_once('utils/js.php');  ?>

</body>
</html>