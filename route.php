<?php

$request = $_SERVER['REQUEST_URI'];

switch ($request) {
    case '/estudiante' :
         require __DIR__ . '/views/estudiante.php';
        break;
    case '/horario' :
        require __DIR__ . '/views/horario.php';
        break;
    case '/descarga' :
        require __DIR__ . '/views/descarga.php';
        break;
    default:
        http_response_code(404);
        require __DIR__ . '/views/404.php';
        break;
}